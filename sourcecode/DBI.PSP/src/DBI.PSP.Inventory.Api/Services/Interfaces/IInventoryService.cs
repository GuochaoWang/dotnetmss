﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Inventory.Api.Services.Interfaces
{
    public interface IInventoryService
    {
        int Get(long id);
    }
}
