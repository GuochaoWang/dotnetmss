﻿using DBI.PSP.Inventory.Api.Services;
using DBI.PSP.Inventory.Api.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Inventory.Api.Internal
{
    public static class UseServiceExtension
    {
        public static void UseService(this IServiceCollection services)
        {
            services.AddScoped<IInventoryService, InventoryService>();
        }
    }
}
