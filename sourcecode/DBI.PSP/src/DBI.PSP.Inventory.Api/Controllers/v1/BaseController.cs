﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.Inventory.Api.Models;

namespace DBI.PSP.Inventory.Api.Controllers.v1
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class BaseController : Controller
    {
        private const string MSG_SUCCESS = "执行成功";
        private const string MSG_EXECUTE_FAILED = "执行失败";
        private const string MSG_BAB_REQUEST = "错误的请求";
        private const string MSG_NOT_FOUND = "未找到";

        private const int CODE_SUCCESS = 200;
        private const int CODE_BAB_REQUEST = 401;
        private const int CODE_NOT_FOUND = 404;
        private const int CODE_EXECUTE_FAILED = 500;

        internal IActionResult BabRequest()
        {
            return Json(FailJsonResult(CODE_BAB_REQUEST, MSG_BAB_REQUEST));
        }

        internal IActionResult BabRequest(string msg)
        {
            return Json(FailJsonResult(CODE_BAB_REQUEST, msg));
        }

        internal new IActionResult NotFound()
        {
            return Json(FailJsonResult(CODE_NOT_FOUND, MSG_NOT_FOUND));
        }

        internal IActionResult Fail()
        {
            return Json(FailJsonResult(CODE_EXECUTE_FAILED, MSG_EXECUTE_FAILED));
        }

        internal IActionResult Fail(string msg)
        {
            return Json(FailJsonResult(CODE_EXECUTE_FAILED, msg));
        }

        internal IActionResult Success<T>(T data)
        {
            return Json(SuccessJsonResult(data));
        }

        internal IActionResult Success<T>(string message, T data)
        {
            return Json(SuccessJsonResult(data, message));
        }

        internal IActionResult Success()
        {
            return Success("");
        }

        private JsonModel<string> FailJsonResult(int code, string msg)
        {
            return new JsonModel<string>
            {
                Code = code,
                Message = msg,
                Success = false,
            };
        }

        private JsonModel<T> SuccessJsonResult<T>(T data)
        {
            return new JsonModel<T>
            {
                Code = CODE_SUCCESS,
                Message = MSG_SUCCESS,
                Success = true,
                Data = data
            };
        }

        private JsonModel<T> SuccessJsonResult<T>(T data, string msg)
        {
            var rst = SuccessJsonResult<T>(data);
            rst.Message = msg;
            return rst;
        }
    }
}