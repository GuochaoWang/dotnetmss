﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.Inventory.Api.Services.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DBI.PSP.Inventory.Api.Controllers.v1
{
    public class InventoryController : BaseController
    {
        private IInventoryService _service;

        public InventoryController(IInventoryService service)
        {
            _service = service;
        }

        // GET: api/values
        [HttpGet]
        public int Get()
        {
            var rst = _service.Get(0);
            return rst;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]int stock)
        {

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
