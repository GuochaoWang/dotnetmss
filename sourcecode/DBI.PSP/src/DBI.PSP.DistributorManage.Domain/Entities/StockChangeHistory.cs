﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using Rafy;
using Rafy.ComponentModel;
using Rafy.Data;
using Rafy.Domain;
using Rafy.Domain.ORM;
using Rafy.Domain.ORM.Query;
using Rafy.Domain.Validation;
using Rafy.ManagedProperty;
using Rafy.MetaModel;
using Rafy.MetaModel.Attributes;
using Rafy.MetaModel.View;

namespace DBI.PSP.DistributorManage.Domain
{
    /// <summary>
    /// 实体的领域名称
    /// </summary>
    [RootEntity, Serializable]
    public partial class StockChangeHistory : DomainEntity
    {
        #region 构造函数

        public StockChangeHistory() { }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected StockChangeHistory(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion

        #region 引用属性

        public static readonly IRefIdProperty DistributorIdProperty =
            P<StockChangeHistory>.RegisterRefId(e => e.DistributorId, ReferenceType.Normal);
        public long DistributorId
        {
            get { return (long)this.GetRefId(DistributorIdProperty); }
            set { this.SetRefId(DistributorIdProperty, value); }
        }
        public static readonly RefEntityProperty<Distributor> DistributorProperty =
            P<StockChangeHistory>.RegisterRef(e => e.Distributor, DistributorIdProperty);
        /// <summary>
        /// 渠道商
        /// </summary>
        public Distributor Distributor
        {
            get { return this.GetRefEntity(DistributorProperty); }
            set { this.SetRefEntity(DistributorProperty, value); }
        }

        #endregion

        #region 组合子属性

        #endregion

        #region 一般属性

        public static readonly Property<long> ProductIdProperty = P<StockChangeHistory>.Register(e => e.ProductId);
        /// <summary>
        /// 产品Id
        /// </summary>
        public long ProductId
        {
            get { return this.GetProperty(ProductIdProperty); }
            set { this.SetProperty(ProductIdProperty, value); }
        }

        public static readonly Property<string> ProductNameProperty = P<StockChangeHistory>.Register(e => e.ProductName);
        /// <summary>
        /// 商品名称
        /// </summary>
        public string ProductName
        {
            get { return this.GetProperty(ProductNameProperty); }
            set { this.SetProperty(ProductNameProperty, value); }
        }

        public static readonly Property<string> ProductNoProperty = P<StockChangeHistory>.Register(e => e.ProductNo);
        /// <summary>
        /// 商品编号
        /// </summary>
        public string ProductNo
        {
            get { return this.GetProperty(ProductNoProperty); }
            set { this.SetProperty(ProductNoProperty, value); }
        }

        public static readonly Property<int> AmountProperty = P<StockChangeHistory>.Register(e => e.Amount);
        /// <summary>
        /// 数量
        /// </summary>
        public int Amount
        {
            get { return this.GetProperty(AmountProperty); }
            set { this.SetProperty(AmountProperty, value); }
        }

        public static readonly Property<DateTime> CreateTimeProperty = P<StockChangeHistory>.Register(e => e.CreateTime);
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime
        {
            get { return this.GetProperty(CreateTimeProperty); }
            set { this.SetProperty(CreateTimeProperty, value); }
        }

        #endregion

        #region 只读属性

        #endregion
    }

    /// <summary>
    /// 实体的领域名称 列表类。
    /// </summary>
    [Serializable]
    public partial class StockChangeHistoryList : DomainEntityList { }

    /// <summary>
    /// 实体的领域名称 仓库类。
    /// 负责 实体的领域名称 类的查询、保存。
    /// </summary>
    public partial class StockChangeHistoryRepository : DomainEntityRepository
    {
        /// <summary>
        /// 单例模式，外界不可以直接构造本对象。
        /// </summary>
        protected StockChangeHistoryRepository() { }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="distId"></param>
        /// <returns></returns>
        [RepositoryQuery]
        public virtual StockChangeHistoryList GetList(long distId)
        {
            var q = this.CreateLinqQuery();
            q = q.Where(t => t.DistributorId == distId);
            q = q.OrderByDescending(c => c.CreateTime);
            //q = q.Take(1);
            return (StockChangeHistoryList)this.QueryData(q);
        }
    }

    /// <summary>
    /// 实体的领域名称 配置类。
    /// 负责 实体的领域名称 类的实体元数据的配置。
    /// </summary>
    internal class StockChangeHistoryConfig : DomainEntityConfig<StockChangeHistory>
    {
        /// <summary>
        /// 配置实体的元数据
        /// </summary>
        protected override void ConfigMeta()
        {
            //配置实体的所有属性都映射到数据表中。
            Meta.MapTable().MapAllProperties();
            Meta.Property(StockChangeHistory.ProductNameProperty).MapColumn().HasLength("100");
            Meta.Property(StockChangeHistory.ProductNoProperty).MapColumn().HasLength("50");
        }
    }
}