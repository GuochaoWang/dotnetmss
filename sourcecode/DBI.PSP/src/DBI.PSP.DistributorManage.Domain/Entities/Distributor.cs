﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using Rafy;
using Rafy.ComponentModel;
using Rafy.Data;
using Rafy.Domain;
using Rafy.Domain.ORM;
using Rafy.Domain.ORM.Query;
using Rafy.Domain.Validation;
using Rafy.ManagedProperty;
using Rafy.MetaModel;
using Rafy.MetaModel.Attributes;
using Rafy.MetaModel.View;

namespace DBI.PSP.DistributorManage.Domain
{
    /// <summary>
    /// 实体的领域名称
    /// </summary>
    [RootEntity, Serializable]
    public partial class Distributor : DomainEntity
    {
        #region 构造函数

        public Distributor() { }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected Distributor(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion

        #region 引用属性

        #endregion

        #region 组合子属性

        #endregion

        #region 一般属性

        public static readonly Property<string> NameProperty = P<Distributor>.Register(e => e.Name);
        /// <summary>
        /// 渠道商名称
        /// </summary>
        public string Name
        {
            get { return this.GetProperty(NameProperty); }
            set { this.SetProperty(NameProperty, value); }
        }

        public static readonly Property<string> UsernameProperty = P<Distributor>.Register(e => e.Username);
        /// <summary>
        /// 帐号名
        /// </summary>
        public string Username
        {
            get { return this.GetProperty(UsernameProperty); }
            set { this.SetProperty(UsernameProperty, value); }
        }

        public static readonly Property<string> PasswordProperty = P<Distributor>.Register(e => e.Password);
        /// <summary>
        /// 帐号密码
        /// </summary>
        public string Password
        {
            get { return this.GetProperty(PasswordProperty); }
            set { this.SetProperty(PasswordProperty, value); }
        }

        public static readonly Property<string> ChannelCodeProperty = P<Distributor>.Register(e => e.ChannelCode);
        /// <summary>
        /// 渠道码
        /// </summary>
        public string ChannelCode
        {
            get { return this.GetProperty(ChannelCodeProperty); }
            set { this.SetProperty(ChannelCodeProperty, value); }
        }

        public static readonly Property<DateTime> CreateTimeProperty = P<Distributor>.Register(e => e.CreateTime);
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime
        {
            get { return this.GetProperty(CreateTimeProperty); }
            set { this.SetProperty(CreateTimeProperty, value); }
        }

        #endregion

        #region 只读属性

        #endregion
    }

    /// <summary>
    /// 实体的领域名称 列表类。
    /// </summary>
    [Serializable]
    public partial class DistributorList : DomainEntityList { }

    /// <summary>
    /// 实体的领域名称 仓库类。
    /// 负责 实体的领域名称 类的查询、保存。
    /// </summary>
    public partial class DistributorRepository : DomainEntityRepository
    {
        /// <summary>
        /// 单例模式，外界不可以直接构造本对象。
        /// </summary>
        protected DistributorRepository() { }

        /// <summary>
        /// 根据创建日期倒叙分页查询
        /// </summary>
        /// <param name="info"></param>
        /// <param name="searchKeyword"></param>
        /// <returns></returns>
        [RepositoryQuery]
        public virtual DistributorList GetList(PagingInfo info, string searchKeyword)
        {
            var q = this.CreateLinqQuery();
            
            if (!string.IsNullOrEmpty(searchKeyword) && !string.IsNullOrWhiteSpace(searchKeyword))
            {
                q = q.Where(t => t.Name.Contains(searchKeyword) || t.Username.Contains(searchKeyword));
            }
            q = q.OrderByDescending(c => c.CreateTime);
            if (info == null)
                return (DistributorList)this.QueryData(q);
            else
                return (DistributorList)this.QueryData(q, info);

        }

        /// <summary>
        /// 得到渠道商名称列表（最新1000条记录）
        /// </summary>
        /// <param name="name"></param>
        /// <param name="pi"></param>
        /// <returns></returns>
        [RepositoryQuery]
        public virtual DistributorList GetNameList()
        {
            var q = this.CreateLinqQuery()
                .Take(1000)
                .OrderByDescending(c => c.CreateTime)
                .Select(t => new { t.Id, t.Name });

            return (DistributorList)this.QueryData(q);

            //            var sql =
            //@"SELECT TOP 1000 [Id], [Name] FROM [Distributor] ORDER BY [CreateTime] DESC";

            //            return (this.DataQueryer as RdbDataQueryer).QueryTable(sql);
        }
    }

    /// <summary>
    /// 实体的领域名称 配置类。
    /// 负责 实体的领域名称 类的实体元数据的配置。
    /// </summary>
    internal class DistributorConfig : DomainEntityConfig<Distributor>
    {
        /// <summary>
        /// 配置实体的元数据
        /// </summary>
        protected override void ConfigMeta()
        {
            //配置实体的所有属性都映射到数据表中。
            Meta.MapTable().MapAllProperties();
            Meta.Property(Distributor.NameProperty).MapColumn().HasLength("80");
            Meta.Property(Distributor.UsernameProperty).MapColumn().HasLength("50");
            Meta.Property(Distributor.PasswordProperty).MapColumn().HasLength("128");
            Meta.Property(Distributor.ChannelCodeProperty).MapColumn().HasLength("20");
        }
    }
}