﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using Rafy;
using Rafy.ComponentModel;
using Rafy.Data;
using Rafy.Domain;
using Rafy.Domain.ORM;
using Rafy.Domain.ORM.Query;
using Rafy.Domain.Validation;
using Rafy.ManagedProperty;
using Rafy.MetaModel;
using Rafy.MetaModel.Attributes;
using Rafy.MetaModel.View;

namespace DBI.PSP.DistributorManage.Domain
{
    /// <summary>
    /// 实体的领域名称
    /// </summary>
    [RootEntity, Serializable]
    public partial class PurchasedProduct : DomainEntity
    {
        #region 构造函数

        public PurchasedProduct() { }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected PurchasedProduct(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion

        #region 引用属性

        public static readonly IRefIdProperty DistributorIdProperty =
            P<PurchasedProduct>.RegisterRefId(e => e.DistributorId, ReferenceType.Normal);
        public long DistributorId
        {
            get { return (long)this.GetRefId(DistributorIdProperty); }
            set { this.SetRefId(DistributorIdProperty, value); }
        }
        public static readonly RefEntityProperty<Distributor> DistributorProperty =
            P<PurchasedProduct>.RegisterRef(e => e.Distributor, DistributorIdProperty);
        /// <summary>
        /// 渠道商
        /// </summary>
        public Distributor Distributor
        {
            get { return this.GetRefEntity(DistributorProperty); }
            set { this.SetRefEntity(DistributorProperty, value); }
        }

        #endregion

        #region 组合子属性

        #endregion

        #region 一般属性

        public static readonly Property<long> ProductIdProperty = P<PurchasedProduct>.Register(e => e.ProductId);
        /// <summary>
        /// 产品Id
        /// </summary>
        public long ProductId
        {
            get { return this.GetProperty(ProductIdProperty); }
            set { this.SetProperty(ProductIdProperty, value); }
        }

        public static readonly Property<string> NoProperty = P<PurchasedProduct>.Register(e => e.No);
        /// <summary>
        /// 编码
        /// </summary>
        public string No
        {
            get { return this.GetProperty(NoProperty); }
            set { this.SetProperty(NoProperty, value); }
        }

        public static readonly Property<string> NameProperty = P<PurchasedProduct>.Register(e => e.Name);
        /// <summary>
        /// 商品名称
        /// </summary>
        public string Name
        {
            get { return this.GetProperty(NameProperty); }
            set { this.SetProperty(NameProperty, value); }
        }

        public static readonly Property<long> ChargeModeIdProperty = P<PurchasedProduct>.Register(e => e.ChargeModeId);
        /// <summary>
        /// 计费方式Id
        /// </summary>
        public long ChargeModeId
        {
            get { return this.GetProperty(ChargeModeIdProperty); }
            set { this.SetProperty(ChargeModeIdProperty, value); }
        }

        public static readonly Property<string> ChargeModeNameProperty = P<PurchasedProduct>.Register(e => e.ChargeModeName);
        /// <summary>
        /// 计费方式名称
        /// </summary>
        public string ChargeModeName
        {
            get { return this.GetProperty(ChargeModeNameProperty); }
            set { this.SetProperty(ChargeModeNameProperty, value); }
        }

        public static readonly Property<decimal> PriceProperty = P<PurchasedProduct>.Register(e => e.Price);
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Price
        {
            get { return this.GetProperty(PriceProperty); }
            set { this.SetProperty(PriceProperty, value); }
        }

        public static readonly Property<DateTime> CreateTimeProperty = P<PurchasedProduct>.Register(e => e.CreateTime);
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime
        {
            get { return this.GetProperty(CreateTimeProperty); }
            set { this.SetProperty(CreateTimeProperty, value); }
        }

        #endregion

        #region 只读属性

        #endregion
    }

    /// <summary>
    /// 实体的领域名称 列表类。
    /// </summary>
    [Serializable]
    public partial class PurchasedProductList : DomainEntityList { }

    /// <summary>
    /// 实体的领域名称 仓库类。
    /// 负责 实体的领域名称 类的查询、保存。
    /// </summary>
    public partial class PurchasedProductRepository : DomainEntityRepository
    {
        /// <summary>
        /// 单例模式，外界不可以直接构造本对象。
        /// </summary>
        protected PurchasedProductRepository() { }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="distId"></param>
        /// <returns></returns>
        [RepositoryQuery]
        public virtual PurchasedProductList GetList(long distId)
        {
            var q = this.CreateLinqQuery();
            q = q.Where(t => t.DistributorId == distId);
            return (PurchasedProductList)this.QueryData(q);
        }
    }

    /// <summary>
    /// 实体的领域名称 配置类。
    /// 负责 实体的领域名称 类的实体元数据的配置。
    /// </summary>
    internal class PurchasedProductConfig : DomainEntityConfig<PurchasedProduct>
    {
        /// <summary>
        /// 配置实体的元数据
        /// </summary>
        protected override void ConfigMeta()
        {
            //配置实体的所有属性都映射到数据表中。
            Meta.MapTable().MapAllProperties();
            Meta.Property(PurchasedProduct.NoProperty).MapColumn().HasLength("50");
            Meta.Property(PurchasedProduct.NameProperty).MapColumn().HasLength("100");
            Meta.Property(PurchasedProduct.ChargeModeNameProperty).MapColumn().HasLength("50");
        }
    }
}