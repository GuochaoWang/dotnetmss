﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using Rafy;
using Rafy.ComponentModel;
using Rafy.Data;
using Rafy.Domain;
using Rafy.Domain.ORM;
using Rafy.Domain.ORM.Query;
using Rafy.Domain.Validation;
using Rafy.ManagedProperty;
using Rafy.MetaModel;
using Rafy.MetaModel.Attributes;
using Rafy.MetaModel.View;

namespace DBI.PSP.DistributorManage.Domain
{
    /// <summary>
    /// 实体的领域名称
    /// </summary>
    [RootEntity, Serializable]
    public partial class Contact : DomainEntity
    {
        #region 构造函数

        public Contact() { }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected Contact(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion

        #region 引用属性

        public static readonly IRefIdProperty DistributorIdProperty =
            P<Contact>.RegisterRefId(e => e.DistributorId, ReferenceType.Normal);
        public long DistributorId
        {
            get { return (long)this.GetRefId(DistributorIdProperty); }
            set { this.SetRefId(DistributorIdProperty, value); }
        }
        public static readonly RefEntityProperty<Distributor> DistributorProperty =
            P<Contact>.RegisterRef(e => e.Distributor, DistributorIdProperty);
        /// <summary>
        /// 渠道商
        /// </summary>
        public Distributor Distributor
        {
            get { return this.GetRefEntity(DistributorProperty); }
            set { this.SetRefEntity(DistributorProperty, value); }
        }

        #endregion

        #region 组合子属性

        public static readonly ListProperty<ContactContentList> ContactContentListProperty = P<Contact>.RegisterList(e => e.ContactContentList);
        public ContactContentList ContactContentList
        {
            get { return this.GetLazyList(ContactContentListProperty); }
        }

        #endregion

        #region 一般属性

        public static readonly Property<string> TitleProperty = P<Contact>.Register(e => e.Title);
        /// <summary>
        /// 合同标题
        /// </summary>
        public string Title
        {
            get { return this.GetProperty(TitleProperty); }
            set { this.SetProperty(TitleProperty, value); }
        }

        public static readonly Property<string> NoProperty = P<Contact>.Register(e => e.No);
        /// <summary>
        /// 合同编号
        /// </summary>
        public string No
        {
            get { return this.GetProperty(NoProperty); }
            set { this.SetProperty(NoProperty, value); }
        }

        public static readonly Property<decimal> PriceProperty = P<Contact>.Register(e => e.Price);
        /// <summary>
        /// 合同金额
        /// </summary>
        public decimal Price
        {
            get { return this.GetProperty(PriceProperty); }
            set { this.SetProperty(PriceProperty, value); }
        }

        public static readonly Property<DateTime> BeginTimeProperty = P<Contact>.Register(e => e.BeginTime);
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime BeginTime
        {
            get { return this.GetProperty(BeginTimeProperty); }
            set { this.SetProperty(BeginTimeProperty, value); }
        }

        public static readonly Property<DateTime> EndTimeProperty = P<Contact>.Register(e => e.EndTime);
        /// <summary>
        /// 截至时间
        /// </summary>
        public DateTime EndTime
        {
            get { return this.GetProperty(EndTimeProperty); }
            set { this.SetProperty(EndTimeProperty, value); }
        }

        public static readonly Property<DateTime> CreateTimeProperty = P<Contact>.Register(e => e.CreateTime);
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime
        {
            get { return this.GetProperty(CreateTimeProperty); }
            set { this.SetProperty(CreateTimeProperty, value); }
        }

        #endregion

        #region 只读属性

        /// <summary>
        /// 合同客户名称
        /// </summary>
        public static readonly Property<string> NameProperty = P<Contact>.RegisterRedundancy(e => e.CompanyName,
          new RedundantPath(DistributorProperty, Distributor.NameProperty));
        public string CompanyName
        {
            get { return this.GetProperty(NameProperty); }
        }

        #endregion
    }

    /// <summary>
    /// 实体的领域名称 列表类。
    /// </summary>
    [Serializable]
    public partial class ContactList : DomainEntityList { }

    /// <summary>
    /// 实体的领域名称 仓库类。
    /// 负责 实体的领域名称 类的查询、保存。
    /// </summary>
    public partial class ContactRepository : DomainEntityRepository
    {
        /// <summary>
        /// 单例模式，外界不可以直接构造本对象。
        /// </summary>
        protected ContactRepository() { }

        [RepositoryQuery]
        public virtual ContactList GetList(PagingInfo info, string searchKeyword)
        {
            var q = this.CreateLinqQuery();
            if (!string.IsNullOrEmpty(searchKeyword) && !string.IsNullOrWhiteSpace(searchKeyword))
            {
                q = q.Where(t => t.CompanyName.Contains(searchKeyword) || t.No.Contains(searchKeyword));
            }
            q = q.OrderByDescending(c => c.CreateTime);
            if (info == null)
                return (ContactList)this.QueryData(q);
            else
                return (ContactList)this.QueryData(q, info);
        }

        [RepositoryQuery]
        public virtual ContactList GetList(PagingInfo info, long distId, string searchKeyword)
        {
            var q = this.CreateLinqQuery();
            q = q.Where(t => t.DistributorId == distId);
            if (!string.IsNullOrEmpty(searchKeyword) && !string.IsNullOrWhiteSpace(searchKeyword))
            {
                q = q.Where(t => t.CompanyName.Contains(searchKeyword) || t.No.Contains(searchKeyword));
            }
            q = q.OrderByDescending(c => c.CreateTime);
            if (info == null)
                return (ContactList)this.QueryData(q);
            else
                return (ContactList)this.QueryData(q, info);
        }
    }

    /// <summary>
    /// 实体的领域名称 配置类。
    /// 负责 实体的领域名称 类的实体元数据的配置。
    /// </summary>
    internal class ContactConfig : DomainEntityConfig<Contact>
    {
        /// <summary>
        /// 配置实体的元数据
        /// </summary>
        protected override void ConfigMeta()
        {
            //配置实体的所有属性都映射到数据表中。
            Meta.MapTable().MapAllProperties();
            Meta.Property(Contact.TitleProperty).MapColumn().HasLength("100");
            Meta.Property(Contact.NoProperty).MapColumn().HasLength("50");
        }
    }
}