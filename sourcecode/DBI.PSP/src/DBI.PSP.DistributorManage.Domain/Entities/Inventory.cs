﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using Rafy;
using Rafy.ComponentModel;
using Rafy.Data;
using Rafy.Domain;
using Rafy.Domain.ORM;
using Rafy.Domain.ORM.Query;
using Rafy.Domain.Validation;
using Rafy.ManagedProperty;
using Rafy.MetaModel;
using Rafy.MetaModel.Attributes;
using Rafy.MetaModel.View;

namespace DBI.PSP.DistributorManage.Domain
{
    /// <summary>
    /// 实体的领域名称
    /// </summary>
    [RootEntity, Serializable]
    public partial class Inventory : DomainEntity
    {
        #region 构造函数

        public Inventory() { }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected Inventory(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion

        #region 引用属性

        public static readonly IRefIdProperty DistributorIdProperty =
            P<Inventory>.RegisterRefId(e => e.DistributorId, ReferenceType.Normal);
        public long DistributorId
        {
            get { return (long)this.GetRefId(DistributorIdProperty); }
            set { this.SetRefId(DistributorIdProperty, value); }
        }
        public static readonly RefEntityProperty<Distributor> DistributorProperty =
            P<Inventory>.RegisterRef(e => e.Distributor, DistributorIdProperty);
        /// <summary>
        /// 渠道商
        /// </summary>
        public Distributor Distributor
        {
            get { return this.GetRefEntity(DistributorProperty); }
            set { this.SetRefEntity(DistributorProperty, value); }
        }

        #endregion

        #region 组合子属性

        #endregion

        #region 一般属性

        public static readonly Property<long> ProductIdProperty = P<Inventory>.Register(e => e.ProductId);
        /// <summary>
        /// 产品Id
        /// </summary>
        public long ProductId
        {
            get { return this.GetProperty(ProductIdProperty); }
            set { this.SetProperty(ProductIdProperty, value); }
        }

        public static readonly Property<string> ProductNoProperty = P<Inventory>.Register(e => e.ProductNo);
        /// <summary>
        /// 商品编号
        /// </summary>
        public string ProductNo
        {
            get { return this.GetProperty(ProductNoProperty); }
            set { this.SetProperty(ProductNoProperty, value); }
        }

        public static readonly Property<string> ProductNameProperty = P<Inventory>.Register(e => e.ProductName);
        /// <summary>
        /// 商品名称
        /// </summary>
        public string ProductName
        {
            get { return this.GetProperty(ProductNameProperty); }
            set { this.SetProperty(ProductNameProperty, value); }
        }


        public static readonly Property<int> StockOnHandProperty = P<Inventory>.Register(e => e.StockOnHand);
        /// <summary>
        /// 当前库存
        /// </summary>
        public int StockOnHand
        {
            get { return this.GetProperty(StockOnHandProperty); }
            set { this.SetProperty(StockOnHandProperty, value); }
        }

        public static readonly Property<int> StockAddUpProperty = P<Inventory>.Register(e => e.StockAddUp);
        /// <summary>
        /// 累计库存
        /// </summary>
        public int StockAddUp
        {
            get { return this.GetProperty(StockAddUpProperty); }
            set { this.SetProperty(StockAddUpProperty, value); }
        }

        public static readonly Property<int> InitialStockProperty = P<Inventory>.Register(e => e.InitialStock);
        /// <summary>
        /// 初始库存
        /// </summary>
        public int InitialStock
        {
            get { return this.GetProperty(InitialStockProperty); }
            set { this.SetProperty(InitialStockProperty, value); }
        }

        public static readonly Property<DateTime> CreateTimeProperty = P<Inventory>.Register(e => e.CreateTime);
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime
        {
            get { return this.GetProperty(CreateTimeProperty); }
            set { this.SetProperty(CreateTimeProperty, value); }
        }

        #endregion

        #region 只读属性

        #endregion
    }

    /// <summary>
    /// 实体的领域名称 列表类。
    /// </summary>
    [Serializable]
    public partial class InventoryList : DomainEntityList { }

    /// <summary>
    /// 实体的领域名称 仓库类。
    /// 负责 实体的领域名称 类的查询、保存。
    /// </summary>
    public partial class InventoryRepository : DomainEntityRepository
    {
        /// <summary>
        /// 单例模式，外界不可以直接构造本对象。
        /// </summary>
        protected InventoryRepository() { }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="distId"></param>
        /// <returns></returns>
        [RepositoryQuery]
        public virtual InventoryList GetList(long distId)
        {
            var q = this.CreateLinqQuery();
            q = q.Where(t => t.DistributorId == distId);
            return (InventoryList)this.QueryData(q);
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="distId"></param>
        /// <param name="prodId"></param>
        /// <returns></returns>
        [RepositoryQuery]
        public virtual InventoryList GetList(long distId, long prodId)
        {
            var q = this.CreateLinqQuery();
            q = q.Where(t => t.DistributorId == distId && t.ProductId == prodId);
            return (InventoryList)this.QueryData(q);
        }
    }

    /// <summary>
    /// 实体的领域名称 配置类。
    /// 负责 实体的领域名称 类的实体元数据的配置。
    /// </summary>
    internal class InventoryConfig : DomainEntityConfig<Inventory>
    {
        /// <summary>
        /// 配置实体的元数据
        /// </summary>
        protected override void ConfigMeta()
        {
            //配置实体的所有属性都映射到数据表中。
            Meta.MapTable().MapAllProperties();
            Meta.Property(Inventory.ProductNameProperty).MapColumn().HasLength("100");
            Meta.Property(Inventory.ProductNoProperty).MapColumn().HasLength("50");
        }
    }
}