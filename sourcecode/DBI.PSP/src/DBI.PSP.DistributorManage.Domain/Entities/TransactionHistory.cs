﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using Rafy;
using Rafy.ComponentModel;
using Rafy.Data;
using Rafy.Domain;
using Rafy.Domain.ORM;
using Rafy.Domain.ORM.Query;
using Rafy.Domain.Validation;
using Rafy.ManagedProperty;
using Rafy.MetaModel;
using Rafy.MetaModel.Attributes;
using Rafy.MetaModel.View;

namespace DBI.PSP.DistributorManage.Domain
{
    /// <summary>
    /// 实体的领域名称
    /// </summary>
    [RootEntity, Serializable]
    public partial class TransactionHistory : DomainEntity
    {
        #region 构造函数

        public TransactionHistory() { }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected TransactionHistory(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion

        #region 引用属性

        public static readonly IRefIdProperty DistributorIdProperty =
            P<TransactionHistory>.RegisterRefId(e => e.DistributorId, ReferenceType.Normal);
        public long DistributorId
        {
            get { return (long)this.GetRefId(DistributorIdProperty); }
            set { this.SetRefId(DistributorIdProperty, value); }
        }
        public static readonly RefEntityProperty<Distributor> DistributorProperty =
            P<TransactionHistory>.RegisterRef(e => e.Distributor, DistributorIdProperty);
        /// <summary>
        /// 渠道商
        /// </summary>
        public Distributor Distributor
        {
            get { return this.GetRefEntity(DistributorProperty); }
            set { this.SetRefEntity(DistributorProperty, value); }
        }

        #endregion

        #region 组合子属性

        #endregion

        #region 一般属性

        public static readonly Property<decimal> AmountProperty = P<TransactionHistory>.Register(e => e.Amount);
        /// <summary>
        /// 金额
        /// </summary>
        public decimal Amount
        {
            get { return this.GetProperty(AmountProperty); }
            set { this.SetProperty(AmountProperty, value); }
        }

        public static readonly Property<DateTime> DealTimeProperty = P<TransactionHistory>.Register(e => e.DealTime);
        /// <summary>
        /// 处理时间
        /// </summary>
        public DateTime DealTime
        {
            get { return this.GetProperty(DealTimeProperty); }
            set { this.SetProperty(DealTimeProperty, value); }
        }

        
        #endregion

        #region 只读属性

        #endregion
    }

    /// <summary>
    /// 实体的领域名称 列表类。
    /// </summary>
    [Serializable]
    public partial class TransactionHistoryList : DomainEntityList { }

    /// <summary>
    /// 实体的领域名称 仓库类。
    /// 负责 实体的领域名称 类的查询、保存。
    /// </summary>
    public partial class TransactionHistoryRepository : DomainEntityRepository
    {
        /// <summary>
        /// 单例模式，外界不可以直接构造本对象。
        /// </summary>
        protected TransactionHistoryRepository() { }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="distId"></param>
        /// <returns></returns>
        [RepositoryQuery]
        public virtual TransactionHistoryList GetList(long distId)
        {
            var q = this.CreateLinqQuery();
            q = q.Where(t => t.DistributorId == distId);
            q = q.OrderByDescending(t => t.DealTime);
            return (TransactionHistoryList)this.QueryData(q);
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="distId"></param>
        /// <returns></returns>
        [RepositoryQuery]
        public virtual TransactionHistoryList GetList(PagingInfo info, long distId)
        {
            var q = this.CreateLinqQuery();
            q = q.Where(t => t.DistributorId == distId);
            q = q.OrderByDescending(t => t.DealTime);
            if (info == null)
                return (TransactionHistoryList)this.QueryData(q);
            else
                return (TransactionHistoryList)this.QueryData(q, info);
        }
    }

    /// <summary>
    /// 实体的领域名称 配置类。
    /// 负责 实体的领域名称 类的实体元数据的配置。
    /// </summary>
    internal class TransactionHistoryConfig : DomainEntityConfig<TransactionHistory>
    {
        /// <summary>
        /// 配置实体的元数据
        /// </summary>
        protected override void ConfigMeta()
        {
            //配置实体的所有属性都映射到数据表中。
            Meta.MapTable().MapAllProperties();
        }
    }
}