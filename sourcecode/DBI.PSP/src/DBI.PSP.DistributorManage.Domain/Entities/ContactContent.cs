﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using Rafy;
using Rafy.ComponentModel;
using Rafy.Data;
using Rafy.Domain;
using Rafy.Domain.ORM;
using Rafy.Domain.ORM.Query;
using Rafy.Domain.Validation;
using Rafy.ManagedProperty;
using Rafy.MetaModel;
using Rafy.MetaModel.Attributes;
using Rafy.MetaModel.View;

namespace DBI.PSP.DistributorManage.Domain
{
    /// <summary>
    /// 实体的领域名称
    /// </summary>
    [RootEntity, Serializable]
    public partial class ContactContent : DomainEntity
    {
        #region 构造函数

        public ContactContent() { }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected ContactContent(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion

        #region 引用属性

        public static readonly IRefIdProperty ContactIdProperty =
            P<ContactContent>.RegisterRefId(e => e.ContactId, ReferenceType.Parent);
        public long ContactId
        {
            get { return (long)this.GetRefId(ContactIdProperty); }
            set { this.SetRefId(ContactIdProperty, value); }
        }
        public static readonly RefEntityProperty<Contact> ContactProperty =
            P<ContactContent>.RegisterRef(e => e.Contact, ContactIdProperty);
        /// <summary>
        /// 
        /// </summary>
        public Contact Contact
        {
            get { return this.GetRefEntity(ContactProperty); }
            set { this.SetRefEntity(ContactProperty, value); }
        }

        #endregion

        #region 组合子属性

        #endregion

        #region 一般属性

        public static readonly Property<long> ProductIdProperty = P<ContactContent>.Register(e => e.ProductId);
        /// <summary>
        /// 产品Id
        /// </summary>
        public long ProductId
        {
            get { return this.GetProperty(ProductIdProperty); }
            set { this.SetProperty(ProductIdProperty, value); }
        }

        public static readonly Property<string> ProductNoProperty = P<ContactContent>.Register(e => e.ProductNo);
        /// <summary>
        /// 商品编号
        /// </summary>
        public string ProductNo
        {
            get { return this.GetProperty(ProductNoProperty); }
            set { this.SetProperty(ProductNoProperty, value); }
        }

        public static readonly Property<string> ProductNameProperty = P<ContactContent>.Register(e => e.ProductName);
        /// <summary>
        /// 商品名称
        /// </summary>
        public string ProductName
        {
            get { return this.GetProperty(ProductNameProperty); }
            set { this.SetProperty(ProductNameProperty, value); }
        }

        public static readonly Property<long> ChargeModeIdProperty = P<ContactContent>.Register(e => e.ChargeModeId);
        /// <summary>
        /// 计费方式Id
        /// </summary>
        public long ChargeModeId
        {
            get { return this.GetProperty(ChargeModeIdProperty); }
            set { this.SetProperty(ChargeModeIdProperty, value); }
        }

        public static readonly Property<string> ChargeModeNameProperty = P<ContactContent>.Register(e => e.ChargeModeName);
        /// <summary>
        /// 计费方式名称
        /// </summary>
        public string ChargeModeName
        {
            get { return this.GetProperty(ChargeModeNameProperty); }
            set { this.SetProperty(ChargeModeNameProperty, value); }
        }

        public static readonly Property<int> CountProperty = P<ContactContent>.Register(e => e.Count);
        /// <summary>
        /// 数量
        /// </summary>
        public int Count
        {
            get { return this.GetProperty(CountProperty); }
            set { this.SetProperty(CountProperty, value); }
        }

        public static readonly Property<DateTime> CreateTimeProperty = P<ContactContent>.Register(e => e.CreateTime);
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime
        {
            get { return this.GetProperty(CreateTimeProperty); }
            set { this.SetProperty(CreateTimeProperty, value); }
        }

        #endregion

        #region 只读属性

        #endregion
    }

    /// <summary>
    /// 实体的领域名称 列表类。
    /// </summary>
    [Serializable]
    public partial class ContactContentList : DomainEntityList { }

    /// <summary>
    /// 实体的领域名称 仓库类。
    /// 负责 实体的领域名称 类的查询、保存。
    /// </summary>
    public partial class ContactContentRepository : DomainEntityRepository
    {
        /// <summary>
        /// 单例模式，外界不可以直接构造本对象。
        /// </summary>
        protected ContactContentRepository() { }

        [RepositoryQuery]
        public virtual ContactContentList GetByKey(PagingInfo info, long contactId, string searchKeyword)
        {
            var q = this.CreateLinqQuery();
            q = q.Where(t => t.ContactId == contactId);
            if (!string.IsNullOrEmpty(searchKeyword) && !string.IsNullOrWhiteSpace(searchKeyword))
            {
                q = q.Where(t => t.ProductName.Contains(searchKeyword));
            }
            if (info == null)
                return (ContactContentList)this.QueryData(q);
            else
                return (ContactContentList)this.QueryData(q, info);
        }

        [RepositoryQuery]
        public virtual ContactContentList GetByKey(long contactId)
        {
            return GetByKey(null, contactId, "");
        }

        [RepositoryQuery]
        public virtual ContactContentList GetByKeys(long[] contactIds)
        {
            var q = this.CreateLinqQuery();
            q = q.Where(t => contactIds.Contains(t.ContactId));
            return (ContactContentList)this.QueryData(q);
        }
    }

    /// <summary>
    /// 实体的领域名称 配置类。
    /// 负责 实体的领域名称 类的实体元数据的配置。
    /// </summary>
    internal class ContactContentConfig : DomainEntityConfig<ContactContent>
    {
        /// <summary>
        /// 配置实体的元数据
        /// </summary>
        protected override void ConfigMeta()
        {
            //配置实体的所有属性都映射到数据表中。
            Meta.MapTable().MapAllProperties();
            Meta.Property(ContactContent.ProductNameProperty).MapColumn().HasLength("100");
            Meta.Property(ContactContent.ChargeModeNameProperty).MapColumn().HasLength("50");
            Meta.Property(ContactContent.ProductNoProperty).MapColumn().HasLength("50");
        }
    }
}