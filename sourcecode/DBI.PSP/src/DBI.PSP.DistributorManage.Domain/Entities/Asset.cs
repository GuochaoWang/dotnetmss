﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using Rafy;
using Rafy.ComponentModel;
using Rafy.Data;
using Rafy.Domain;
using Rafy.Domain.ORM;
using Rafy.Domain.ORM.Query;
using Rafy.Domain.Validation;
using Rafy.ManagedProperty;
using Rafy.MetaModel;
using Rafy.MetaModel.Attributes;
using Rafy.MetaModel.View;

namespace DBI.PSP.DistributorManage.Domain
{
    /// <summary>
    /// 实体的领域名称
    /// </summary>
    [RootEntity, Serializable]
    public partial class Asset : DomainEntity
    {
        #region 构造函数

        public Asset() { }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected Asset(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion

        #region 引用属性

        public static readonly IRefIdProperty DistributorIdProperty =
            P<Asset>.RegisterRefId(e => e.DistributorId, ReferenceType.Normal);
        public long DistributorId
        {
            get { return (long)this.GetRefId(DistributorIdProperty); }
            set { this.SetRefId(DistributorIdProperty, value); }
        }
        public static readonly RefEntityProperty<Distributor> DistributorProperty =
            P<Asset>.RegisterRef(e => e.Distributor, DistributorIdProperty);
        /// <summary>
        /// 渠道商
        /// </summary>
        public Distributor Distributor
        {
            get { return this.GetRefEntity(DistributorProperty); }
            set { this.SetRefEntity(DistributorProperty, value); }
        }

        #endregion

        #region 组合子属性

        #endregion

        #region 一般属性

        public static readonly Property<decimal> BalanceProperty = P<Asset>.Register(e => e.Balance);
        /// <summary>
        /// 余额
        /// </summary>
        public decimal Balance
        {
            get { return this.GetProperty(BalanceProperty); }
            set { this.SetProperty(BalanceProperty, value); }
        }

        public static readonly Property<DateTime> CreateTimeProperty = P<Asset>.Register(e => e.CreateTime);
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime
        {
            get { return this.GetProperty(CreateTimeProperty); }
            set { this.SetProperty(CreateTimeProperty, value); }
        }

        #endregion

        #region 只读属性

        #endregion
    }

    /// <summary>
    /// 实体的领域名称 列表类。
    /// </summary>
    [Serializable]
    public partial class AssetList : DomainEntityList { }

    /// <summary>
    /// 实体的领域名称 仓库类。
    /// 负责 实体的领域名称 类的查询、保存。
    /// </summary>
    public partial class AssetRepository : DomainEntityRepository
    {
        /// <summary>
        /// 单例模式，外界不可以直接构造本对象。
        /// </summary>
        protected AssetRepository() { }
    }

    /// <summary>
    /// 实体的领域名称 配置类。
    /// 负责 实体的领域名称 类的实体元数据的配置。
    /// </summary>
    internal class AssetConfig : DomainEntityConfig<Asset>
    {
        /// <summary>
        /// 配置实体的元数据
        /// </summary>
        protected override void ConfigMeta()
        {
            //配置实体的所有属性都映射到数据表中。
            Meta.MapTable().MapAllProperties();
        }
    }
}