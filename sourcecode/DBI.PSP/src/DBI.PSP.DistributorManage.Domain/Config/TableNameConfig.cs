﻿using Rafy.MetaModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBI.PSP.DistributorManage.Domain.Config
{
    internal static class TableNameConfig
    {
        public static string IsPhantomColumnName = "DBI_IsPhantom";
        public static void Config()
        {
            CommonModel.Entities.EntityMetaCreated -= Entities_EntityMetaCreated;
            CommonModel.Entities.EntityMetaCreated += Entities_EntityMetaCreated;
        }
        private static void Entities_EntityMetaCreated(object sender, EntityMetaCreatedEventArgs e)
        {
            var meta = e.EntityMeta.TableMeta;
            if (meta != null)
            {
                var name = meta.TableName ?? e.EntityMeta.EntityType.Name;

                meta.TableName = "T_" + name;
            }
            ConfigMeta(e.EntityMeta);
        }

        private static void ConfigMeta(EntityMeta meta)
        {
            //所有实体，默认支持幽灵（假删除）功能。
            //meta.EnablePhantoms();
            //meta.Property(EntityPhantomExtension.IsPhantomProperty).MapColumn()
            //    .HasColumnName(TableNameConfig.IsPhantomColumnName);
        }
    }
}
