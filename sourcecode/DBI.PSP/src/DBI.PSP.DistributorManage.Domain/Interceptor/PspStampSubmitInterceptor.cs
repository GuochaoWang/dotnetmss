﻿using Rafy;
using Rafy.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBI.PSP.DistributorManage.Domain.Interceptor
{
    public class PspStampSubmitInterceptor : SubmitInterceptor
    {
        protected override void Submit(SubmitArgs e, ISubmitInterceptorLink link)
        {
            //switch (e.Action)
            //{
            //    case SubmitAction.ChildrenOnly:
            //    case SubmitAction.Update:
            //        var entity = e.Entity;
            //        if (entity.GetUpdatedTime().Year == 2000)
            //        {
            //            entity.SetUpdatedTime(DateTime.Now);
            //        }
            //        var user = RafyEnvironment.Identity;
            //        if (user.IsAuthenticated)
            //        {
            //            entity.SetUpdatedUser(user.Name);
            //        }
            //        break;
            //    case SubmitAction.Insert:
            //        var entity2 = e.Entity;
            //        var now = DateTime.Now;
            //        if (entity2.GetUpdatedTime().Year == 2000)
            //        {
            //            entity2.SetUpdatedTime(DateTime.Now);
            //        }

            //        entity2.SetCreatedTime(now);
            //        var user2 = RafyEnvironment.Identity;
            //        if (user2.IsAuthenticated)
            //        {
            //            entity2.SetUpdatedUser(user2.Name);
            //            entity2.SetCreatedUser(user2.Name);
            //        }
            //        break;
            //    default:
            //        //do nothing;
            //        break;
            //}

            link.InvokeNext(this, e);
        }
    }
}
