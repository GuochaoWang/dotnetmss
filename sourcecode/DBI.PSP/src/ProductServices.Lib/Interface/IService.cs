﻿using ProductAndSvcServices.Lib.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace ProductAndSvcServices.Lib.Interface
{
    internal interface IService
    {
        Task<ApiResult<ListPageDto<CompanyListDto>>> GetCompanies(string channelCode, string keyword, string[] productNo = null, ListPager pager = null);
        
        Task<ApiResult<ListPageDto<OrderListDto>>> GetCompanyOrders(long id, string keyword, string[] productNo, ListPager pager = null);

        Task<ApiResult<CompanyInfo>> GetCompanyInfo(long id);

        Task<ApiResult<List<OrderStatListDto>>> GetProductOrderStat(string channelCode, string[] productNo = null);

        Task<ApiResult<ListPageDto<OrderListDto>>> GetProductOrders(string channelCode, string keyword, string[] productNo = null, ListPager pager = null);
    }
}
