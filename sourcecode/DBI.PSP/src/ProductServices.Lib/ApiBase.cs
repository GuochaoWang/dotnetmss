﻿using ProductAndSvcServices.Lib.Internal;
using ProductAndSvcServices.Lib.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApiComm.Controllers;
using WebApiComm.Models;

namespace ProductAndSvcServices.Lib
{
    internal class ApiBase : ApiBaseController
    {
        private string _baseUri;

        public ApiBase(string baseUri)
        {
            _baseUri = baseUri;
        }

        public virtual async Task<ApiResult<ListPageDto<CompanyListDto>>> GetCompanies(string channelCode, string keyword, string[] productNo = null, ListPager pager = null)
        {
            if (string.IsNullOrEmpty(channelCode) ||
                string.IsNullOrWhiteSpace(channelCode))
                return Fail<ListPageDto<CompanyListDto>>(601, "渠道码为空");

            var url = $"{_baseUri}{Config.CompanyListUri}";

            var postData = new
            {
                ChannelCode = channelCode,
                Keyword = keyword,
                ProductNo = productNo,
                Pager = pager == null ? null : new
                {
                    PageNumber = pager.PageNumber,
                    PageSize = pager.PageSize,
                }
            };

            var rst = await HttpHelper.PostAsync<dynamic, ApiResultExt<ListPageDto<CompanyListDto>>>(url, postData);
            return ConvertApiResult(rst);
        }

        public virtual async Task<ApiResult<ListPageDto<OrderListDto>>> GetCompanyOrders(long id, string keyword, string[] productNo = null, ListPager pager = null)
        {
            if (id <= 0) return Fail<ListPageDto<OrderListDto>>(601, "Id不正确");

            var url = $"{_baseUri}{Config.CompanyOrderUri}";

            var postData = new
            {
                Id = id,
                Keyword = keyword,
                ProductNo = productNo,
                Pager = pager == null ? null : new
                {
                    PageNumber = pager.PageNumber,
                    PageSize = pager.PageSize,
                }
            };

            var rst = await HttpHelper.PostAsync<dynamic, ApiResultExt<ListPageDto<OrderListDto>>>(url, postData);
            return ConvertApiResult(rst);
        }

        public virtual async Task<ApiResult<CompanyInfo>> GetCompanyInfo(long id)
        {
            if (id <= 0) return Fail<CompanyInfo>(601, "Id不正确");

            var url = $"{_baseUri}{Config.CompanyInfoUri}";

            var postData = new
            {
                Id = id,
            };

            var rst = await HttpHelper.PostAsync<dynamic, ApiResultExt<CompanyInfo>>(url, postData);
            return ConvertApiResult(rst);
        }

        public virtual async Task<ApiResult<List<OrderStatListDto>>> GetProductOrderStat(string channelCode, string[] productNo = null)
        {
            if (string.IsNullOrEmpty(channelCode) ||
                string.IsNullOrWhiteSpace(channelCode))
                return Fail<List<OrderStatListDto>>(601, "渠道码为空");

            var url = $"{_baseUri}{Config.ProductOrderStatUri}";

            var postData = new
            {
                ChannelCode = channelCode,
                ProductNo = productNo
            };

            var rst = await HttpHelper.PostAsync<dynamic, ApiResultExt<List<OrderStatListDto>>>(url, postData);
            return ConvertApiResult(rst);
        }

        public virtual async Task<ApiResult<ListPageDto<OrderListDto>>> GetProductOrders(string channelCode, string keyword, string[] productNo = null, ListPager pager = null)
        {
            if (string.IsNullOrEmpty(channelCode) ||
                string.IsNullOrWhiteSpace(channelCode))
                return Fail<ListPageDto<OrderListDto>>(601, "渠道码为空");

            if (string.IsNullOrEmpty(channelCode)) return null;

            var url = $"{_baseUri}{Config.ProductOrderListUri}";

            var postData = new
            {
                ChannelCode = channelCode,
                Keyword = keyword,
                ProductNo = productNo,
                Pager = pager == null ? null : new
                {
                    PageNumber = pager.PageNumber,
                    PageSize = pager.PageSize,
                }
            };

            var rst = await HttpHelper.PostAsync<dynamic, ApiResultExt<ListPageDto<OrderListDto>>>(url, postData);
            return ConvertApiResult(rst);
        }

        private ApiResult<T> ConvertApiResult<T>(ApiResultExt<T> rst)
        {
            return new ApiResult<T>
            {
                Code = rst.StatusCode,
                Message = rst.Message,
                Data = rst.Data,
                Success = rst.Success,
            };
        }
    }
}
