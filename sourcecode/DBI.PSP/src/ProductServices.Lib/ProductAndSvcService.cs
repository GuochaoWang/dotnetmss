﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using ProductAndSvcServices.Lib.Interface;
using ProductAndSvcServices.Lib.Internal;
using ProductAndSvcServices.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiComm.Controllers;
using WebApiComm.Models;

namespace ProductAndSvcServices.Lib
{
    public class ProductAndSvcService : ApiBaseController, IProductAndSvcService
    {
        private const int DefPageSize = 10;
        private const int DefPageTotalCount = 1;

        /// <summary>
        /// 获取渠道商门店信息列表
        /// </summary>
        /// <param name="channelCode">渠道码</param>
        /// <param name="keyword">检索关键字</param>
        /// <param name="productNo">产品编码</param>
        /// <param name="pager">分页信息</param>
        /// <returns></returns>
        public async Task<ApiResult<ListPageDto<CompanyListDto>>> GetCompanies(string channelCode, string keyword, string[] productNo = null, ListPager pager = null)
        {
            var svcCmds = ServiceFactory.CreateSvcCmds(productNo);

            if (pager == null) pager = GetListPager(DefPageTotalCount, DefPageSize, 1);

            var reqs = svcCmds.Select(t => t.Service.GetCompanies(channelCode, keyword, t.ProductNo, pager)).ToList();

            var resps = await Task.WhenAll(reqs);

            var respList = resps.ToList();

            if (respList.Any(t => !t.Success))
            {
                var errMsg = string.Join(";", resps.Select(t => t.Message));
                return Fail<ListPageDto<CompanyListDto>>(701, errMsg);
            }

            var mergeList = MergeListAndSort(resps.Select(t => t.Data).ToList());
            
            var originPager = respList.FirstOrDefault(t => t.Data.Pager != null && t.Data.Pager.PageSize != 0);
            var data = new ListPageDto<CompanyListDto>();
            if (originPager == null)
            {
                data.List = mergeList;
                data.Pager = GetListPager(DefPageTotalCount, DefPageSize, 1);
                return Success(data);
            }

            var totalCount = respList.FindAll(t => t.Data.Pager != null && t.Data.Pager.TotalCount != null).Sum(t => t.Data.Pager.TotalCount.Value);
            var pageSize = originPager.Data.Pager.PageSize;
            if (totalCount == 0)
            {
                data.List = mergeList;
                data.Pager = GetListPager(DefPageTotalCount, pageSize, 1);
                return Success(data);
            }

            var pageNumber = originPager.Data.Pager.PageNumber;
            data.Pager = GetListPager(totalCount, pageSize, pageNumber);
            if (mergeList.Count > pageSize) data.List = mergeList.Skip((int)(pageSize * (pageNumber - 1))).Take(pageSize).ToList();
            data.List = mergeList;

            return Success(data);
        }

        /// <summary>
        /// 获取渠道商门店订单列表
        /// </summary>
        /// <param name="id">门店ID</param>
        /// <param name="keyword">检索关键字</param>
        /// <param name="productNo">商品编码</param>
        /// <param name="pager">分页信息</param>
        /// <returns></returns>
        public async Task<ApiResult<ListPageDto<OrderListDto>>> GetCompanyOrders(long id, string keyword, string[] productNo = null, ListPager pager = null)
        {
            var svcCmds = ServiceFactory.CreateSvcCmds(productNo);

            if (pager == null) pager = GetListPager(DefPageTotalCount, DefPageSize, 1);

            var reqs = svcCmds.Select(t => t.Service.GetCompanyOrders(id, keyword, t.ProductNo, pager)).ToList();

            var resps = await Task.WhenAll(reqs);

            var respList = resps.ToList();

            if (respList.Any(t => !t.Success))
            {
                var errMsg = string.Join(";", resps.Select(t => t.Message));
                return Fail<ListPageDto<OrderListDto>>(701, errMsg);
            }

            var mergeList = MergeListAndSort(resps.Select(t => t.Data).ToList());

            var originPager = respList.FirstOrDefault(t => t.Data.Pager != null && t.Data.Pager.PageSize != 0);
            var data = new ListPageDto<OrderListDto>();
            if (originPager == null)
            {
                data.List = mergeList;
                data.Pager = GetListPager(DefPageTotalCount, DefPageSize, 1);
                return Success(data);
            }

            var totalCount = respList.FindAll(t => t.Data.Pager != null && t.Data.Pager.TotalCount != null).Sum(t => t.Data.Pager.TotalCount.Value);
            var pageSize = originPager.Data.Pager.PageSize;
            if (totalCount == 0)
            {
                data.List = mergeList;
                data.Pager = GetListPager(DefPageTotalCount, pageSize, 1);
                return Success(data);
            }

            var pageNumber = originPager.Data.Pager.PageNumber;
            data.Pager = GetListPager(totalCount, pageSize, pageNumber);
            if (mergeList.Count > pageSize) data.List = mergeList.Skip((int)(pageSize * (pageNumber - 1))).Take(pageSize).ToList();
            data.List = mergeList;

            return Success(data);
        }

        /// <summary>
        /// 获取渠道商门店信息
        /// </summary>
        /// <param name="id">门店ID</param>
        /// <returns></returns>
        public async Task<ApiResult<CompanyInfo>> GetCompanyInfo(long id)
        {
            return await ServiceFactory.CreateSaaSApi().GetCompanyInfo(id);
        }

        /// <summary>
        /// 获取渠道商产品订单统计
        /// </summary>
        /// <param name="channelCode">渠道码</param>
        /// <param name="productNo">商品编码</param>
        /// <returns></returns>
        public async Task<ApiResult<List<OrderStatListDto>>> GetProductOrderStat(string channelCode, string[] productNo = null)
        {
            var svcCmds = ServiceFactory.CreateSvcCmds(productNo);

            var reqs = svcCmds.Select(t => t.Service.GetProductOrderStat(channelCode, t.ProductNo)).ToList();

            var resps = await Task.WhenAll(reqs);

            var respList = resps.ToList();

            if (respList.Any(t => !t.Success))
            {
                var errMsg = string.Join(";", resps.Select(t => t.Message));
                return Fail<List<OrderStatListDto>>(701, errMsg);
            }

            var mergeList = MergeList(resps.Select(t => t.Data).ToList());

            return Success(mergeList);
        }

        /// <summary>
        /// 获取渠道商产品订单明细
        /// </summary>
        /// <param name="channelCode">渠道码</param>
        /// <param name="keyword">检索关键字</param>
        /// <param name="productNo">产品编码</param>
        /// <param name="pager">分页信息</param>
        /// <returns></returns>
        public async Task<ApiResult<ListPageDto<OrderListDto>>> GetProductOrders(string channelCode, string keyword, string[] productNo = null, ListPager pager = null)
        {
            var svcCmds = ServiceFactory.CreateSvcCmds(productNo);

            if (pager == null) pager = GetListPager(DefPageTotalCount, DefPageSize, 1);

            var reqs = svcCmds.Select(t => t.Service.GetProductOrders(channelCode, keyword, t.ProductNo, pager)).ToList();

            var resps = await Task.WhenAll(reqs);

            var respList = resps.ToList();

            if (respList.Any(t => !t.Success))
            {
                var errMsg = string.Join(";", resps.Select(t => t.Message));
                return Fail<ListPageDto<OrderListDto>>(701, errMsg);
            }

            var mergeList = MergeListAndSort(resps.Select(t => t.Data).ToList());

            var originPager = respList.FirstOrDefault(t => t.Data.Pager != null && t.Data.Pager.PageSize != 0);
            var data = new ListPageDto<OrderListDto>();
            if (originPager == null)
            {
                data.List = mergeList;
                data.Pager = GetListPager(DefPageTotalCount, DefPageSize, 1);
                return Success(data);
            }
            
            var totalCount = respList.FindAll(t => t.Data.Pager != null && t.Data.Pager.TotalCount != null).Sum(t => t.Data.Pager.TotalCount.Value);
            var pageSize = originPager.Data.Pager.PageSize;
            if (totalCount == 0)
            {
                data.List = mergeList;
                data.Pager = GetListPager(DefPageTotalCount, pageSize, 1);
                return Success(data);
            }

            var pageNumber = originPager.Data.Pager.PageNumber;
            data.Pager = GetListPager(totalCount, pageSize, pageNumber);
            if (mergeList.Count > pageSize) data.List = mergeList.Skip((int)(pageSize * (pageNumber - 1))).Take(pageSize).ToList();
            data.List = mergeList;

            return Success(data);
        }

        private List<T> MergeListAndSort<T>(List<ListPageDto<T>> listPages) where T : BaseListModel
        {
            var mergeList = new List<T>();

            for (int i = 0; i < listPages.Count; i++)
            {
                var list = listPages[i].List;
                if (list == null) continue;

                if (mergeList == null)
                {
                    mergeList = list;
                    continue;
                }
                
                mergeList.AddRange(list);
            }

            return mergeList.OrderByDescending(t => t.CreateDate).ToList();
        }

        private List<T> MergeList<T>(List<List<T>> list)
        {
            var mergeList = new List<T>();

            for (int i = 0; i < list.Count; i++)
            {
                if (mergeList == null)
                {
                    mergeList = list[i];
                    continue;
                }
                if (list[i] == null) continue;
                mergeList.AddRange(list[i]);
            }

            return mergeList;
        }

        private ListPager GetListPager(long totalCount, int pageSize, long pageNumber)
        {
            if (totalCount <= pageSize) return new ListPager()
            {
                TotalCount = totalCount,
                PageSize = pageSize,
                PageCount = 1,
                PageNumber = 1,
                HasNextPage = false,
                HasPreviousPage = false,
            };

            var remain = totalCount % pageSize;
            var pageCount = (double)totalCount / pageSize;
            if (remain == 0) return new ListPager()
            {
                TotalCount = totalCount,
                PageSize = pageSize,
                PageCount = (long)pageCount,
                PageNumber = pageNumber,
                HasNextPage = pageNumber < pageCount,
                HasPreviousPage = pageNumber > 1,
            };


            pageCount = Math.Floor((double)totalCount / pageSize);

            if (pageCount * pageSize < totalCount) pageCount += 1;

            return new ListPager()
            {
                TotalCount = totalCount,
                PageSize = pageSize,
                PageCount = (long)pageCount,
                PageNumber = pageNumber,
                HasNextPage = pageNumber < pageCount,
                HasPreviousPage = pageNumber > 1,
            };
        }
    }
}
