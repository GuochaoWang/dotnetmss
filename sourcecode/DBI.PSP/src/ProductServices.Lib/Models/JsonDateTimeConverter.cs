﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductAndSvcServices.Lib.Models
{
    public class JsonDateTimeConverter : IsoDateTimeConverter
    {
        public JsonDateTimeConverter(string format)
        {
            base.DateTimeFormat = format;
        }
    }
}
