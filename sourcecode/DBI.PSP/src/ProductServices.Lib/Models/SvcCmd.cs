﻿using ProductAndSvcServices.Lib.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductAndSvcServices.Lib.Models
{
    internal class SvcCmd
    {
        public IService Service { get; set; }
        public string[] ProductNo { get; set; }
    }
}
