﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductAndSvcServices.Lib.Models
{
    [Serializable]
    internal class ApiResultExt<T>
    {
        public string Message { get; set; }
        public int StatusCode { get; set; }
        public T Data { get; set; }
        public bool Success { get; set; }
    }
}
