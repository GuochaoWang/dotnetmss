﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductAndSvcServices.Lib.Models
{
    public class CompanyListDto : BaseListModel
    {
        public long Id { get; set; }
        public string ChannelCode { get; set; }
        public string CompanyName { get; set; }
        public string Username { get; set; }
        public string SaleTaxNo { get; set; }
        public string Phone { get; set; }
        public string ProductNo { get; set; }
        public string ProductName { get; set; }
    }
}
