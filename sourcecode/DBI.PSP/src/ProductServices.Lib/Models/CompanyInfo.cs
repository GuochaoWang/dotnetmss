﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductAndSvcServices.Lib.Models
{
    public class CompanyInfo
    {
        public string CompanyName { get; set; }
        public string Username { get; set; }
    }
}
