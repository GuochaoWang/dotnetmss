﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductAndSvcServices.Lib.Models
{
    public class OrderStatListDto
    {
        public string ProductNo { get; set; }
        public string ProductName { get; set; }
        public int SumQuantity { get; set; }
    }
}
