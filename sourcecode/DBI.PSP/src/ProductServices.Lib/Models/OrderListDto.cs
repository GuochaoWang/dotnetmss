﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductAndSvcServices.Lib.Models
{
    public class OrderListDto : BaseListModel
    {
        public string CompanyName { get; set; }
        public long OrderId { get; set; }
        public string OrderCode { get; set; }
        public string ProductNo { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
