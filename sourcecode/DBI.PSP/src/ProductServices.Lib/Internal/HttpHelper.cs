﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AopComm.Log;

namespace ProductAndSvcServices.Lib.Internal
{
    public class HttpHelper
    {
        private static HttpClient _client;
        private static object _syncRoot = new Object();

        private static HttpClient HttpClient
        {
            get
            {
                if (_client == null)
                {
                    lock (_syncRoot)
                    {
                        if (_client == null)
                        {
                            _client = new HttpClient();
                        }
                    }
                }
                return _client;
            }
        }

        public static async Task<TRst> GetStringAsync<TRst>(string uri)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, uri);

            SetAuthorizationHeader(requestMessage);

            var dataString = string.Empty;

            try
            {
                var response = await HttpClient.SendAsync(requestMessage);
                dataString = await response.Content.ReadAsStringAsync();
                LogHelper.Info($"uri:{uri}    reponse:{dataString}");
            }
            catch(Exception ex)
            {
                var log = $"method:get;stage:call api;url:{uri}";
                LogHelper.Exception(ex, log);
                throw (ex);
            }

            try
            {
                return JsonConvert.DeserializeObject<TRst>(dataString);
            }
            catch (Exception ex)
            {
                var log = $"method:get;stage:api result deserialize;url:{uri};data:{dataString}";
                LogHelper.Exception(ex, log);
                throw (ex);
            }
        }

        public static async Task<TRst> PostAsync<TInp, TRst>(string uri, TInp item)
        {
            return await DoPostPutAsync<TInp, TRst>(HttpMethod.Post, uri, item);
        }

        public static async Task<TRst> PutAsync<TInp, TRst>(string uri, TInp item)
        {
            return await DoPostPutAsync<TInp, TRst>(HttpMethod.Put, uri, item);
        }

        private static async Task<TRst> DoPostPutAsync<TInp, TRst>(HttpMethod method, string uri, TInp item)
        {
            if (method != HttpMethod.Post && method != HttpMethod.Put)
            {
                throw new ArgumentException("Http Method必须为POST或PUT", nameof(method));
            }

            var requestMessage = new HttpRequestMessage(method, uri);

            SetAuthorizationHeader(requestMessage);

            var postDataJosn = JsonConvert.SerializeObject(item);

            requestMessage.Content = new StringContent(postDataJosn, Encoding.UTF8, "application/json");
            var dataString = string.Empty;

            try
            {
                var response = await HttpClient.SendAsync(requestMessage);
                dataString = await response.Content.ReadAsStringAsync();
                LogHelper.Info($"uri:{uri}    postdata:{postDataJosn}    reponse:{dataString}");
            }
            catch (Exception ex)
            {
                var log = $"method:post;stage:call api;url:{uri};json data:{postDataJosn}";
                LogHelper.Exception(ex, log);
                throw (ex);
            }
            
            try
            {
                return JsonConvert.DeserializeObject<TRst>(dataString);
            }
            catch (Exception ex)
            {
                var log = $"method:post;stage:api result deserialize;url:{uri};data:{dataString}";
                LogHelper.Exception(ex, log);
                throw (ex);
            }
        }

        private static void SetAuthorizationHeader(HttpRequestMessage requestMessage)
        {
            requestMessage.Headers.Add("DBISaaS-UserName", new List<string>() { Config.ApiAuthUsername });
            requestMessage.Headers.Add("DBISaaS-Password", new List<string>() { Config.ApiAuthPassword });
        }
    }
}
