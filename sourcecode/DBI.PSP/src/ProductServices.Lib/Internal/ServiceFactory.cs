﻿using ProductAndSvcServices.Lib.Interface;
using ProductAndSvcServices.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProductAndSvcServices.Lib.Internal
{
    internal static class ServiceFactory
    {
        public static List<SvcCmd> CreateSvcCmds(string[] productNo)
        {
            if (productNo == null)
            {
                return new List<SvcCmd>
                {
                    CreateSvcCmd(CreateSaaSApi(), null),
                };
            }

            var saasProdNo = productNo.Intersect(Config.SaaSProductNo, StringComparer.OrdinalIgnoreCase).ToArray();

            var rtn = new List<SvcCmd>();
            if (saasProdNo.Length > 0) rtn.Add(CreateSvcCmd(CreateSaaSApi(), saasProdNo));

            return rtn;
        }

        public static IService CreateSaaSApi()
        {
            return new SaaSApi();
        }

        private static SvcCmd CreateSvcCmd(IService service, string[] productNo)
        {
            return new SvcCmd()
            {
                Service = service, 
                ProductNo = productNo,
            };
        }
    }
}
