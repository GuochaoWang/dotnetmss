﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using AopComm.Config;

namespace ProductAndSvcServices.Lib.Internal
{
    public static class Config
    {
        public static string SaaSUri
        {
            get { return ConfigHelper.GetStrValue("SaaSUri"); }
        }

        public static string InvoiceChkUri
        {
            get { return ConfigHelper.GetStrValue("InvoiceChkUri"); }
        }

        public static string CompanyListUri
        {
            get { return ConfigHelper.GetStrValue("CompanyListUri"); }
        }

        public static string CompanyInfoUri
        {
            get { return ConfigHelper.GetStrValue("CompanyInfoUri"); }
        }

        public static string CompanyOrderUri
        {
            get { return ConfigHelper.GetStrValue("CompanyOrderUri"); }
        }

        public static string ProductOrderStatUri
        {
            get { return ConfigHelper.GetStrValue("ProductOrderStatUri"); }
        }

        public static string ProductOrderListUri
        {
            get { return ConfigHelper.GetStrValue("ProductOrderListUri"); }
        }

        public static string ApiAuthUsername
        {
            get { return ConfigHelper.GetStrValue("ApiAuthUsername"); }
        }

        public static string ApiAuthPassword
        {
            get { return ConfigHelper.GetStrValue("ApiAuthPassword"); }
        }

        public static string[] SaaSProductNo
        {
            get { return ConfigHelper.GetArrayValue("SaaSProductNo"); }
        }
    }
}
