﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProductAndSvcServices.Lib.Interface;
using ProductAndSvcServices.Lib.Internal;
using ProductAndSvcServices.Lib.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApiComm.Controllers;
using WebApiComm.Models;

namespace ProductAndSvcServices.Lib
{
    internal class SaaSApi : ApiBaseController, IService
    {
        private readonly string baseUri = Config.SaaSUri;
        
        public async Task<ApiResult<ListPageDto<CompanyListDto>>> GetCompanies(string channelCode, string keyword, string[] productNo = null, ListPager pager = null)
        {
            return await new ApiBase(baseUri).GetCompanies(channelCode, keyword, productNo, pager);
        }

        public async Task<ApiResult<ListPageDto<OrderListDto>>> GetCompanyOrders(long id, string keyword, string[] productNo = null, ListPager pager = null)
        {
            return await new ApiBase(baseUri).GetCompanyOrders(id, keyword, productNo, pager);
        }

        public async Task<ApiResult<CompanyInfo>> GetCompanyInfo(long id)
        {
            return await new ApiBase(baseUri).GetCompanyInfo(id);
        }

        public async Task<ApiResult<List<OrderStatListDto>>> GetProductOrderStat(string channelCode, string[] productNo = null)
        {
            return await new ApiBase(baseUri).GetProductOrderStat(channelCode, productNo);
        }

        public async Task<ApiResult<ListPageDto<OrderListDto>>> GetProductOrders(string channelCode, string keyword, string[] productNo = null, ListPager pager = null)
        {
            return await new ApiBase(baseUri).GetProductOrders(channelCode, keyword, productNo, pager);
        }
    }
}
