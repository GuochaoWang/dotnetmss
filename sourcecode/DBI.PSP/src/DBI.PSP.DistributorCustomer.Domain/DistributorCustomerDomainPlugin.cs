﻿using Rafy.ComponentModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBI.PSP.DistributorCustomer.Domain
{
    public class DistributorCustomerDomainPlugin : DomainPlugin
    {
        public static string DbSettingName = "DistributorCustomerDomain";

        public override void Initialize(IApp app)
        {
        }
    }
}
