using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Models.Enum
{
    public enum EnumUpdDistProdRst
    {
        [Description("调用接口失败")]
        CallExtApiFail = 750,
        [Description("拒绝修改")]
        Decline = 760,
        [Description("产品无效")]
        ProdInvalid = 770,
        [Description("购买数量无效")]
        StockInvalid = 780,
    }
}
