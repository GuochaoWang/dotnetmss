using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Models
{
    public class DistributorContContCreateForm
    {
        [DisplayName("合同Id")]
        [Required]
        public long ContactId { get; set; }
        [DisplayName("产品Id")]
        [Required]
        public long ProductId { get; set; }
        [DisplayName("商品编码")]
        [Required]
        [StringLength(50)]
        public string ProductNo { get; set; }
        [DisplayName("商品名称")]
        [Required]
        [StringLength(100)]
        public string ProductName { get; set; }
        [DisplayName("计费模式Id")]
        [Required]
        public long ChargeModeId { get; set; }
        [DisplayName("计费模式名称")]
        [Required]
        [StringLength(50)]
        public string ChargeModeName { get; set; }
        [DisplayName("数量")]
        [Required]
        public int Count { get; set; }
    }
}
