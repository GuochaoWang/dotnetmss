using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Models
{
    public class DistributorRechargeForm
    {
        [DisplayName("渠道商Id")]
        [Required]
        public long DistributorId { get; set; }
        [DisplayName("充值金额")]
        [Required]
        public decimal Amount { get; set; }
    }
}
