using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Models
{
    public class ResetPasswordForm
    {
        [DisplayName("渠道商Id")]
        [Required]
        public long Id { get; set; }
        [DisplayName("密码")]
        [Required]
        [StringLength(128)]
        public string Password { get; set; }
    }
}
