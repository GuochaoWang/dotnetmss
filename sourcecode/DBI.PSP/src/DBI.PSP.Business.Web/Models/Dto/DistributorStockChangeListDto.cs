using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Models.Dto
{
    public class DistributorStockChangeListDto
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductNo { get; set; }
        public int Amount { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
