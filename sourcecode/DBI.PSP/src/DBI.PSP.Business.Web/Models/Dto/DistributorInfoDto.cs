using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Models.Dto
{
    [Serializable]
    public class DistributorInfoDto
    {
        public DistributorDto Distributor { get; set; }
        public DistributorAssetDto Asset { get; set; }
    }
}
