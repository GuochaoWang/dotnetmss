using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Models.Dto
{
    public class BusinessUserDto
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string TrueName { get; set; }
    }
}
