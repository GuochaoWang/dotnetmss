using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Business.Web.Models.Dto
{
    public class DistributorStockConsumeListDto
    {
        public string CompanyName { get; set; }
        public long OrderId { get; set; }
        public string OrderCode { get; set; }
        public string ProductNo { get; set; }
        public string ProductName { get; set; }
        public long ChargeModeId { get; set; }
        public string ChargeModeName { get; set; }
        public int Quantity { get; set; }
        [JsonConverter(typeof(JsonDateTimeConverter), "yyyy-MM-dd")]
        public DateTime BeginDate { get; set; }
        [JsonConverter(typeof(JsonDateTimeConverter), "yyyy-MM-dd")]
        public DateTime EndDate { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
