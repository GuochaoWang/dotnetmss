using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Business.Web.Models.Dto
{
    [Serializable]
    public class DistributorContactInfoDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 渠道商Id
        /// </summary>
        public long DistributorId { get; set; }

        /// <summary>
        /// 客户名称
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        public string No { get; set; }

        /// <summary>
        /// 合同金额
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        [JsonConverter(typeof(JsonDateTimeConverter), "yyyy-MM-dd")]
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        [JsonConverter(typeof(JsonDateTimeConverter), "yyyy-MM-dd")]
        public DateTime EndTime { get; set; }


        /// <summary>
        /// 合同内容
        /// </summary>
        public List<DistributorContactContentListDto> Contents { get; set; }
    }
}
