using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Models.Dto
{
    [Serializable]
    public class DistributorContactContentListDto
    {
        public long Id { get; set; }
        public long ContactId { get; set; }
        public long ProductId { get; set; }
        public string ProductNo { get; set; }
        public string ProductName { get; set; }
        public long ChargeModeId { get; set; }
        public string ChargeModeName { get; set; }
        public int Count { get; set; }
    }
}
