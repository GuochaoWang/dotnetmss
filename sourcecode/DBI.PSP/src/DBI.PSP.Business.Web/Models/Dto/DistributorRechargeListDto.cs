using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Business.Web.Models.Dto
{
    [Serializable]
    public class DistributorRechargeListDto
    {
        public long Id { get; set; }
        public decimal Amount { get; set; }
        [JsonConverter(typeof(JsonDateTimeConverter), "yyyy-MM-dd HH:mm")]
        public DateTime DealTime { get; set; }
    }
}
