using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Models
{
    public class DistributorProductCreateForm
    {
        [DisplayName("产品Id")]
        [Required]
        public long ProductId { get; set; }
        [DisplayName("渠道商Id")]
        [Required]
        public long DistributorId { get; set; }
        [DisplayName("商品编码")]
        [Required]
        [StringLength(50)]
        public string No { get; set; }
        [DisplayName("商品名称")]
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [DisplayName("计费模式Id")]
        [Required]
        public long ChargeModeId { get; set; }
        [DisplayName("计费模式名称")]
        [Required]
        [StringLength(50)]
        public string ChargeModeName { get; set; }
        [DisplayName("单价")]
        [Required]
        public decimal Price { get; set; }
        [DisplayName("购买数量")]
        public int PurchaseCount { get; set; }
    }
}
