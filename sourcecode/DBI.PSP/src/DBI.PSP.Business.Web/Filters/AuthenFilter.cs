using DBI.PSP.Business.Web.Exceptions;
using DBI.PSP.Business.Web.Internal;
using DBI.PSP.Business.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Filters
{
    public class AuthenFilter : ActionFilterAttribute
    {
        private const string HEAD_KEY_TOKEN = "token";
        
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            if (!actionContext.HttpContext.Request.Headers.TryGetValue(HEAD_KEY_TOKEN, out StringValues authHead)) throw new AuthenFailException();
            var token = string.Empty;
            token = authHead[0];
            if (string.IsNullOrEmpty(token)) throw new AuthenFailException();
            var arrVal = ClientToken.GetVal(token);
            if (arrVal == null || arrVal.Length != 1) throw new AuthenFailException();
            
            var claim = new ClaimsIdentity();
            claim.AddClaim(new Claim(ConstSetting.ClaimKey.USER_ID, arrVal[0]));
            var myPrincipal = new ClaimsPrincipal(claim);
            actionContext.HttpContext.User = myPrincipal;
        }
    }
}
