using Newtonsoft.Json;
using Resilience.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Business.Web.Infrastructure
{
    public static class API
    {
        public static class Executor
        {
            public static async Task<ApiResult<TRst>> Get<TRst>(IHttpClient httpClient, string url)
            {
                var dataString = await httpClient.GetStringAsync(url);
                if (string.IsNullOrEmpty(dataString)) throw new Exception();
                return JsonConvert.DeserializeObject<ApiResult<TRst>>(dataString);
            }

            public static async Task<ApiResult<TRst>> Post<TRst, TInp>(IHttpClient httpClient, string url, TInp data)
            {
                var dataString = await httpClient.PostAsync(url, data);
                return JsonConvert.DeserializeObject<ApiResult<TRst>>(dataString);
            }
        }

        public static class URL
        {
            public static class Product
            {
                public static string GetList(string baseUri)
                {
                    return $"{baseUri}/v1/product/list";
                }

                public static string GetChargeModeList(string baseUri)
                {
                    return $"{baseUri}/v1/product/chargemodelist";
                }
            }

            public static class BusinessManage
            {
                public static string Login(string baseUri)
                {
                    return $"{baseUri}/v1/businessmng/account/login";
                }
            }

            public static class DistributorManage
            {
                public static string Login(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/account/login";
                }

                public static string List(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/distributor/list";
                }

                public static string Create(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/distributor/create";
                }

                public static string Update(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/distributor/update";
                }

                public static string ResetPassword(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/distributor/resetpsd";
                }

                public static string Get(string baseUri, long id)
                {
                    return $"{baseUri}/v1/distributormng/distributor/get/{id}";
                }

                public static string GetInfo(string baseUri, long id)
                {
                    return $"{baseUri}/v1/distributormng/distributor/getinfo/{id}";
                }

                public static string GetNameList(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/distributor/namelist";
                }

                public static string GetAsset(string baseUri, long id)
                {
                    return $"{baseUri}/v1/distributormng/distributor/getasset/{id}";
                }

                public static string GetProductList(string baseUri, long id)
                {
                    return $"{baseUri}/v1/distributormng/distributor/productlist/{id}";
                }

                public static string GetRechargeList(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/distributor/rechargelist";
                }

                public static string GetStockChangeList(string baseUri, long id)
                {
                    return $"{baseUri}/v1/distributormng/distributor/stockchangelist/{id}";
                }

                public static string GetStockList(string baseUri, long id)
                {
                    return $"{baseUri}/v1/distributormng/distributor/stocklist/{id}";
                }

                public static string GetStock(string baseUri, long id, long prodId)
                {
                    return $"{baseUri}/v1/distributormng/distributor/getstock/{id}/{prodId}";
                }

                public static string AddStock(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/distributor/addstock";
                }

                public static string CreateProduct(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/distributor/createproduct";
                }

                public static string DeleteProduct(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/distributor/deleteproduct";
                }

                public static string UpdateProduct(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/distributor/updateproduct";
                }

                public static string Recharge(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/distributor/recharge";
                }

                public static string GetContactList(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/contact/list";
                }

                public static string CreateContact(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/contact/create";
                }

                public static string UpdateContact(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/contact/update";
                }

                public static string DeleteContact(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/contact/delete";
                }

                public static string GetContactInfo(string baseUri, long id)
                {
                    return $"{baseUri}/v1/distributormng/contact/getinfo/{id}";
                }

                public static string GetContactContentList(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/contact/contentlist";
                }

                public static string GetContactContents(string baseUri, long contId)
                {
                    return $"{baseUri}/v1/distributormng/contact/contents/{contId}";
                }

                public static string GetContactContents(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/contact/contents";
                }

                public static string CreateContactContent(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/contact/createcontent";
                }

                public static string UpdateContactContent(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/contact/updatecontent";
                }

                public static string DeleteContactContent(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/contact/deletecontent";
                }
            }

            public static class DistributorCustomer
            {
                public static string GetUser(string baseUri)
                {
                    return $"{baseUri}/v1/distributorcus/user";
                }
            }
        }
    }
}
