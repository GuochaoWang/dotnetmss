using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Exceptions
{
    public class AuthenFailException : Exception
    {
        private const string MSG_FAIL = "身份认证失败";

        public AuthenFailException() : base(MSG_FAIL)
        {
        }

        public AuthenFailException(string msg) : base(msg)
        {
        }
    }
}
