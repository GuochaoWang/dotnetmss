using DBI.PSP.Business.Web.Services;
using DBI.PSP.Business.Web.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using ProductAndSvcServices.Lib;
using ProductAndSvcServices.Lib.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Internal
{
    public static class AddServiceExtension
    {
        public static void AddService(this IServiceCollection services)
        {
            services.AddScoped<IBusinessManageService, BusinessManageService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IDistributorCustomerService, DistributorCustomerService>();
            services.AddScoped<IDistributorManageService, DistributorManageService>();
            services.AddScoped<IProductAndSvcService, ProductAndSvcService>();
        }
    }
}
