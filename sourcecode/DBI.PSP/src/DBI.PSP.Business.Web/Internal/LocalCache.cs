using CommTools.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Internal
{
    public static class LocalCache
    {
        public static void Set<T>(T data, string key)
        {
            var cacheHelper = LocalCacheHelper<T>.Instance;
            cacheHelper.Set(data, key);
        }

        public static T Get<T>(string key)
        {
            var cacheHelper = LocalCacheHelper<T>.Instance;
            return cacheHelper.Get(key);
        }

        public static void Clear<T>(string key)
        {
            var cacheHelper = LocalCacheHelper<T>.Instance;
            cacheHelper.Remove(key);
        }
    }
}
