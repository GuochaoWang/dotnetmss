using DBI.PSP.Business.Web.IntegrationEvents.Events;
using DBI.PSP.Business.Web.IntegrationEvents.Handlers;
using EventBus.Abstractions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Internal
{
    public static class UseEventBusExtension
    {
        public static void UseEventBus(this IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            eventBus.Subscribe<ProductUpdInteEvent, ProductUpdInteEventHandler>();
        }
    }
}
