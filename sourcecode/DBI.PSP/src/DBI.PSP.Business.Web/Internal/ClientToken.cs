using CommTools.Cryptology;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Internal
{
    public class ClientToken
    {
        private const string SPLIT_CHAR = "%%";
        private const string FORMAT = "{0}%%{1}";

        public static string Create(string id)
        {
            return Create(new string[] { id });
        }

        public static string Create(string[] arrVal)
        {
            var val = string.Join(",", arrVal);
            var salt = MD5Helper.Get(string.Format(FORMAT, val, Config.ClientTokenSalt));
            val = string.Format(FORMAT, salt, val);
            var rtn = DESHelper.Encrypt(val, Config.ClientTokenKey);
            return rtn.Replace('+', '-').Replace('/', '_').Replace('=', ',');
        }

        public static string[] GetVal(string token)
        {
            if (string.IsNullOrEmpty(token)) return null;
            token = token.Replace('-', '+').Replace('_', '/').Replace(',', '=');
            token = DESHelper.Decrypt(token, Config.ClientTokenKey);
            var splitIndex = token.IndexOf(SPLIT_CHAR);
            if (splitIndex == -1) return null;
            var salt = token.Substring(0, splitIndex);
            var val = token.Substring(splitIndex + 2);
            var saltNew = MD5Helper.Get(string.Format(FORMAT, val, Config.ClientTokenSalt));
            if (salt != saltNew) return null;
            return val.Split(',', StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
