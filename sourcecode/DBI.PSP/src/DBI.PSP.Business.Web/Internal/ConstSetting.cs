using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.Internal
{
    public static class ConstSetting
    {
        public static class LocalCacheKey
        {
            public const string PRODUCT_ALL = "ProductAll";
            public const string PRODUCT_CHARGEMODE_ALL = "ProductChrgModeAll";
            public const string DISTRIBUTOR_NAME = "DistName";
        }

        public static class ClaimKey
        {
            public const string USER_ID = "UserId";
            public const string CHAN_CODE = "ChanId";
        }
    }
}
