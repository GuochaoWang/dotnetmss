using DBI.PSP.Business.Web.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBI.PSP.Business.Web.Models.Dto;
using Microsoft.Extensions.Options;
using Resilience.Http;
using Microsoft.AspNetCore.Http;
using DBI.PSP.Business.Web.Infrastructure;
using WebApiComm.Models;
using Newtonsoft.Json;
using DBI.PSP.Business.Web.Models;
using ProductAndSvcServices.Lib.Interface;
using DBI.PSP.Business.Web.Models.Enum;
using WebApiComm.Extensions;
using DBI.PSP.Business.Web.Internal;
using ProductAndSvcServices.Lib.Models;

namespace DBI.PSP.Business.Web.Services
{
    public class DistributorManageService : IDistributorManageService
    {
        protected readonly IOptionsSnapshot<AppSettings> _settings;
        protected IHttpClient _apiClient;
        protected string _remoteServiceBaseUrl;
        protected IHttpContextAccessor _httpContextAccesor;
        private IProductAndSvcService _prodAndSvcService;
        private IProductService _productService;

        public DistributorManageService(IOptionsSnapshot<AppSettings> settings,
            IHttpContextAccessor httpContextAccesor,
            IHttpClient httpClient,
            IProductAndSvcService prodAndSvcService,
            IProductService productService)
        {
            _settings = settings;
            _remoteServiceBaseUrl = _settings.Value.DistributorManageUrl;
            _httpContextAccesor = httpContextAccesor;
            _apiClient = httpClient;
            _prodAndSvcService = prodAndSvcService;
            _productService = productService;
        }

        public async Task<ApiResult<long>> CreateDistributor(DistributorCreateForm data)
        {
            var url = API.URL.DistributorManage.Create(_remoteServiceBaseUrl);
            var rst = await API.Executor.Post<long, DistributorCreateForm>(_apiClient, url, data);
            if (rst.Success && rst.Data > 0) ClearNameListCache();
            return rst;
        }

        public async Task<ListPageDto<DistributorListDto>> GetDistributorList(ListPageForm pager)
        {
            var url = API.URL.DistributorManage.List(_remoteServiceBaseUrl);
            var rst = await API.Executor.Post<ListPageDto<DistributorListDto>, ListPageForm>(_apiClient, url, pager);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<ApiResult<bool>> ResetPassword(ResetPasswordForm data)
        {
            var url = API.URL.DistributorManage.ResetPassword(_remoteServiceBaseUrl);
            return await API.Executor.Post<bool, ResetPasswordForm>(_apiClient, url, data);
        }

        public async Task<ApiResult<string>> UpdateDistributor(DistributorUpdateForm data)
        {
            var url = API.URL.DistributorManage.Update(_remoteServiceBaseUrl);
            var rst = await API.Executor.Post<string, DistributorUpdateForm>(_apiClient, url, data);
            if (rst.Success)
            {
                ClearNameListCache();
                return rst;
            }
            return rst;
        }

        public async Task<ApiResult<bool>> AddStock(DistributorStockAddForm data)
        {
            var url = API.URL.DistributorManage.AddStock(_remoteServiceBaseUrl);
            return await API.Executor.Post<bool, DistributorStockAddForm>(_apiClient, url, data);
        }

        public async Task<DistributorDto> Get(long id)
        {
            var url = API.URL.DistributorManage.Get(_remoteServiceBaseUrl, id);
            var rst = await API.Executor.Get<DistributorDto>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<DistributorAssetDto> GetAsset(long distId)
        {
            var url = API.URL.DistributorManage.GetAsset(_remoteServiceBaseUrl, distId);
            var rst = await API.Executor.Get<DistributorAssetDto>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<DistributorInfoDto> GetInfo(long distId)
        {
            var url = API.URL.DistributorManage.GetInfo(_remoteServiceBaseUrl, distId);
            var rst = await API.Executor.Get<DistributorInfoDto>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<List<SelectItemDto<long>>> GetNameList(string kw, int top = 50)
        {
            var list = await GetNameList();
            if (!string.IsNullOrEmpty(kw)) list = list.Where(t => t.Value.Contains(kw)).ToList();
            return list.Take(top).ToList();
        }

        public async Task<List<DistributorProductDto>> GetProductList(long distId)
        {
            var url = API.URL.DistributorManage.GetProductList(_remoteServiceBaseUrl, distId);
            var rst = await API.Executor.Get<List<DistributorProductDto>>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<ListPageDto<DistributorRechargeListDto>> GetRechargeList(ListPageForm pager)
        {
            var url = API.URL.DistributorManage.GetRechargeList(_remoteServiceBaseUrl);
            var rst = await API.Executor.Post<ListPageDto<DistributorRechargeListDto>, ListPageForm>(_apiClient, url, pager);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<List<DistributorStockChangeListDto>> GetStockChangeList(long distId)
        {
            var url = API.URL.DistributorManage.GetRechargeList(_remoteServiceBaseUrl);
            var rst = await API.Executor.Get<List<DistributorStockChangeListDto>>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<List<DistributorStockDto>> GetStockList(long distId, string channelCode)
        {
            var purchProdList = await GetProductList(distId);
            var url = API.URL.DistributorManage.GetStockList(_remoteServiceBaseUrl, distId);
            var rst = await API.Executor.Get<List<DistributorStockDto>>(_apiClient, url);
            if (rst.Success && rst.Data != null && purchProdList != null)
            {
                List<OrderStatListDto> stockCons = null;

                var pordNo = purchProdList.Where(t => t.CreateTime < DateTime.Now.AddMinutes(5)).Select(t => t.No).ToArray();
                var stockConsResp = await _prodAndSvcService.GetProductOrderStat(channelCode, pordNo);
                if (stockConsResp.Success && stockConsResp.Data != null)
                {
                    stockCons = stockConsResp.Data;
                }

                var prodIds = purchProdList.Select(t => t.ProductId).ToList();
                var stockList = rst.Data.Where(t => prodIds.Contains(t.ProductId)).ToList();

                stockList.ForEach(t =>
                {
                    OrderStatListDto stockConsData;
                    if (stockCons != null)
                    {
                        stockConsData = stockCons.FirstOrDefault(y => string.Compare(y.ProductNo, t.ProductNo) == 0);
                        if (stockConsData != null)
                        {
                            t.StockConsumed = stockConsData.SumQuantity;
                            t.StockOnHand = t.StockAddUp - stockConsData.SumQuantity;
                        }
                    }

                    t.PurchasedProduct = purchProdList.First(y => y.ProductId == t.ProductId);
                });

                return stockList;
            }
            return null;
        }

        public async Task<DistributorStockDto> GetStock(long distId, long prodId)
        {
            var url = API.URL.DistributorManage.GetStock(_remoteServiceBaseUrl, distId, prodId);
            var rst = await API.Executor.Get<DistributorStockDto>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<ApiResult<long>> CreateProduct(DistributorProductCreateForm data)
        {
            if (data.ProductId == 0) return new ApiResult<long> { Success = false, Code = 601, Message = "产品无效" };

            var prod = await _productService.Get(data.ProductId);
            if (prod == null) return new ApiResult<long> { Success = false, Code = 601, Message = "产品无效" };

            data.Name = prod.Name;
            data.No = prod.No;
            data.ChargeModeId = prod.ChargeMode.Id;
            data.ChargeModeName = prod.ChargeMode.Name;

            var url = API.URL.DistributorManage.CreateProduct(_remoteServiceBaseUrl);
            return await API.Executor.Post<long, DistributorProductCreateForm>(_apiClient, url, data);
        }

        public async Task<ApiResult<bool>> DeleteProduct(DistributorProductDeleteForm data)
        {
            if (data.ProductId == 0) return new ApiResult<bool> { Success = false, Code = 601, Data = false, Message = "产品Id无效" };

            //var prod = await _productService.Get(data.ProductId);
            //if (prod == null) return new ApiResult<bool> { Success = false, Code = 601, Data = false, Message = "产品Id无效" };

            //var compResp = await _prodAndSvcService.GetProductOrders(data.ChannelCode,
            //    new string[] { prod.No },
            //    new ListPager() { PageSize = 1, PageNumber = 1 });

            //if (!compResp.Success)
            //{
            //    return new ApiResult<bool>() { Success = false, Code = 701, Data = false, Message = "调用接口失败" };
            //}

            //if (compResp.Data.List != null && compResp.Data.List.Count > 0)
            //{
            //    return new ApiResult<bool>() { Success = false, Code = 702, Data = false, Message = "拒绝删除" };
            //}

            var url = API.URL.DistributorManage.DeleteProduct(_remoteServiceBaseUrl);

            return await API.Executor.Post<bool, DistributorProductDeleteForm>(_apiClient, url, data);
        }

        public async Task<ApiResult<string>> UpdateProduct(DistributorProductUpdateForm data)
        {
            if (data.ProductId == 0) return UpdateProductFail(EnumUpdDistProdRst.ProdInvalid);

            var prod = await _productService.Get(data.ProductId);
            if (prod == null) return UpdateProductFail(EnumUpdDistProdRst.ProdInvalid);
            var prodMode = await _productService.GetChargeMode(data.ChargeModeId);
            if (prodMode == null) return UpdateProductFail(EnumUpdDistProdRst.ProdInvalid);

            data.Name = prod.Name;
            data.No = prod.No;
            data.ChargeModeId = prodMode.Id;
            data.ChargeModeName = prodMode.Name;

            //var compResp = await _prodAndSvcService.GetCompanies(data.ChannelCode,
            //    "", new string[] { data.No },
            //    new ListPager() { PageSize = 1, PageNumber = 1 });
            //if (!compResp.Success) return UpdateProductFail(EnumUpdDistProdRst.CallExtApiFail);
            //if (compResp.Data.List != null && compResp.Data.List.Count > 0) return UpdateProductFail(EnumUpdDistProdRst.Decline);
            //var stock = await GetStock(data.DistributorId, data.ProductId);
            //if (stock == null) return UpdateProductFail(EnumUpdDistProdRst.Decline);
            //if (stock.StockAddUp < data.PurchaseCount) return UpdateProductFail(EnumUpdDistProdRst.StockInvalid);

            var url = API.URL.DistributorManage.UpdateProduct(_remoteServiceBaseUrl);
            return await API.Executor.Post<string, DistributorProductUpdateForm>(_apiClient, url, data);
        }

        public async Task<ApiResult<bool>> Recharge(DistributorRechargeForm data)
        {
            var url = API.URL.DistributorManage.Recharge(_remoteServiceBaseUrl);
            return await API.Executor.Post<bool, DistributorRechargeForm>(_apiClient, url, data);
        }

        public async Task<List<DistributorStockDto>> GetStockUsedStat(long distId, string channelCode)
        {
            var url = API.URL.DistributorManage.GetStockList(_remoteServiceBaseUrl, distId);
            var rst = await API.Executor.Get<List<DistributorStockDto>>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                var pordNo = rst.Data.Select(t => t.ProductNo).ToArray();
                var stockUseList = await _prodAndSvcService.GetProductOrderStat(channelCode, pordNo);
                if (stockUseList.Success && stockUseList.Data != null)
                {
                    rst.Data.ForEach(t =>
                    {
                        var stockUseData = stockUseList.Data.FirstOrDefault(y => string.Compare(y.ProductNo, t.ProductNo) == 0);
                        if (stockUseData != null)
                        {
                            t.StockOnHand = t.StockAddUp - stockUseData.SumQuantity;
                        }
                        else
                        {
                            t.StockOnHand = t.StockAddUp;
                        }
                    });
                }
                return rst.Data;
            }
            return null;
        }

        public async Task<ListPageDto<DistributorStockConsumeListDto>> GetProdConsumeList(ListPageForm listPager, string prodNo, string channelCode)
        {
            var rtn = new ListPageDto<DistributorStockConsumeListDto>
            {
                List = new List<DistributorStockConsumeListDto>(),
                Pager = new ListPager { TotalCount = 1 },
            };

            var stockConsListPage = await _prodAndSvcService.GetProductOrders(channelCode, listPager.Keyword, new string[] { prodNo }, listPager.Pager);
            if (stockConsListPage.Success && stockConsListPage.Data != null)
            {
                var products = await _productService.List();
                var stockConsData = stockConsListPage.Data.List;
                rtn.List = new List<DistributorStockConsumeListDto>();
                stockConsData.ForEach(t =>
                {
                    var prod = products.FirstOrDefault(y => string.Compare(y.No, t.ProductNo) == 0);
                    if (prod != null)
                    {
                        var item = CreateStockConsumeListDto(t);
                        item.ChargeModeId = prod.ChargeMode.Id;
                        item.ChargeModeName = prod.ChargeMode.Name;
                        rtn.List.Add(item);
                    }
                });
                rtn.Pager = stockConsListPage.Data.Pager;
            }
            return rtn;
        }

        public async Task<List<DistributorStockConsumeListDto>> GetProdConsumeList(long distId, string channelCode)
        {
            var url = API.URL.DistributorManage.GetProductList(_remoteServiceBaseUrl, distId);
            var rst = await API.Executor.Get<List<ProductDto>>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                var rtn = new List<DistributorStockConsumeListDto>();

                var pordNo = rst.Data.Select(t => t.No).ToArray();
                var products = await _productService.List();
                var stockConsList = await _prodAndSvcService.GetProductOrders(channelCode, "", pordNo);
                if (stockConsList.Success && stockConsList.Data != null)
                {
                    rst.Data.ForEach(t =>
                    {
                        var stockConsData = stockConsList.Data.List.FirstOrDefault(y => string.Compare(y.ProductNo, t.No) == 0);
                        var prod = products.FirstOrDefault(y => string.Compare(y.No, t.No) == 0);
                        if (stockConsData != null && prod != null)
                        {
                            var item = CreateStockConsumeListDto(stockConsData);
                            item.ChargeModeId = prod.ChargeMode.Id;
                            item.ChargeModeName = prod.ChargeMode.Name;
                            rtn.Add(item);
                        }
                    });
                }
                return rtn;
            }
            return null;
        }

        public async Task<ListPageDto<DistributorContactListDto>> GetContactList(ListPageForm pager)
        {
            var url = API.URL.DistributorManage.GetContactList(_remoteServiceBaseUrl);
            var dataString = await _apiClient.PostAsync(url, pager);
            var rst = JsonConvert.DeserializeObject<ApiResult<ListPageDto<DistributorContactListDto>>>(dataString);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<ApiResult<long>> CreateContact(DistributorContCreateForm data)
        {
            var url = API.URL.DistributorManage.CreateContact(_remoteServiceBaseUrl);
            return await API.Executor.Post<long, DistributorContCreateForm>(_apiClient, url, data);
        }

        public async Task<ApiResult<int>> UpdateContact(DistributorContUpdateForm data)
        {
            var url = API.URL.DistributorManage.UpdateContact(_remoteServiceBaseUrl);
            return await API.Executor.Post<int, DistributorContUpdateForm>(_apiClient, url, data);
        }

        public async Task<ApiResult<bool>> DeleteContact(DistributorContDeleteForm data)
        {
            var url = API.URL.DistributorManage.DeleteContact(_remoteServiceBaseUrl);
            return await API.Executor.Post<bool, DistributorContDeleteForm>(_apiClient, url, data);
        }

        public async Task<DistributorContactInfoDto> GetContactInfo(long id)
        {
            var url = API.URL.DistributorManage.GetContactInfo(_remoteServiceBaseUrl, id);
            var rst = await API.Executor.Get<DistributorContactInfoDto>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<ListPageDto<DistributorContactContentListDto>> GetContactContentList(ListPageForm pager)
        {
            var url = API.URL.DistributorManage.GetContactContentList(_remoteServiceBaseUrl);
            var dataString = await _apiClient.PostAsync(url, pager);
            var rst = JsonConvert.DeserializeObject<ApiResult<ListPageDto<DistributorContactContentListDto>>>(dataString);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<List<DistributorContactContentListDto>> GetContactContents(long contId)
        {
            var url = API.URL.DistributorManage.GetContactContents(_remoteServiceBaseUrl, contId);
            var dataString = await _apiClient.GetStringAsync(url);
            var rst = JsonConvert.DeserializeObject<ApiResult<List<DistributorContactContentListDto>>>(dataString);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<List<DistributorContactContentListDto>> GetContactContents(long[] contIds)
        {
            var url = API.URL.DistributorManage.GetContactContents(_remoteServiceBaseUrl);
            var dataString = await _apiClient.PostAsync(url, contIds);
            var rst = JsonConvert.DeserializeObject<ApiResult<List<DistributorContactContentListDto>>>(dataString);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<ApiResult<long>> CreateContactContent(DistributorContContCreateForm data)
        {
            if (data.ProductId == 0) return new ApiResult<long> { Success = false, Code = 601, Message = "产品无效" };

            var prod = await _productService.Get(data.ProductId);
            if (prod == null) return new ApiResult<long> { Success = false, Code = 601, Message = "产品无效" };

            data.ProductName = prod.Name;
            data.ProductNo = prod.No;
            data.ChargeModeId = prod.ChargeMode.Id;
            data.ChargeModeName = prod.ChargeMode.Name;

            var url = API.URL.DistributorManage.CreateContactContent(_remoteServiceBaseUrl);
            return await API.Executor.Post<long, DistributorContContCreateForm>(_apiClient, url, data);
        }

        public async Task<ApiResult<bool>> UpdateContactContent(DistributorContContUpdateForm data)
        {
            var url = API.URL.DistributorManage.UpdateContactContent(_remoteServiceBaseUrl);
            return await API.Executor.Post<bool, DistributorContContUpdateForm>(_apiClient, url, data);
        }

        public async Task<ApiResult<bool>> DeleteContactContent(DistributorContContDeleteForm data)
        {
            var url = API.URL.DistributorManage.DeleteContactContent(_remoteServiceBaseUrl);
            return await API.Executor.Post<bool, DistributorContContDeleteForm>(_apiClient, url, data);
        }

        private async Task<List<SelectItemDto<long>>> GetNameListFromService()
        {
            var url = API.URL.DistributorManage.GetNameList(_remoteServiceBaseUrl);

            var rst = await API.Executor.Get<List<SelectItemDto<long>>>(_apiClient, url);

            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }

            return null;
        }

        private async Task<List<SelectItemDto<long>>> GetNameList()
        {
            var cacheData = LocalCache.Get<List<SelectItemDto<long>>>(ConstSetting.LocalCacheKey.DISTRIBUTOR_NAME);
            if (cacheData != null) return cacheData;

            var data = await GetNameListFromService();
            if (data == null) return null;

            LocalCache.Set(data, ConstSetting.LocalCacheKey.DISTRIBUTOR_NAME);
            return data;
        }

        private void ClearNameListCache()
        {
            LocalCache.Clear<List<SelectItemDto<long>>>(ConstSetting.LocalCacheKey.DISTRIBUTOR_NAME);
        }

        private ApiResult<string> UpdateProductFail(EnumUpdDistProdRst rst)
        {
            return new ApiResult<string> { Success = false, Code = rst.GetVal(), Message = rst.GetDes() };
        }

        private DistributorStockConsumeListDto CreateStockConsumeListDto(OrderListDto data)
        {
            return new DistributorStockConsumeListDto
            {
                CompanyName = data.CompanyName,
                OrderId = data.OrderId,
                OrderCode = data.OrderCode,
                ProductNo = data.ProductNo,
                ProductName = data.ProductName,
                Quantity = data.Quantity,
                BeginDate = data.BeginDate,
                EndDate = data.EndDate,
                CreateDate = data.CreateDate,
            };
        }
    }
}
