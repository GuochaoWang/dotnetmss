using DBI.PSP.Business.Web.Infrastructure;
using DBI.PSP.Business.Web.Models.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Resilience.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Business.Web.Services
{
    public class ContactManageService
    {
        protected readonly IOptionsSnapshot<AppSettings> _settings;
        protected string _remoteServiceBaseUrl;
        protected IHttpClient _apiClient;
        protected IHttpContextAccessor _httpContextAccesor;

        public ContactManageService(IOptionsSnapshot<AppSettings> settings, IHttpContextAccessor httpContextAccesor, IHttpClient httpClient) //: base(settings, httpContextAccesor, httpClient)
        {
            _settings = settings;
            _remoteServiceBaseUrl = _settings.Value.ContactManageUrl;
            _httpContextAccesor = httpContextAccesor;
            _apiClient = httpClient;
        }

        public async Task<ListPageDto<List<DistributorContactListDto>>> GetDistributorList(ListPageForm pager)
        {
            var url = API.URL.DistributorManage.List(_remoteServiceBaseUrl);
            var dataString = await _apiClient.PostAsync(url, pager);
            var rst = JsonConvert.DeserializeObject<ApiResult<ListPageDto<List<DistributorContactListDto>>>>(dataString);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }
    }
}
