using DBI.PSP.Business.Web.Models;
using DBI.PSP.Business.Web.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Business.Web.Services.Interfaces
{
    public interface IProductService
    {
        Task<List<ProductDto>> List();
        Task<ProductDto> Get(long id);
        Task Update(ProductDto data);
        Task<List<ChargeModeDto>> ChargeModeList();
        Task<ChargeModeDto> GetChargeMode(long id);
    }
}
