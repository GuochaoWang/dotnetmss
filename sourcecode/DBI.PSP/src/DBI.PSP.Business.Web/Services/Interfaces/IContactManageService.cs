using DBI.PSP.Business.Web.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Business.Web.Services.Interfaces
{
    public interface IContactManageService
    {
        Task<ListPageDto<List<DistributorContactListDto>>> GetDistributorList(ListPageForm pager);
    }
}
