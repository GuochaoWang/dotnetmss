using DBI.PSP.Business.Web.Models;
using DBI.PSP.Business.Web.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;
using DBI.PSP.Business.Web.Models.Enum;

namespace DBI.PSP.Business.Web.Services.Interfaces
{
    public interface IDistributorManageService
    {
        Task<ListPageDto<DistributorListDto>> GetDistributorList(ListPageForm listPager);
        Task<ApiResult<long>> CreateDistributor(DistributorCreateForm data);
        Task<ApiResult<string>> UpdateDistributor(DistributorUpdateForm data);
        Task<ApiResult<bool>> ResetPassword(ResetPasswordForm data);
        Task<DistributorDto> Get(long id);
        Task<DistributorInfoDto> GetInfo(long id);
        Task<List<SelectItemDto<long>>> GetNameList(string kw, int top = 50);

        Task<DistributorAssetDto> GetAsset(long distId);
        Task<ApiResult<bool>> Recharge(DistributorRechargeForm data);
        Task<ListPageDto<DistributorRechargeListDto>> GetRechargeList(ListPageForm listPager);

        Task<List<DistributorProductDto>> GetProductList(long distId);
        Task<ApiResult<long>> CreateProduct(DistributorProductCreateForm data);
        Task<ApiResult<string>> UpdateProduct(DistributorProductUpdateForm data);
        Task<ApiResult<bool>> DeleteProduct(DistributorProductDeleteForm data);
        Task<ListPageDto<DistributorStockConsumeListDto>> GetProdConsumeList(ListPageForm listPager, string prodNo, string channelCode);
        Task<List<DistributorStockConsumeListDto>> GetProdConsumeList(long distId, string channelCode);

        Task<List<DistributorStockDto>> GetStockList(long distId, string channelCode);
        Task<DistributorStockDto> GetStock(long distId, long prodId);
        Task<ApiResult<bool>> AddStock(DistributorStockAddForm data);
        Task<List<DistributorStockChangeListDto>> GetStockChangeList(long distId);

        Task<ListPageDto<DistributorContactListDto>> GetContactList(ListPageForm listPager);
        Task<ApiResult<long>> CreateContact(DistributorContCreateForm data);
        Task<ApiResult<int>> UpdateContact(DistributorContUpdateForm data);
        Task<ApiResult<bool>> DeleteContact(DistributorContDeleteForm data);
        Task<DistributorContactInfoDto> GetContactInfo(long id);

        Task<ListPageDto<DistributorContactContentListDto>> GetContactContentList(ListPageForm listPager);
        Task<List<DistributorContactContentListDto>> GetContactContents(long contId);
        Task<List<DistributorContactContentListDto>> GetContactContents(long[] contIds);
        Task<ApiResult<long>> CreateContactContent(DistributorContContCreateForm data);
        Task<ApiResult<bool>> UpdateContactContent(DistributorContContUpdateForm data);
        Task<ApiResult<bool>> DeleteContactContent(DistributorContContDeleteForm data);
    }
}
