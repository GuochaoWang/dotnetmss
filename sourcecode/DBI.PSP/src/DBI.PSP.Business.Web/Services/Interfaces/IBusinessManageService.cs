using DBI.PSP.Business.Web.Models;
using DBI.PSP.Business.Web.Models.Dto;
using DBI.PSP.Business.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Business.Web.Services.Interfaces
{
    public interface IBusinessManageService
    {
        Task<ApiResult<AccountViewModel>> Login(string username, string passwork);
    }
}
