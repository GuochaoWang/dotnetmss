using DBI.PSP.Business.Web.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;
using ProductAndSvcServices.Lib.Interface;
using ProductAndSvcServices.Lib.Models;

namespace DBI.PSP.Business.Web.Services
{
    public class DistributorCustomerService : IDistributorCustomerService
    {
        private IProductAndSvcService _prodAndSvcService;

        public DistributorCustomerService(IProductAndSvcService prodAndSvcService)
        {
            _prodAndSvcService = prodAndSvcService;
        }

        public async Task<ApiResult<ListPageDto<CompanyListDto>>> GetCompanies(string channelCode, string keyword, string[] productNo = null, ListPager pager = null)
        {
            return await _prodAndSvcService.GetCompanies(channelCode, keyword, productNo, pager);
        }

        public async Task<ApiResult<CompanyInfo>> GetCompanyInfo(long id, string productNo)
        {
            return await _prodAndSvcService.GetCompanyInfo(id);
        }

        public async Task<ApiResult<ListPageDto<OrderListDto>>> GetCompanyOrders(long id, string keyword, string[] productNo, ListPager pager = null)
        {
            return await _prodAndSvcService.GetCompanyOrders(id, keyword, productNo, pager);
        }

        public async Task<ApiResult<ListPageDto<OrderListDto>>> GetProductOrders(string channelCode, string keyword, string[] productNo = null, ListPager pager = null)
        {
            return await _prodAndSvcService.GetProductOrders(channelCode, keyword, productNo, pager);
        }

        public async Task<ApiResult<List<OrderStatListDto>>> GetProductOrderStat(string channelCode, string[] productNo = null)
        {
            return await _prodAndSvcService.GetProductOrderStat(channelCode, productNo);
        }
    }
}
