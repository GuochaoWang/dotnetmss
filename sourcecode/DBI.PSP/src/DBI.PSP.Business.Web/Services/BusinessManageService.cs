using DBI.PSP.Business.Web.Infrastructure;
using DBI.PSP.Business.Web.Internal;
using DBI.PSP.Business.Web.Models;
using DBI.PSP.Business.Web.Models.Dto;
using DBI.PSP.Business.Web.Services.Interfaces;
using DBI.PSP.Business.Web.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Resilience.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Business.Web.Services
{
    public class BusinessManageService : IBusinessManageService
    {
        protected readonly IOptionsSnapshot<AppSettings> _settings;
        protected IHttpClient _apiClient;
        protected string _remoteServiceBaseUrl;
        protected IHttpContextAccessor _httpContextAccesor;

        public BusinessManageService(IOptionsSnapshot<AppSettings> settings, IHttpContextAccessor httpContextAccesor, IHttpClient httpClient) //: base(settings, httpContextAccesor, httpClient)
        {
            _settings = settings;
            _remoteServiceBaseUrl = _settings.Value.BusinessManageUrl;
            _httpContextAccesor = httpContextAccesor;
            _apiClient = httpClient;
        }

        public async Task<ApiResult<AccountViewModel>> Login(string username, string password)
        {
            var url = API.URL.BusinessManage.Login(_remoteServiceBaseUrl);
            var rst = await API.Executor.Post<BusinessUserDto, LoginForm>(_apiClient, url, new LoginForm { Username = username, Password = password });
            if (!rst.Success)
            {
                return new ApiResult<AccountViewModel>()
                {
                    Success = rst.Success,
                    Code = rst.Code,
                    Message = rst.Message,
                    Data = null,
                };
            }

            var token = ClientToken.Create(rst.Data.Id.ToString());

            return new ApiResult<AccountViewModel>()
            {
                Success = rst.Success,
                Code = rst.Code,
                Message = rst.Message,
                Data = new AccountViewModel
                {
                    Username = rst.Data.Username,
                    TrueName = rst.Data.TrueName,
                    Token = token,
                },
            };
        }
    }
}
