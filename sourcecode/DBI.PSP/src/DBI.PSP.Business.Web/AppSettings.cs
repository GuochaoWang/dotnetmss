using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web
{
    public class AppSettings
    {
        public string ProductUrl { get; set; }
        public string BusinessManageUrl { get; set; }
        public string DistributorManageUrl { get; set; }
        public string ContactManageUrl { get; set; }
        public string DistributorCustomerUrl { get; set; }
        public Logging Logging { get; set; }
    }

    public class Logging
    {
        public bool IncludeScopes { get; set; }
        public Loglevel LogLevel { get; set; }
    }

    public class Loglevel
    {
        public string Default { get; set; }
        public string System { get; set; }
        public string Microsoft { get; set; }
    }
}
