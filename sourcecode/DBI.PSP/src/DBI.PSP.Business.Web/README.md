# DBI.PSP.Web #
大贲科技产品与服务市场1.0

## 前言 ##
大贲科技产品与服务市场1.0

## 功能 ##
- 商务
- [x] 登录
- [x] 渠道管理
- [x] 账户详情
- [x] 合同管理
- 渠道商
- [x] 登录
- [x] 个人中心
- [x] 用户管理
- [x] 我的合同


## 目录结构介绍 ##

	|-- build                            // webpack配置文件
	|-- config                           // 项目打包路径
	|-- src                              // 源码目录
	|   |-- components                   // 组件
	|       |-- common                   // 公共组件
	|           |-- Header.vue           // 公共头部
	|           |-- Home.vue           	 // 公共路由入口
	|           |-- Sidebar.vue          // 公共左边栏
	|		|-- page                   	 // 主要路由页面
	|           |-- BaseCharts.vue       // 基础图表
	|           |-- BaseForm.vue         // 基础表单
	|           |-- BaseTable.vue        // 基础表格
	|           |-- Login.vue          	 // 登录
	|           |-- Markdown.vue         // markdown组件
	|           |-- Readme.vue           // 自述组件
	|           |-- Upload.vue           // 图片上传
	|           |-- VueEditor.vue        // 富文本编辑器
	|           |-- VueTable.vue         // vue表格组件
	|   |-- App.vue                      // 页面入口文件
	|   |-- main.js                      // 程序入口文件，加载各种公共组件
	|-- .babelrc                         // ES6语法编译配置
	|-- .editorconfig                    // 代码编写规格
	|-- .gitignore                       // 忽略的文件
	|-- index.html                       // 入口html文件
	|-- package.json                     // 项目及工具的依赖配置文件
	|-- README.md                        // 说明


## 开发环境配置 ##

	安装Node.js
    装置cnpm：http://blog.csdn.net/wing_93/article/details/78573820
	cd src\DBI.PSP.Web  // 进入模板目录
	cnpm install         // 安装项目依赖，等待安装完成之后

    *注意：
    *  1、因为node_modules目录结构复杂，在svn中忽略此目录，否则更新/提交等操作会非常慢
    *  2、因为node_modules目录结构复杂，在Windows中把node_modules目录及子目录设置为隐藏目录，否则VS打开项目会很慢
    *  3、未避免node_modules在vs中加载慢可以单独为vue项目创建文件夹
    *  4、VS设置中取消Projects and Solution/Web Package Management/Package Restore中所有选项

## 本地开发 ##

	// 开启服务器，浏览器访问 http://localhost:8080
	cnpm run dev

    *注意：
    *  1、使用.net core mvc项目开发作为web api的话，需要配置web api支持服务端跨域请求
    *  2、config/index.js中配置proxyTable，指定代理地址为.net core mvc站点地址
    *  


## 构建生产 ##

	// 执行构建命令，生成的dist文件夹放在服务器下即可访问
	cnpm run build
