using DBI.PSP.Business.Web.Models.Dto;
using EventBus.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.IntegrationEvents.Events
{
    public class ProductUpdInteEvent : IntegrationEvent
    {
        public ProductDto Product { get; set; }

        public ProductUpdInteEvent(ProductDto product)
        {
            Product = product;
        }
    }
}
