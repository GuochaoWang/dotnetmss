using DBI.PSP.Business.Web.IntegrationEvents.Events;
using DBI.PSP.Business.Web.Services.Interfaces;
using EventBus.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Business.Web.IntegrationEvents.Handlers
{
    public class ProductUpdInteEventHandler
        : IIntegrationEventHandler<ProductUpdInteEvent>
    {
        private readonly IProductService _produSvc;

        public ProductUpdInteEventHandler(IProductService produSvc)
        {
            _produSvc = produSvc ?? throw new ArgumentNullException(nameof(produSvc));
        }

        public async Task Handle(ProductUpdInteEvent @event)
        {
            if (@event.Product == null) return;
            await _produSvc.Update(@event.Product);
        }
    }
}
