using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.Business.Web.ViewModels;
using WebApiComm.Controllers;

namespace DBI.PSP.BusinessManage.Api.Controllers.v1
{
    [Route("api/v1/[controller]/[action]")]
    public class BaseController : ApiBaseController
    {
    }
}
