/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.BusinessManage.Api.Controllers.v1;
using DBI.PSP.Business.Web.Services.Interfaces;
using WebApiComm.Models;
using DBI.PSP.Business.Web.Models.Dto;
using DBI.PSP.Business.Web.Models;
using DBI.PSP.Business.Web.Internal;
using DBI.PSP.Business.Web.Filters;

namespace DBI.PSP.Business.Web.Controllers
{
    [AuthenFilter]
    public class DistributorController : BaseController
    {
        private IDistributorManageService _distributorMngService;

        public DistributorController(IDistributorManageService distributorMngService)
        {
            _distributorMngService = distributorMngService;
        }

        /// <summary>
        /// 渠道商列表
        /// </summary>
        /// <param name="listPager">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<ListPageDto<DistributorListDto>>> List([FromBody]ListPageForm listPager)
        {
            if (listPager == null || listPager.Pager == null) return ParameterNull<ListPageDto<DistributorListDto>>();
            if (listPager.Pager.PageSize == 0) listPager.Pager.PageSize = Config.DefaultListPageSize;
            if (listPager.Pager.PageNumber == 0) listPager.Pager.PageNumber = 1;
            var rst = await _distributorMngService.GetDistributorList(listPager);
            return Success(rst);
        }

        /// <summary>
        /// 创建渠道商
        /// </summary>
        /// <param name="data">渠道商表单信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<long>> Create([FromBody]DistributorCreateForm data)
        {
            if (data == null) return ParameterNull<long>();
            return await _distributorMngService.CreateDistributor(data);
        }

        /// <summary>
        /// 修改渠道商
        /// </summary>
        /// <param name="data">渠道商表单信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<string>> Update([FromBody]DistributorUpdateForm data)
        {
            if (data == null) return ParameterNull<string>();
            return await _distributorMngService.UpdateDistributor(data);
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="data">重置密码表单</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<bool>> ResetPassword([FromBody]ResetPasswordForm data)
        {
            if (data == null) return ParameterNull<bool>();
            return await _distributorMngService.ResetPassword(data);
        }

        /// <summary>
        /// 得到渠道商详细信息
        /// </summary>
        /// <param name="id">渠道商记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ApiResult<DistributorInfoDto>> Info(long id)
        {
            if (id <= 0) return ParameterNull<DistributorInfoDto>();
            var rst = await _distributorMngService.GetInfo(id);
            return Success(rst);
        }

        /// <summary>
        /// 得到最新渠道商名称
        /// </summary>
        /// <param name="top">个数</param>
        /// <returns></returns>
        [HttpGet("{top}")]
        public async Task<ApiResult<List<SelectItemDto<long>>>> NameList(int top)
        {
            if (top <= 0) return ParameterNull<List<SelectItemDto<long>>>();
            var rst = await _distributorMngService.GetNameList("", top);
            return Success(rst);
        }

        /// <summary>
        /// 得到最新渠道商名称
        /// </summary>
        /// <param name="top">个数</param>
        /// <param name="kw">检索关键字</param>
        /// <returns></returns>
        [HttpGet("{top}/{kw}")]
        public async Task<ApiResult<List<SelectItemDto<long>>>> NameList(int top, string kw)
        {
            if (top <= 0) return ParameterNull<List<SelectItemDto<long>>>();
            var rst = await _distributorMngService.GetNameList(kw, top);
            return Success(rst);
        }

        /// <summary>
        /// 得到渠道商资产信息
        /// </summary>
        /// <param name="id">渠道商记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ApiResult<DistributorAssetDto>> Asset(long id)
        {
            if (id <= 0) return ParameterNull<DistributorAssetDto>();
            var rst = await _distributorMngService.GetAsset(id);
            return Success(rst);
        }

        /// <summary>
        /// 渠道商充值
        /// </summary>
        /// <param name="data">充值表单信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<bool>> Recharge([FromBody]DistributorRechargeForm data)
        {
            if (data == null) return ParameterNull<bool>();
            return await _distributorMngService.Recharge(data);
        }

        /// <summary>
        /// 渠道商充值记录
        /// </summary>
        /// <param name="listPager">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<ListPageDto<DistributorRechargeListDto>>> RechargeList([FromBody]ListPageForm listPager)
        {
            if (listPager == null || listPager.Pager == null) return ParameterNull<ListPageDto<DistributorRechargeListDto>>();
            if (listPager.Pager.PageSize == 0) listPager.Pager.PageSize = Config.DefaultListPageSize;
            if (listPager.Pager.PageNumber == 0) listPager.Pager.PageNumber = 1;
            var rst = await _distributorMngService.GetRechargeList(listPager);
            return Success(rst);
        }

        /// <summary>
        /// 渠道商已购商品列表
        /// </summary>
        /// <param name="id">渠道商记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ApiResult<List<DistributorProductDto>>> ProductList(long id)
        {
            if (id <= 0) return ParameterNull<List<DistributorProductDto>>();
            var rst = await _distributorMngService.GetProductList(id);
            return Success(rst);
        }

        /// <summary>
        /// 创建渠道商商品
        /// </summary>
        /// <param name="data">商品表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<long>> CreateProduct([FromBody]DistributorProductCreateForm data)
        {
            if (data == null) return ParameterNull<long>();
            return await _distributorMngService.CreateProduct(data);
        }

        /// <summary>
        /// 修改渠道商商品
        /// </summary>
        /// <param name="data">商品表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<string>> UpdateProduct([FromBody]DistributorProductUpdateForm data)
        {
            if (data == null) return ParameterNull<string>();
            return await _distributorMngService.UpdateProduct(data);
        }

        /// <summary>
        /// 删除渠道商商品
        /// </summary>
        /// <param name="data">商品表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<bool>> DeleteProduct([FromBody]DistributorProductDeleteForm data)
        {
            if (data == null) return ParameterNull<bool>();
            return await _distributorMngService.DeleteProduct(data);
        }

        /// <summary>
        /// 渠道商商品库粗列表
        /// </summary>
        /// <param name="id">渠道商记录ID</param>
        /// <param name="chancode">渠道码</param>
        /// <returns></returns>
        [HttpGet("{id}/{chancode}")]
        public async Task<ApiResult<List<DistributorStockDto>>> StockList(long id, string chancode)
        {
            if (id <= 0) return ParameterNull<List<DistributorStockDto>>();
            var rst = await _distributorMngService.GetStockList(id, chancode);
            return Success(rst);
        }

        /// <summary>
        /// 新增渠道商库存
        /// </summary>
        /// <param name="data">库存表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<bool>> AddStock([FromBody]DistributorStockAddForm data)
        {
            if (data == null) return ParameterNull<bool>();
            return await _distributorMngService.AddStock(data);
        }

        /// <summary>
        /// 渠道商库存更新记录
        /// </summary>
        /// <param name="id">渠道商ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ApiResult<List<DistributorStockChangeListDto>>> StockChangeList(long id)
        {
            if (id <= 0) return ParameterNull<List<DistributorStockChangeListDto>>();
            var rst = await _distributorMngService.GetStockChangeList(id);
            return Success(rst);
        }

        /// <summary>
        /// 渠道商商品订单明细
        /// </summary>
        /// <param name="id">渠道商记录ID</param>
        /// <param name="chancode">渠道码</param>
        /// <returns></returns>
        [HttpGet("{id}/{chancode}")]
        public async Task<ApiResult<List<DistributorStockConsumeListDto>>> ProductAllConsumeList(long id, string chancode)
        {
            if (id <= 0 || string.IsNullOrEmpty(chancode)) return ParameterNull<List<DistributorStockConsumeListDto>>();
            var rst = await _distributorMngService.GetProdConsumeList(id, chancode);
            return Success(rst);
        }

        /// <summary>
        /// 渠道商商品订单明细
        /// </summary>
        /// <param name="listPager">分页信息</param>
        /// <param name="prodno">商品编码</param>
        /// <param name="chancode">渠道码</param>
        /// <returns></returns>
        [HttpPost("{prodno}/{chancode}")]
        public async Task<ApiResult<ListPageDto<DistributorStockConsumeListDto>>> ProductConsumeList([FromBody]ListPageForm listPager, string prodno, string chancode)
        {
            if (listPager == null || listPager.Pager == null) return ParameterNull<ListPageDto<DistributorStockConsumeListDto>>();
            if (listPager.Pager.PageSize == 0) listPager.Pager.PageSize = Config.DefaultListPageSize;
            if (listPager.Pager.PageNumber == 0) listPager.Pager.PageNumber = 1;
            if (string.IsNullOrEmpty(prodno) || string.IsNullOrEmpty(chancode)) return ParameterNull<ListPageDto<DistributorStockConsumeListDto>>();
            var rst = await _distributorMngService.GetProdConsumeList(listPager, prodno, chancode);
            return Success(rst);
        }

        /// <summary>
        /// 渠道商合同列表
        /// </summary>
        /// <param name="listPager">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<ListPageDto<DistributorContactListDto>>> ContactList([FromBody]ListPageForm listPager)
        {
            if (listPager == null || listPager.Pager == null) return ParameterNull<ListPageDto<DistributorContactListDto>>();
            if (listPager.Pager.PageSize == 0) listPager.Pager.PageSize = Config.DefaultListPageSize;
            if (listPager.Pager.PageNumber == 0) listPager.Pager.PageNumber = 1;
            var rst = await _distributorMngService.GetContactList(listPager);
            return Success(rst);
        }

        /// <summary>
        /// 创建合同
        /// </summary>
        /// <param name="data">合同表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<long>> CreateContact([FromBody]DistributorContCreateForm data)
        {
            if (data == null) return ParameterNull<long>();
            return await _distributorMngService.CreateContact(data);
        }

        /// <summary>
        /// 修改合同
        /// </summary>
        /// <param name="data">合同表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<int>> UpdateContact([FromBody]DistributorContUpdateForm data)
        {
            if (data == null) return ParameterNull<int>();
            return await _distributorMngService.UpdateContact(data);
        }

        /// <summary>
        /// 删除合同
        /// </summary>
        /// <param name="data">合同表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<bool>> DeleteContact([FromBody]DistributorContDeleteForm data)
        {
            if (data == null) return ParameterNull<bool>();
            return await _distributorMngService.DeleteContact(data);
        }

        /// <summary>
        /// 得到合同详细信息
        /// </summary>
        /// <param name="id">合同记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ApiResult<DistributorContactInfoDto>> ContactInfo(long id)
        {
            if (id <= 0) return ParameterNull<DistributorContactInfoDto>();
            var rst = await _distributorMngService.GetContactInfo(id);
            return Success(rst);
        }

        /// <summary>
        /// 合同内容列表
        /// </summary>
        /// <param name="listPager">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<ListPageDto<DistributorContactContentListDto>>> ContactContentList([FromBody]ListPageForm listPager)
        {
            if (listPager == null || listPager.Pager == null) return ParameterNull<ListPageDto<DistributorContactContentListDto>>();
            if (listPager.Pager.PageSize == 0) listPager.Pager.PageSize = Config.DefaultListPageSize;
            if (listPager.Pager.PageNumber == 0) listPager.Pager.PageNumber = 1;
            var rst = await _distributorMngService.GetContactContentList(listPager);
            return Success(rst);
        }

        /// <summary>
        /// 合同内容列表
        /// </summary>
        /// <param name="id">合同记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ApiResult<List<DistributorContactContentListDto>>> ContactContents(long id)
        {
            if (id <= 0) return ParameterNull<List<DistributorContactContentListDto>>();
            var rst = await _distributorMngService.GetContactContents(id);
            return Success(rst);
        }

        /// <summary>
        /// 创建合同内容
        /// </summary>
        /// <param name="data">合同内容表单</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<long>> CreateContactContent([FromBody]DistributorContContCreateForm data)
        {
            if (data == null) return ParameterNull<long>();
            return await _distributorMngService.CreateContactContent(data);
        }

        /// <summary>
        /// 修改合同内容
        /// </summary>
        /// <param name="data">合同内容表单</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<bool>> UpdateContactContent([FromBody]DistributorContContUpdateForm data)
        {
            if (data == null) return ParameterNull<bool>();
            return await _distributorMngService.UpdateContactContent(data);
        }

        /// <summary>
        /// 删除合同内容
        /// </summary>
        /// <param name="data">合同内容表单</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<bool>> DeleteContactContent([FromBody]DistributorContContDeleteForm data)
        {
            if (data == null) return ParameterNull<bool>();
            return await _distributorMngService.DeleteContactContent(data);
        }
    }
}
