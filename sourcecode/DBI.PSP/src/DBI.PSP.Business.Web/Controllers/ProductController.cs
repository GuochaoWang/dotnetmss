/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.BusinessManage.Api.Controllers.v1;
using DBI.PSP.Business.Web.Services.Interfaces;
using DBI.PSP.Business.Web.Models.Dto;
using WebApiComm.Models;
using DBI.PSP.Business.Web.Filters;

namespace DBI.PSP.Business.Web.Controllers
{
    [AuthenFilter]
    public class ProductController : BaseController
    {
        private IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        /// <summary>
        /// 得到商品列表
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public async Task<ApiResult<List<ProductDto>>> List()
        {
            var rst = await _productService.List();
            return Success(rst);
        }

        /// <summary>
        /// 得到购买方式列表
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public async Task<ApiResult<List<ChargeModeDto>>> ChargeModeList()
        {
            var rst = await _productService.ChargeModeList();
            return Success(rst);
        }
    }
}
