using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.Business.Web.Services.Interfaces;
using DBI.PSP.Business.Web.Filters;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DBI.PSP.Business.Web.Controllers
{
    [AuthenFilter]
    public class DistributorCustomController : Controller
    {
        private IDistributorCustomerService _distorCusService;

        public DistributorCustomController(IDistributorCustomerService distorCusService)
        {
            _distorCusService = distorCusService;
        }

        
    }
}
