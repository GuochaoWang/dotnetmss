/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.Business.Web.Services.Interfaces;
using DBI.PSP.BusinessManage.Api.Controllers.v1;
using DBI.PSP.Business.Web.ViewModels;
using WebApiComm.Models;
using DBI.PSP.Business.Web.Models;

namespace DBI.PSP.Business.Web.Controllers
{
    public class AccountController : BaseController
    {
        private IBusinessManageService _businessMngService;

        public AccountController(IBusinessManageService businessMngService)
        {
            _businessMngService = businessMngService;
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="data">登录表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<AccountViewModel>> Login([FromBody]LoginForm data)
        {
            return await _businessMngService.Login(data.Username, data.Password);
        }
    }
}
