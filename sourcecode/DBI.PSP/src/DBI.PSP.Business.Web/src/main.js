import Vue from 'vue';
import App from './App';
import router from './router';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';    // 默认主题
// import '../static/css/theme-green/index.css';       // 浅绿色主题
import consts from './config/consts.js';
import webapi from './api/webapi.js';
import store from './store';
import curcyfmt from 'currency-formatter';
import "babel-polyfill";

Vue.use(ElementUI);
Vue.prototype.$consts = consts;
Vue.prototype.$webapi = webapi;
Vue.prototype.$curcyfmt = curcyfmt;

Vue.filter('currency', formatNumberAsCNY);

new Vue({
    router,
    store,
    curcyfmt,
    render: h => h(App)
}).$mount('#app');

router.beforeEach((to, from, next) => {
    if (to.matched.some(m => m.meta.auth)) {
        // 对路由进行验证
        //console.log("登录状态："+store.getters.logined);
        if (store.getters.logined === consts.DEFAULT_LOGINED &&
            store.getters.token) {
            // 已经登陆
            // 正常跳转到你设置好的页面
            next();
        } else {
            // 未登录则跳转到登陆界面，query:{ Rurl: to.fullPath}表示把当前路由信息传递过去方便登录后跳转回来；
            next({ path: '/login', query: { Rurl: to.fullPath } });
        }
    } else {
        next();
    }
});

function formatNumberAsCNY(value) {
    if (!value) value = 0;
    value = Number(value);
    return curcyfmt.format(value, { code: 'CNY' });
}
