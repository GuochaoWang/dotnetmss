import Vue from 'vue';
import vuex from 'vuex';

Vue.use(vuex);

export default new vuex.Store({
    state: {
        logined: '',
        token: '',
        seledDistName: '',
        username: '',
        truename: '',
        tempval: '',
    },
    getters: {
        logined: state => {
            if (state.logined === '') {
                var val = sessionStorage.getItem("bm_logined");
                return !val ? '' : val;
            }
            else return state.logined;
        },
        token: state => {
            if (state.token === '') {
                var val = sessionStorage.getItem("bm_token");
                return !val ? '' : val;
            } else return state.token
        },
        seledDistName: state => {
            if (state.seledDistName === '') {
                var val = sessionStorage.getItem("bm_seledDistName");
                return !val ? '' : val;
            } else return state.seledDistName
        },
        username: state => {
            if (state.username === '') {
                var val = sessionStorage.getItem("bm_username");
                return !val ? '' : val;
            } else return state.username
        },
        truename: state => {
            if (state.truename === '') {
                var val = sessionStorage.getItem("bm_truename");
                return !val ? '' : val;
            } else return state.truename
        },
        tempval: state => {
            if (state.tempval === '') {
                var val = sessionStorage.getItem("bm_tempval");
                return !val ? '' : val;
            } else return state.tempval
        },
    },
    mutations: {
        changeLogin(state, data) {
            sessionStorage.setItem("bm_logined", data);
            state.logined = data;
        },
        storageToken(state, data) {
            sessionStorage.setItem("bm_token", data);
            state.token = data;
        },
        storageSeledDistName(state, data) {
            sessionStorage.setItem("bm_seledDistName", data);
            state.seledDistName = data;
        },
        storageUsername(state, data) {
            sessionStorage.setItem("bm_username", data);
            state.username = data;
        },
        storageTruename(state, data) {
            sessionStorage.setItem("bm_truename", data);
            state.truename = data;
        },
        storageTempval(state, data) {
            sessionStorage.setItem("bm_tempval", data);
            state.tempval = data;
        },
        clear(state) {
            state.logined = '';
            state.token = '';
            state.seledDistName = '';
            state.username = '';
            state.truename = '';
            state.tempval = '';

            sessionStorage.clear();
        },
    }
})
