import axios from 'axios';
import store from '../store';
import consts from '../config/consts.js';

var isProduction = process.env.NODE_ENV === 'production'
const API_BASE_URL = isProduction ? '/api/v1' : 'http://localhost:64650/api/v1';

const ERROR_HTTP_REQ = { success: false, message: "网络异常，请稍后重试" };

const instance = axios.create({
    baseURL: API_BASE_URL,
    timeout: 10000,
    headers: {
        'content-type': 'application/json',
    }
});

// http request 请求拦截器，有token值则配置上token值
instance.interceptors.request.use(
    config => {
        var getTimestamp = new Date().getTime();
        if (config.url.indexOf("?") > -1) {
            config.url += "&t=" + getTimestamp
        } else {
            config.url += "?t=" + getTimestamp
        }
        config.headers.common['token'] = store.getters.token;
        return config;
    },
    err => {
        return Promise.reject(err);
    }
);

function httpPost(url, data, cb, chklogin) {
    if (!(chklogin === false)) {
        if (!checkLogin()) return;
    }
    instance.post(url, data).then(rst => {
        cb(rst.data);
    }, error => {
        console.error(error);
        cb(ERROR_HTTP_REQ);
    });
}

function httpGet(url, cb, chklogin) {
    if (!(chklogin === false)) {
        if (!checkLogin()) return;
    }
    instance.get(url).then(rst => {
        cb(rst.data);
    }, error => {
        console.error(error);
        cb(ERROR_HTTP_REQ);
    });
}

function checkLogin() {
    //console.log("login: " + store.getters.logined);
    if (store.getters.logined == consts.DEFAULT_LOGINED) {
        return true;
    }
    return false;
}

export default {
    product: {
        list: function (callback) {
            var url = "/product/list";
            httpGet(url, callback);
        },
        chargeModeList: function (callback) {
            var url = "/product/chargemodelist";
            httpGet(url, callback);
        }
    },
    acccount: {
        login: function (username, password, callback) {
            var url = "/account/login";
            var data = {
                Username: username,
                Password: password
            };
            httpPost(url, data, callback, false);
        }
    },
    distributor: {
        list: function (pageSize, pageNumber, keyword, callback) {
            var url = "/distributor/list";
            var data = {
                Pager: {
                    PageSize: pageSize,
                    PageNumber: pageNumber
                },
                Keyword: keyword
            };
            httpPost(url, data, callback);
        },
        create: function (formData, callback) {
            var url = "/distributor/create";
            httpPost(url, formData, callback);
        },
        update: function (formData, callback) {
            var url = "/distributor/update";
            httpPost(url, formData, callback);
        },
        resetPassword: function (formData, callback) {
            var url = "/distributor/resetpassword";
            httpPost(url, formData, callback);
        },
        getInfo: function (id, callback) {
            var url = "/distributor/info/" + id;
            httpGet(url, callback);
        },
        getNameList: function (top, kw, callback) {
            var url = "/distributor/namelist/";
            if (kw === "") url += top;
            else url += top + "/" + kw;
            httpGet(url, callback);
        },
        getAsset: function (id, callback) {
            var url = "/distributor/asset/" + id;
            httpGet(url, callback);
        },
        recharge: function (formData, callback) {
            var url = "/distributor/recharge";
            httpPost(url, formData, callback);
        },
        rechargeList: function (pageSize, pageNumber, pid, callback) {
            var url = "/distributor/rechargelist";
            var data = {
                Pager: {
                    PageSize: pageSize,
                    PageNumber: pageNumber
                },
                Keyword: "",
                ParentKey: pid
            };
            httpPost(url, data, callback);
        },
        productList: function (id, callback) {
            var url = "/distributor/productlist/" + id;
            httpGet(url, callback);
        },
        createProduct: function (formData, callback) {
            var url = "/distributor/createproduct";
            httpPost(url, formData, callback);
        },
        updateProduct: function (formData, callback) {
            var url = "/distributor/updateproduct";
            httpPost(url, formData, callback);
        },
        deleteProduct: function (formData, callback) {
            var url = "/distributor/deleteproduct";
            httpPost(url, formData, callback);
        },
        stockList: function (id, channelCode, callback) {
            var url = "/distributor/stocklist/" + id + "/" + channelCode;
            httpGet(url, callback);
        },
        addStock: function (formData, callback) {
            var url = "/distributor/addstock";
            httpPost(url, formData, callback);
        },
        stockChangeList: function (id, callback) {
            var url = "/distributor/stockchangelist/" + id;
            httpGet(url, callback);
        },
        productConsumeList: function (pageSize, pageNumber, prodNo, channelCode, kw, callback) {
            var url = "/distributor/productconsumelist/" + prodNo + "/" + channelCode;
            var data = {
                Pager: {
                    PageSize: pageSize,
                    PageNumber: pageNumber
                },
                Keyword: kw,
                ParentKey: ""
            };
            httpPost(url, data, callback);
        },
        contactList: function (pageSize, pageNumber, keyword, callback) {
            var url = "/distributor/contactlist";
            var data = {
                Pager: {
                    PageSize: pageSize,
                    PageNumber: pageNumber
                },
                Keyword: keyword,
                ParentKey: ""
            };
            httpPost(url, data, callback);
        },
        createContact: function (formData, callback) {
            var url = "/distributor/createcontact";
            httpPost(url, formData, callback);
        },
        updateContact: function (formData, callback) {
            var url = "/distributor/updatecontact";
            httpPost(url, formData, callback);
        },
        deleteContact: function (formData, callback) {
            var url = "/distributor/deletecontact";
            httpPost(url, formData, callback);
        },
        getContactInfo: function (id, callback) {
            var url = "/distributor/contactinfo/" + id;
            httpGet(url, callback);
        },
        contactContents: function (id, callback) {
            var url = "/distributor/contactcontents/" + id;
            httpGet(url, callback);
        },
        createContactContent: function (formData, callback) {
            var url = "/distributor/createcontactcontent";
            httpPost(url, formData, callback);
        },
        updateContactContent: function (formData, callback) {
            var url = "/distributor/updatecontactcontent";
            httpPost(url, formData, callback);
        },
        deleteContactContent: function (formData, callback) {
            var url = "/distributor/deleteContactcontent";
            httpPost(url, formData, callback);
        },
    }
}
