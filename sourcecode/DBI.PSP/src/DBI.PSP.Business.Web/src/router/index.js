import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/distributor/home',
            component: resolve => require(['../components/common/Home.vue'], resolve),
            meta: { auth: true },
            children:[
                {
                    path: '/distributor',
                    meta: { auth: true },
                    component: resolve => require(['../components/page/distributormng/list.vue'], resolve),
                },
                {
                    path: '/distributor/detail',
                    meta: { auth: true },
                    component: resolve => require(['../components/page/distributormng/detail.vue'], resolve),
                },
                {
                    path: '/distributor/detail/recharge/list',
                    meta: { auth: true },
                    component: resolve => require(['../components/page/distributormng/rechargeList.vue'], resolve),
                },
                {
                    path: '/distributor/detail/product/consulist',
                    meta: { auth: true },
                    component: resolve => require(['../components/page/distributormng/consProdList.vue'], resolve),
                },
            ],
        },
        {
            path: '/contact/home',
            component: resolve => require(['../components/common/Home.vue'], resolve),
            meta: { auth: true },
            children: [
                {
                    path: '/contact',
                    meta: { auth: true },
                    component: resolve => require(['../components/page/contactmng/list.vue'], resolve),
                },
                {
                    path: '/contact/create',
                    meta: { auth: true },
                    component: resolve => require(['../components/page/contactmng/create.vue'], resolve),
                },
                {
                    path: '/contact/view',
                    meta: { auth: true },
                    component: resolve => require(['../components/page/contactmng/view.vue'], resolve),
                },
                {
                    path: '/contact/edit',
                    meta: { auth: true },
                    component: resolve => require(['../components/page/contactmng/edit.vue'], resolve),
                }
            ]
        },
        {
            path: '/login',
            component: resolve => require(['../components/page/Login.vue'], resolve)
        },
    ]
})
