using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Distributor.Web.ViewModels
{
    public class AccountViewModel
    {
        public string TrueName { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    }
}
