/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.Distributor.Web.Services.Interfaces;
using DBI.PSP.Distributor.Web.ViewModels;
using DBI.PSP.Distributor.Web.Models;
using DBI.PSP.Distributor.Web.Models.Dto;
using WebApiComm.Models;
using Microsoft.AspNetCore.Http;
using DBI.PSP.Distributor.Web.Filters;
using DBI.PSP.Distributor.Web.Internal;

namespace DBI.PSP.Distributor.Api.Controllers
{
    public class AccountController : BaseController
    {
        private IAccountService _accountService;
        private ICustomerManageService _cusMngService;

        public AccountController(IHttpContextAccessor httpCtac, IAccountService accountService, ICustomerManageService cusMngService) : base(httpCtac)
        {
            _accountService = accountService;
            _cusMngService = cusMngService;
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="data">登录表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<AccountViewModel>> Login([FromBody]LoginForm data)
        {
            return await _accountService.Login(data);
        }

        /// <summary>
        /// 个人信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AuthenFilter]
        public async Task<ApiResult<DistributorInfoDto>> Info()
        {
            var rst = await _accountService.GetInfo(DistributorId);
            return Success(rst);
        }

        /// <summary>
        /// 已购商品列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AuthenFilter]
        public async Task<ApiResult<List<DistributorStockDto>>> ProductList()
        {
            var rst = await _cusMngService.GetStockList(DistributorId, ChannelCode);
            return Success(rst);
        }

        /// <summary>
        /// 商品订单列表
        /// </summary>
        /// <param name="listPager">分页信息</param>
        /// <param name="prodno">商品编码</param>
        /// <returns></returns>
        [HttpPost("{prodno}")]
        [AuthenFilter]
        public async Task<ApiResult<ListPageDto<DistributorStockConsumeListDto>>> ProductConsumeList([FromBody]ListPageForm listPager, string prodno)
        {
            if (listPager == null || listPager.Pager == null) return ParameterNull<ListPageDto<DistributorStockConsumeListDto>>();
            if (listPager.Pager.PageSize == 0) listPager.Pager.PageSize = Config.DefaultListPageSize;
            if (listPager.Pager.PageNumber == 0) listPager.Pager.PageNumber = 1;

            if (string.IsNullOrEmpty(prodno))
            {
                return Fail<ListPageDto<DistributorStockConsumeListDto>>(601, "参数错误");
            }

            var rst = await _cusMngService.GetProdConsumeList(listPager, prodno, ChannelCode);
            return Success(rst);
        }
    }
}
