/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.Distributor.Web.Services.Interfaces;
using WebApiComm.Models;
using DBI.PSP.Distributor.Web.Filters;
using Microsoft.AspNetCore.Http;
using ProductAndSvcServices.Lib.Models;

namespace DBI.PSP.Distributor.Api.Controllers
{
    [AuthenFilter]
    public class CustomerController : BaseController
    {
        private ICustomerManageService _cusMngService;

        public CustomerController(IHttpContextAccessor httpCtac, ICustomerManageService cusMngService) : base(httpCtac)
        {
            _cusMngService = cusMngService;
        }

        /// <summary>
        /// 门店列表
        /// </summary>
        /// <param name="listPage">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<ListPageDto<CompanyListDto>>> CompanyList([FromBody]ListPageForm listPage)
        {
            if (listPage == null ||
                listPage.Pager == null ||
                listPage.Pager.PageSize == 0 ||
                listPage.Pager.PageNumber == 0)
            {
                return Fail<ListPageDto<CompanyListDto>>(601, "参数错误");
            }

            var rst = await _cusMngService.GetCompanyList(listPage, DistributorId, ChannelCode);
            return Success(rst);
        }

        /// <summary>
        /// 门店详细信息
        /// </summary>
        /// <param name="id">门店ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ApiResult<CompanyInfo>> CompanyInfo(long id)
        {
            if (id <= 0)
            {
                return Fail<CompanyInfo>(601, "参数错误");
            }

            var rst = await _cusMngService.GetCompanyInfo(id);
            return Success(rst);
        }

        /// <summary>
        /// 门店订单列表
        /// </summary>
        /// <param name="listPage">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<ListPageDto<OrderListDto>>> CompanyOrderList([FromBody]ListPageForm listPage)
        {
            if (listPage == null ||
                listPage.Pager == null ||
                listPage.Pager.PageSize == 0 ||
                listPage.Pager.PageNumber == 0)
            {
                return Fail<ListPageDto<OrderListDto>>(601, "参数错误");
            }

            if (string.IsNullOrEmpty(listPage.ParentKey) || !long.TryParse(listPage.ParentKey, out long companyId))
            {
                return Fail<ListPageDto<OrderListDto>>(602, "企业Id不正确");
            }

            var rst = await _cusMngService.GetCompanyOrderList(listPage, DistributorId);
            return Success(rst);
        }
    }
}
