/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.Distributor.Web.Services.Interfaces;
using WebApiComm.Models;
using DBI.PSP.Distributor.Web.Models.Dto;
using DBI.PSP.Distributor.Web.Internal;
using DBI.PSP.Distributor.Web.Filters;
using Microsoft.AspNetCore.Http;

namespace DBI.PSP.Distributor.Api.Controllers
{
    [AuthenFilter]
    public class ContactController : BaseController
    {
        private IContactManageService _contMngService;

        public ContactController(IHttpContextAccessor httpCtac, IContactManageService contMngService) : base(httpCtac)
        {
            _contMngService = contMngService;
        }

        /// <summary>
        /// 合同列表
        /// </summary>
        /// <param name="listPager">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<ListPageDto<DistributorContactListDto>>> List([FromBody]ListPageForm listPager)
        {
            if (listPager == null || listPager.Pager == null) return ParameterNull<ListPageDto<DistributorContactListDto>>();
            if (listPager.Pager.PageSize == 0) listPager.Pager.PageSize = Config.DefaultListPageSize;
            if (listPager.Pager.PageNumber == 0) listPager.Pager.PageNumber = 1;
            var rst = await _contMngService.GetContactListByDistId(listPager, DistributorId);
            return Success(rst);
        }
    }
}
