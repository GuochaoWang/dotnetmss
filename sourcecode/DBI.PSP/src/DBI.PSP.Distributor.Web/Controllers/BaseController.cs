/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApiComm.Controllers;
using Microsoft.AspNetCore.Http;
using DBI.PSP.Distributor.Web.Internal;
using DBI.PSP.Distributor.Web.Exceptions;

namespace DBI.PSP.Distributor.Api.Controllers
{
    [Route("api/v1/[controller]/[action]")]
    public class BaseController : ApiBaseController
    {
        protected IHttpContextAccessor _httpContextAccesor;

        /// <summary>
        /// 渠道商记录ID
        /// </summary>
        public long DistributorId
        {
            get
            {
                var claim = _httpContextAccesor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ConstSetting.ClaimKey.USER_ID);
                if (claim == null) throw new AuthenFailException();
                var val = long.Parse(claim.Value);
                if (val <= 0) throw new AuthenFailException();
                return val;
            }
        }

        /// <summary>
        /// 渠道码
        /// </summary>
        public string ChannelCode
        {
            get
            {
                var claim = _httpContextAccesor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ConstSetting.ClaimKey.CHAN_CODE);
                if (claim == null) throw new AuthenFailException();
                if (string.IsNullOrEmpty(claim.Value)) throw new AuthenFailException();
                return claim.Value;
            }
        }

        public BaseController(IHttpContextAccessor httpContextAccesor)
        {
            _httpContextAccesor = httpContextAccesor;
        }
    }
}
