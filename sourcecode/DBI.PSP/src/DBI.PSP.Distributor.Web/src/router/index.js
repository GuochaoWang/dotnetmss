import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/account/home',
            component: resolve => require(['../components/common/Home.vue'], resolve),
            meta: { auth: true },
            children: [
                {
                    path: '/account',
                    meta: { auth: true },
                    component: resolve => require(['../components/page/account/info.vue'], resolve),
                },
            ],
        },
        {
            path: '/customer/home',
            component: resolve => require(['../components/common/Home.vue'], resolve),
            meta: { auth: true },
            children: [
                {
                    path: '/customer',
                    meta: { auth: true },
                    component: resolve => require(['../components/page/customermng/list.vue'], resolve),
                }
            ]
        },
        {
            path: '/contact/home',
            component: resolve => require(['../components/common/Home.vue'], resolve),
            meta: { auth: true },
            children: [
                {
                    path: '/contact',
                    meta: { auth: true },
                    component: resolve => require(['../components/page/contactmng/list.vue'], resolve),
                },
            ]
        },
        {
            path: '/login',
            component: resolve => require(['../components/page/Login.vue'], resolve)
        },
    ]
})
