import Vue from 'vue';
import vuex from 'vuex';

Vue.use(vuex);

export default new vuex.Store({
    state: {
        logined: '',
        token: '',
        username: '',
        truename: '',
    },
    getters: {
        logined: state => {
            if (state.logined === '') {
                var val = sessionStorage.getItem("dm_logined");
                return !val ? '' : val;
            }
            else return  state.logined
        },
        token: state => {
            if (state.token === '') {
                var val = sessionStorage.getItem("dm_token");
                return !val ? '' : val;
            }
            else return  state.token
        },
        username: state => {
            if (state.username === '') {
                var val = sessionStorage.getItem("dm_username");
                return !val ? '' : val;
            }
            else return  state.username
        },
        truename: state => {
            if (state.truename === '') {
                var val = sessionStorage.getItem("dm_truename");
                return !val ? '' : val;
            }
            else return state.truename
        },
    },
    mutations: {
        changeLogin(state, data) {
            sessionStorage.setItem("dm_logined", data); 
            state.logined = data;
        },
        storageToken(state, data) {
            sessionStorage.setItem("dm_token", data);
            state.token = data;
        },
        storageUsername(state, data) {
            sessionStorage.setItem("dm_username", data);
            state.username = data;
        },
        storageTruename(state, data) {
            sessionStorage.setItem("dm_truename", data);
            state.truename = data;
        },
        clear(state) {
            state.logined = '';
            state.token = '';
            state.username = '';
            state.truename = '';

            sessionStorage.clear();
        },
    }
})
