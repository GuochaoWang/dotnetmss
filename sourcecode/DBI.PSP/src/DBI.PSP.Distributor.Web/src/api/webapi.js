import axios from 'axios';
import store from '../store';
import consts from '../config/consts.js';

var isProduction = process.env.NODE_ENV === 'production'
const API_BASE_URL = isProduction ? '/api/v1' : 'http://localhost:64660/api/v1';

const ERROR_HTTP_REQ = { success: false, message: "网络异常，请稍后重试" };

const instance = axios.create({
    baseURL: API_BASE_URL,
    timeout: 10000,
    headers: {
        'content-type': 'application/json',
    }
});

// http request 请求拦截器，有token值则配置上token值
instance.interceptors.request.use(
    config => {
        var getTimestamp = new Date().getTime();
        if (config.url.indexOf("?") > -1) {
            config.url += "&t=" + getTimestamp
        } else {
            config.url += "?t=" + getTimestamp
        }
        config.headers.common['token'] = store.getters.token;
        return config;
    },
    err => {
        return Promise.reject(err);
    }
);

function httpPost(url, data, cb, chklogin) {
    if (!(chklogin === false)) {
        if (!checkLogin()) return;
    }
    instance.post(url, data).then(rst => {
        cb(rst.data);
    }, error => {
        console.error(error);
        cb(ERROR_HTTP_REQ);
    });
}

function httpGet(url, cb, chklogin) {
    if (!(chklogin === false)) {
        if (!checkLogin()) return;
    }
    instance.get(url).then(rst => {
        cb(rst.data);
    }, error => {
        console.error(error);
        cb(ERROR_HTTP_REQ);
    });
}

function checkLogin() {
    //console.log("login: " + store.getters.logined);
    if (store.getters.logined == consts.DEFAULT_LOGINED) {
        return true;
    }
    return false;
}

export default {
    product: {
        list: function (callback) {
            var url = "/product/list";
            httpGet(url, callback);
        },
        chargeModeList: function (callback) {
            var url = "/product/chargemodelist";
            httpGet(url, callback);
        }
    },
    acccount: {
        login: function (username, password, callback) {
            var url = "/account/login";
            var data = {
                Username: username,
                Password: password
            };
            httpPost(url, data, callback, false);
        },
        getInfo: function (callback) {
            var url = "/account/info";
            httpGet(url, callback);
        },
        productList: function (callback) {
            var url = "/account/productlist";
            httpGet(url, callback);
        },
        productConsumeList: function (pageSize, pageNumber, prodNo, kw, callback) {
            var url = "/account/productconsumelist/" + prodNo;
            var data = { Pager: {
                    PageSize: pageSize,
                    PageNumber: pageNumber
                },
                Keyword: kw
            };
            httpPost(url, data, callback);
        },
    },
    customer: {
        companyList: function (pageSize, pageNumber, kw, callback) {
            var url = "/customer/companylist";
            var data = {
                Pager: {
                    PageSize: pageSize,
                    PageNumber: pageNumber
                },
                Keyword: kw
            };
            httpPost(url, data, callback);
        },
        companyInfo: function (id, callback) {
            var url = "/customer/companyinfo/" + id;
            httpGet(url, callback);
        },
        companyOrderList: function (pageSize, pageNumber, kw, pk, callback) {
            var url = "/customer/companyorderlist";
            var data = {
                Pager: {
                    PageSize: pageSize,
                    PageNumber: pageNumber
                },
                Keyword: kw,
                ParentKey: pk,
            };
            httpPost(url, data, callback);
        },
    },
    contact: {
        contactList: function (pageSize, pageNumber, keyword, callback) {
            var url = "/contact/list";
            var data = {
                Pager: {
                    PageSize: pageSize,
                    PageNumber: pageNumber
                },
                Keyword: keyword,
                ParentKey: ""
            };
            httpPost(url, data, callback);
        },
    }
}
