/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using AopComm.Config;

namespace DBI.PSP.Distributor.Web.Internal
{
    /// <summary>
    /// 配置信息帮助类
    /// </summary>
    public static class Config
    {
        public static int DefaultListPageSize
        {
            get { return ConfigHelper.GetIntValue("DefaultListPageSize"); }
        }

        public static string ClientTokenKey
        {
            get { return ConfigHelper.GetStrValue("ClientTokenKey"); }
        }

        public static string ClientTokenSalt
        {
            get { return ConfigHelper.GetStrValue("ClientTokenSalt"); }
        }
    }
}
