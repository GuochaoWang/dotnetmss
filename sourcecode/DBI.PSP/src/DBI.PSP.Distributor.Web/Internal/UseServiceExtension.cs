/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.Distributor.Web.Services;
using DBI.PSP.Distributor.Web.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using ProductAndSvcServices.Lib;
using ProductAndSvcServices.Lib.Interface;

namespace DBI.PSP.Distributor.Web.Internal
{
    /// <summary>
    /// 业务服务依赖注册
    /// </summary>
    public static class UseServiceExtension
    {
        public static void UseService(this IServiceCollection services)
        {
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IContactManageService, ContactManageService>();
            services.AddScoped<ICustomerManageService, CustomerManageService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IProductAndSvcService, ProductAndSvcService>();
        }
    }
}
