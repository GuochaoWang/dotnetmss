/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using CommTools.Cryptology;

namespace DBI.PSP.Distributor.Web.Internal
{
    /// <summary>
    /// Token帮助类
    /// </summary>
    public class ClientToken
    {
        private const string SPLIT_CHAR = "%%";
        private const string FORMAT = "{0}%%{1}";

        public static string Create(string id, string chanCode)
        {
            return Create(new string[] { id, chanCode });
        }

        public static string Create(string[] arrVal)
        {
            var val = string.Join(",", arrVal);
            var salt = MD5Helper.Get(string.Format(FORMAT, val, Config.ClientTokenSalt));
            val = string.Format(FORMAT, salt, val);
            var rtn = DESHelper.Encrypt(val, Config.ClientTokenKey);
            return rtn.Replace('+', '-').Replace('/', '_').Replace('=', ',');
        }

        public static string[] GetVal(string token)
        {
            if (string.IsNullOrEmpty(token)) return null;
            token = token.Replace('-', '+').Replace('_', '/').Replace(',', '=');
            token = DESHelper.Decrypt(token, Config.ClientTokenKey);
            var splitIndex = token.IndexOf(SPLIT_CHAR);
            var salt = token.Substring(0, splitIndex);
            var val = token.Substring(splitIndex + 2);
            var saltNew = MD5Helper.Get(string.Format(FORMAT, val, Config.ClientTokenSalt));
            if (salt != saltNew) return null;
            return val.Split(',');
        }
    }
}
