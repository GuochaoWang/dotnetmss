/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

namespace DBI.PSP.Distributor.Web.Internal
{
    /// <summary>
    /// 系统常量定义
    /// </summary>
    public static class ConstSetting
    {
        public static class LocalCacheKey
        {
            public const string PRODUCT_ALL = "ProductAll";
            public const string PRODUCT_CHARGEMODE_ALL = "ProductChrgModeAll";
        }

        public static class ClaimKey
        {
            public const string USER_ID = "UserId";
            public const string CHAN_CODE = "ChanId";
        }
    }
}
