/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using CommTools.Cache;

namespace DBI.PSP.Distributor.Web.Internal
{
    /// <summary>
    /// 本地缓存帮助类
    /// </summary>
    public static class LocalCache
    {
        public static void Set<T>(T data, string key)
        {
            var cacheHelper = LocalCacheHelper<T>.Instance;
            cacheHelper.Set(data, key);
        }

        public static T Get<T>(string key)
        {
            var cacheHelper = LocalCacheHelper<T>.Instance;
            return cacheHelper.Get(key);
        }

        public static void Clear<T>(string key)
        {
            var cacheHelper = LocalCacheHelper<T>.Instance;
            cacheHelper.Remove(key);
        }
    }
}
