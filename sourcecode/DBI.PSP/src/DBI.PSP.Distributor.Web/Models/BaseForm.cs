using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Distributor.Web.Models
{
    public class BaseForm
    {
        public string Token { get; set; }
        [JsonIgnore]
        public string Key { get; set; }
    }
}
