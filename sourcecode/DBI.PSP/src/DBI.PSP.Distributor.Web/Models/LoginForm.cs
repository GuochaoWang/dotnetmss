using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Distributor.Web.Models
{
    public class LoginForm
    {
        [DisplayName("用户名")]
        [Required]
        [StringLength(50)]
        public string Username { get; set; }
        [DisplayName("密码")]
        [Required]
        [StringLength(128)]
        public string Password { get; set; }
    }
}
