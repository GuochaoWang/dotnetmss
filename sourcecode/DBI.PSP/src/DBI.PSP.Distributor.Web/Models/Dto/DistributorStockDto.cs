using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Distributor.Web.Models.Dto
{
    public class DistributorStockDto
    {
        public long ProductId { get; set; }
        public string ProductNo { get; set; }
        public DistributorProductDto PurchasedProduct { get; set; }
        public int StockOnHand { get; set; }
        public int StockAddUp { get; set; }
        public int StockConsumed { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
