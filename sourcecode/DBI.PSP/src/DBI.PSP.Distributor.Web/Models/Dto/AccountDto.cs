using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Distributor.Web.Models.Dto
{
    public class AccountDto
    {
        public long Id { get; set; }
        public string TrueName { get; set; }
        public string Username { get; set; }
        public string ChannelCode { get; set; }
        public string Token { get; set; }
    }
}
