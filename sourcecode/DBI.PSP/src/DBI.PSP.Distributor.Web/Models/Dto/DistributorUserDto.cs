using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Distributor.Web.Models.Dto
{
    public class DistributorUserDto
    {
        public long Id { get; set; }
        public string TrueName { get; set; }
    }
}
