using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Distributor.Web.Models.Dto
{
    [Serializable]
    public class DistributorAssetDto
    {
        public decimal Balance { get; set; }
    }
}
