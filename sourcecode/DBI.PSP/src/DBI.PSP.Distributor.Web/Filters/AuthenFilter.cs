/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.Distributor.Web.Exceptions;
using DBI.PSP.Distributor.Web.Internal;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System.Security.Claims;

namespace DBI.PSP.Distributor.Web.Filters
{
    /// <summary>
    /// 有效性验证拦截器
    /// </summary>
    public class AuthenFilter : ActionFilterAttribute
    {
        private const string HEAD_KEY_TOKEN = "token";
        
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            if (!actionContext.HttpContext.Request.Headers.TryGetValue(HEAD_KEY_TOKEN, out StringValues authHead)) throw new AuthenFailException();
            var token = string.Empty;
            token = authHead[0];
            if (string.IsNullOrEmpty(token)) throw new AuthenFailException();
            var arrVal = ClientToken.GetVal(token);
            if (arrVal == null || arrVal.Length != 2) throw new AuthenFailException();
            
            var claim = new ClaimsIdentity();
            claim.AddClaim(new Claim(ConstSetting.ClaimKey.USER_ID, arrVal[0]));
            claim.AddClaim(new Claim(ConstSetting.ClaimKey.CHAN_CODE, arrVal[1]));
            var myPrincipal = new ClaimsPrincipal(claim);
            actionContext.HttpContext.User = myPrincipal;
        }
    }
}
