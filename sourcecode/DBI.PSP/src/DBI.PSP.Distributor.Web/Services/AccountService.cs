/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.Distributor.Web.Services.Interfaces;
using System.Threading.Tasks;
using DBI.PSP.Distributor.Web.Models.Dto;
using DBI.PSP.Distributor.Web.ViewModels;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using Resilience.Http;
using DBI.PSP.Distributor.Web.Infrastructure;
using DBI.PSP.Distributor.Web.Models;
using WebApiComm.Models;
using DBI.PSP.Distributor.Web.Internal;

namespace DBI.PSP.Distributor.Web.Services
{
    public class AccountService : IAccountService
    {
        protected readonly IOptionsSnapshot<AppSettings> _settings;
        protected IHttpClient _apiClient;
        protected string _remoteServiceBaseUrl;
        protected IHttpContextAccessor _httpContextAccesor;
        private IProductService _productService;

        public AccountService(IOptionsSnapshot<AppSettings> settings,
            IHttpContextAccessor httpContextAccesor,
            IHttpClient httpClient,
            IProductService productService)
        {
            _settings = settings;
            _remoteServiceBaseUrl = _settings.Value.DistributorManageUrl;
            _httpContextAccesor = httpContextAccesor;
            _apiClient = httpClient;
            _productService = productService;
        }

        public async Task<DistributorInfoDto> GetInfo(long distId)
        {
            var url = API.URL.Account.GetInfo(_remoteServiceBaseUrl, distId);
            var rst = await API.Executor.Get<DistributorInfoDto>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<ApiResult<AccountViewModel>> Login(LoginForm data)
        {
            var url = API.URL.Account.Login(_remoteServiceBaseUrl);
            var rst = await API.Executor.Post<AccountDto, LoginForm>(_apiClient, url, data);
            if (!rst.Success)
            {
                return new ApiResult<AccountViewModel>()
                {
                    Success = rst.Success,
                    Code = rst.Code,
                    Message = rst.Message,
                    Data = null,
                };
            }
                
            var token = ClientToken.Create(rst.Data.Id.ToString(), rst.Data.ChannelCode);

            return new ApiResult<AccountViewModel>()
            {
                Success = rst.Success,
                Code = rst.Code,
                Message = rst.Message,
                Data = new AccountViewModel
                {
                    Username = rst.Data.Username,
                    TrueName = rst.Data.TrueName,
                    Token = token,
                },
            };
        }
    }
}
