/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.Distributor.Web.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBI.PSP.Distributor.Web.Models.Dto;
using Microsoft.Extensions.Options;
using Resilience.Http;
using Microsoft.AspNetCore.Http;
using ProductAndSvcServices.Lib.Interface;
using DBI.PSP.Distributor.Web.Infrastructure;
using ProductAndSvcServices.Lib.Models;
using WebApiComm.Models;

namespace DBI.PSP.Distributor.Web.Services
{
    public class CustomerManageService : ICustomerManageService
    {
        protected readonly IOptionsSnapshot<AppSettings> _settings;
        protected IHttpClient _apiClient;
        protected string _remoteServiceBaseUrl;
        protected IHttpContextAccessor _httpContextAccesor;
        private IProductAndSvcService _prodAndSvcService;
        private IProductService _productService;

        public CustomerManageService(IOptionsSnapshot<AppSettings> settings,
            IHttpContextAccessor httpContextAccesor,
            IHttpClient httpClient,
            IProductAndSvcService prodAndSvcService,
            IProductService productService)
        {
            _settings = settings;
            _remoteServiceBaseUrl = _settings.Value.DistributorManageUrl;
            _httpContextAccesor = httpContextAccesor;
            _apiClient = httpClient;
            _prodAndSvcService = prodAndSvcService;
            _productService = productService;
        }

        public async Task<List<DistributorProductDto>> GetProductList(long distId)
        {
            var url = API.URL.CustomerManage.GetProductList(_remoteServiceBaseUrl, distId);
            var rst = await API.Executor.Get<List<DistributorProductDto>>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<List<DistributorStockDto>> GetStockList(long distId, string channelCode)
        {
            var purchProdList = await GetProductList(distId);
            var url = API.URL.CustomerManage.GetStockList(_remoteServiceBaseUrl, distId);
            var rst = await API.Executor.Get<List<DistributorStockDto>>(_apiClient, url);
            if (rst.Success && rst.Data != null && purchProdList != null)
            {
                List<OrderStatListDto> stockCons = null;

                var pordNo = purchProdList.Where(t => t.CreateTime < DateTime.Now.AddMinutes(15)).Select(t => t.No).ToArray();
                var stockConsResp = await _prodAndSvcService.GetProductOrderStat(channelCode, pordNo);
                if (stockConsResp.Success && stockConsResp.Data != null)
                {
                    stockCons = stockConsResp.Data;
                }

                var prodIds = purchProdList.Select(t => t.ProductId).ToList();
                var stockList = rst.Data.Where(t => prodIds.Contains(t.ProductId)).ToList();

                stockList.ForEach(t =>
                {
                    OrderStatListDto stockConsData;
                    if (stockCons != null)
                    {
                        stockConsData = stockCons.FirstOrDefault(y => string.Compare(y.ProductNo, t.ProductNo) == 0);
                        if (stockConsData != null)
                        {
                            t.StockConsumed = stockConsData.SumQuantity;
                            t.StockOnHand = t.StockAddUp - stockConsData.SumQuantity;
                        }
                    }

                    t.PurchasedProduct = purchProdList.First(y => y.ProductId == t.ProductId);
                });

                return stockList;
            }
            return null;
        }

        public async Task<List<DistributorStockDto>> GetStockUsedStat(long distId, string channelCode)
        {
            var url = API.URL.CustomerManage.GetStockList(_remoteServiceBaseUrl, distId);
            var rst = await API.Executor.Get<List<DistributorStockDto>>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                var pordNo = rst.Data.Select(t => t.ProductNo).ToArray();
                var stockUseList = await _prodAndSvcService.GetProductOrderStat(channelCode, pordNo);
                if (stockUseList.Success && stockUseList.Data != null)
                {
                    rst.Data.ForEach(t =>
                    {
                        var stockUseData = stockUseList.Data.FirstOrDefault(y => string.Compare(y.ProductNo, t.ProductNo) == 0);
                        if (stockUseData != null)
                        {
                            t.StockOnHand = t.StockAddUp - stockUseData.SumQuantity;
                        }
                        else
                        {
                            t.StockOnHand = t.StockAddUp;
                        }
                    });
                }
                return rst.Data;
            }
            return null;
        }

        public async Task<ListPageDto<DistributorStockConsumeListDto>> GetProdConsumeList(ListPageForm listPager, string prodNo, string channelCode)
        {
            var rtn = new ListPageDto<DistributorStockConsumeListDto>
            {
                List = new List<DistributorStockConsumeListDto>(),
                Pager = new ListPager { TotalCount = 1 },
            };

            var products = await _productService.List();
            var stockConsListPage = await _prodAndSvcService.GetProductOrders(channelCode, listPager.Keyword, new string[] { prodNo }, listPager.Pager);
            if (stockConsListPage.Success && stockConsListPage.Data != null)
            {
                var stockConsData = stockConsListPage.Data.List;
                rtn.List = new List<DistributorStockConsumeListDto>();
                stockConsData.ForEach(t =>
                {
                    var prod = products.FirstOrDefault(y => string.Compare(y.No, t.ProductNo) == 0);
                    if (prod != null)
                    {
                        var item = CreateStockConsumeListDto(t);
                        item.ChargeModeId = prod.ChargeMode.Id;
                        item.ChargeModeName = prod.ChargeMode.Name;
                        rtn.List.Add(item);
                    }
                });
                rtn.Pager = stockConsListPage.Data.Pager;
            }
            return rtn;
        }

        public async Task<List<DistributorStockConsumeListDto>> GetProdConsumeList(long distId, string channelCode)
        {
            var url = API.URL.CustomerManage.GetProductList(_remoteServiceBaseUrl, distId);
            var rst = await API.Executor.Get<List<ProductDto>>(_apiClient, url);
            if (rst.Success && rst.Data != null)
            {
                var rtn = new List<DistributorStockConsumeListDto>();

                var pordNo = rst.Data.Select(t => t.No).ToArray();
                var products = await _productService.List();
                var stockConsList = await _prodAndSvcService.GetProductOrders(channelCode, "", pordNo);
                if (stockConsList.Success && stockConsList.Data != null)
                {
                    rst.Data.ForEach(t =>
                    {
                        var stockConsData = stockConsList.Data.List.FirstOrDefault(y => string.Compare(y.ProductNo, t.No) == 0);
                        var prod = products.FirstOrDefault(y => string.Compare(y.No, t.No) == 0);
                        if (stockConsData != null && prod != null)
                        {
                            var item = CreateStockConsumeListDto(stockConsData);
                            item.ChargeModeId = prod.ChargeMode.Id;
                            item.ChargeModeName = prod.ChargeMode.Name;
                            rtn.Add(item);
                        }
                    });
                }
                return rtn;
            }
            return null;
        }

        public async Task<ListPageDto<CompanyListDto>> GetCompanyList(ListPageForm listPager, long distId, string channelCode)
        {
            var purchProdList = await GetProductList(distId);
            if (purchProdList == null) return null;

            var pordNo = purchProdList.Select(t => t.No).ToArray();
            var rst = await _prodAndSvcService.GetCompanies(channelCode, listPager.Keyword, pordNo, listPager.Pager);

            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<CompanyInfo> GetCompanyInfo(long id)
        {
            var rst = await _prodAndSvcService.GetCompanyInfo(id);

            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }

            return null;
        }

        public async Task<ListPageDto<OrderListDto>> GetCompanyOrderList(ListPageForm listPager, long distId)
        {
            var purchProdList = await GetProductList(distId);
            if (purchProdList == null) return null;

            var pordNo = purchProdList.Select(t => t.No).ToArray();
            var rst = await _prodAndSvcService.GetCompanyOrders(long.Parse(listPager.ParentKey), listPager.Keyword, pordNo, listPager.Pager);

            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }

            return null;
        }

        private DistributorStockConsumeListDto CreateStockConsumeListDto(OrderListDto data)
        {
            return new DistributorStockConsumeListDto
            {
                CompanyName = data.CompanyName,
                OrderId = data.OrderId,
                OrderCode = data.OrderCode,
                ProductNo = data.ProductNo,
                ProductName = data.ProductName,
                Quantity = data.Quantity,
                BeginDate = data.BeginDate,
                EndDate = data.EndDate,
                CreateDate = data.CreateDate,
            };
        }
    }
}
