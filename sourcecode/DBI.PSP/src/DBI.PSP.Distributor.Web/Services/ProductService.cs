/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.Distributor.Web.Infrastructure;
using DBI.PSP.Distributor.Web.Internal;
using DBI.PSP.Distributor.Web.Models.Dto;
using DBI.PSP.Distributor.Web.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Resilience.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Distributor.Web.Services
{
    public class ProductService : IProductService
    {
        protected readonly IOptionsSnapshot<AppSettings> _settings;
        protected IHttpClient _apiClient;
        protected string _remoteServiceBaseUrl;
        protected IHttpContextAccessor _httpContextAccesor;

        public ProductService(IOptionsSnapshot<AppSettings> settings, IHttpContextAccessor httpContextAccesor, IHttpClient httpClient) //: base(settings, httpContextAccesor, httpClient)
        {
            _settings = settings;
            _remoteServiceBaseUrl = _settings.Value.ProductUrl;
            _httpContextAccesor = httpContextAccesor;
            _apiClient = httpClient;
        }

        public async Task<List<ProductDto>> List()
        {
            var cacheData = LocalCache.Get<List<ProductDto>>(ConstSetting.LocalCacheKey.PRODUCT_ALL);
            if (cacheData != null) return cacheData;

            var data = await GetListFromService();
            if (data == null) return null;

            LocalCache.Set(data, ConstSetting.LocalCacheKey.PRODUCT_ALL);
            return data;
        }

        public async Task<ProductDto> Get(long id)
        {
            var list = await List();
            if (list == null) return null;
            return list.FirstOrDefault(t => t.Id == id);
        }

        public async Task<List<ChargeModeDto>> ChargeModeList()
        {
            var cacheData = LocalCache.Get<List<ChargeModeDto>>(ConstSetting.LocalCacheKey.PRODUCT_CHARGEMODE_ALL);
            if (cacheData != null) return cacheData;

            var data = await GetChargeModeListFromService();
            if (data == null) return null;

            LocalCache.Set<List<ChargeModeDto>>(data, ConstSetting.LocalCacheKey.PRODUCT_CHARGEMODE_ALL);
            return data;
        }

        public async Task<ChargeModeDto> GetChargeMode(long id)
        {
            var list = await ChargeModeList();
            if (list == null) return null;
            return list.FirstOrDefault(t => t.Id == id);
        }

        private async Task<List<ProductDto>> GetListFromService()
        {
            var url = API.URL.Product.GetList(_remoteServiceBaseUrl);

            var rst = await API.Executor.Get<List<ProductDto>>(_apiClient, url);

            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }

            return null;
        }

        private async Task<List<ChargeModeDto>> GetChargeModeListFromService()
        {
            var url = API.URL.Product.GetChargeModeList(_remoteServiceBaseUrl);

            var rst = await API.Executor.Get<List<ChargeModeDto>>(_apiClient, url);

            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }

            return null;
        }
    }
}
