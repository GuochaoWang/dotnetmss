/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.Distributor.Web.Infrastructure;
using DBI.PSP.Distributor.Web.Models.Dto;
using DBI.PSP.Distributor.Web.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Resilience.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Distributor.Web.Services
{
    public class ContactManageService : IContactManageService
    {
        protected readonly IOptionsSnapshot<AppSettings> _settings;
        protected IHttpClient _apiClient;
        protected string _remoteServiceBaseUrl;
        protected IHttpContextAccessor _httpContextAccesor;

        public ContactManageService(IOptionsSnapshot<AppSettings> settings,
            IHttpContextAccessor httpContextAccesor,
            IHttpClient httpClient)
        {
            _settings = settings;
            _remoteServiceBaseUrl = _settings.Value.DistributorManageUrl;
            _httpContextAccesor = httpContextAccesor;
            _apiClient = httpClient;
        }

        public async Task<ListPageDto<DistributorContactListDto>> GetContactListByDistId(ListPageForm pager, long distId)
        {
            var url = API.URL.ContactManage.GetContactListByDistId(_remoteServiceBaseUrl, distId);
            var dataString = await _apiClient.PostAsync(url, pager);
            var rst = JsonConvert.DeserializeObject<ApiResult<ListPageDto<DistributorContactListDto>>>(dataString);
            if (rst.Success && rst.Data != null)
            {
                if (rst.Data.List.Count > 0)
                {
                    var pids = rst.Data.List.Select(t => t.Id).ToArray();
                    var contents = await GetContactContents(pids);
                    rst.Data.List.ForEach(t =>
                    {
                        t.Contents = contents.FindAll(y => y.ContactId == t.Id);
                    });
                }
                return rst.Data;
            }
            return null;
        }

        public async Task<ListPageDto<DistributorContactContentListDto>> GetContactContentList(ListPageForm pager)
        {
            var url = API.URL.ContactManage.GetContactContentList(_remoteServiceBaseUrl);
            var dataString = await _apiClient.PostAsync(url, pager);
            var rst = JsonConvert.DeserializeObject<ApiResult<ListPageDto<DistributorContactContentListDto>>>(dataString);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<List<DistributorContactContentListDto>> GetContactContents(long contId)
        {
            var url = API.URL.ContactManage.GetContactContents(_remoteServiceBaseUrl, contId);
            var dataString = await _apiClient.GetStringAsync(url);
            var rst = JsonConvert.DeserializeObject<ApiResult<List<DistributorContactContentListDto>>>(dataString);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }

        public async Task<List<DistributorContactContentListDto>> GetContactContents(long[] contIds)
        {
            var url = API.URL.ContactManage.GetContactContents(_remoteServiceBaseUrl, contIds);
            var dataString = await _apiClient.PostAsync(url, contIds);
            var rst = JsonConvert.DeserializeObject<ApiResult<List<DistributorContactContentListDto>>>(dataString);
            if (rst.Success && rst.Data != null)
            {
                return rst.Data;
            }
            return null;
        }
    }
}
