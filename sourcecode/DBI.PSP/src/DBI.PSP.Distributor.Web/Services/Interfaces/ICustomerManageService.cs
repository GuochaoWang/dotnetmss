using DBI.PSP.Distributor.Web.Models.Dto;
using DBI.PSP.Distributor.Web.ViewModels;
using ProductAndSvcServices.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Distributor.Web.Services.Interfaces
{
    public interface ICustomerManageService
    {
        Task<List<DistributorProductDto>> GetProductList(long distId);
        Task<List<DistributorStockDto>> GetStockList(long distId, string channelCode);
        Task<List<DistributorStockDto>> GetStockUsedStat(long distId, string channelCode);
        Task<ListPageDto<DistributorStockConsumeListDto>> GetProdConsumeList(ListPageForm listPager, string prodNo, string channelCode);
        Task<List<DistributorStockConsumeListDto>> GetProdConsumeList(long distId, string channelCode);

        Task<ListPageDto<CompanyListDto>> GetCompanyList(ListPageForm pager, long distId, string channelCode);
        Task<CompanyInfo> GetCompanyInfo(long id);
        Task<ListPageDto<OrderListDto>> GetCompanyOrderList(ListPageForm pager, long distId);
    }
}
