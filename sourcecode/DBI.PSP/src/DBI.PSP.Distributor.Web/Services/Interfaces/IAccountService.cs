using DBI.PSP.Distributor.Web.Models;
using DBI.PSP.Distributor.Web.Models.Dto;
using DBI.PSP.Distributor.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Distributor.Web.Services.Interfaces
{
    public interface IAccountService
    {
        Task<ApiResult<AccountViewModel>> Login(LoginForm data);
        Task<DistributorInfoDto> GetInfo(long distId);
    }
}
