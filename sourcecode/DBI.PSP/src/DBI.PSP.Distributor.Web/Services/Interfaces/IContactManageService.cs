using DBI.PSP.Distributor.Web.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Distributor.Web.Services.Interfaces
{
    public interface IContactManageService
    {
        Task<ListPageDto<DistributorContactListDto>> GetContactListByDistId(ListPageForm pager, long distId);
        Task<ListPageDto<DistributorContactContentListDto>> GetContactContentList(ListPageForm pager);
        Task<List<DistributorContactContentListDto>> GetContactContents(long contId);
        Task<List<DistributorContactContentListDto>> GetContactContents(long[] contIds);
    }
}
