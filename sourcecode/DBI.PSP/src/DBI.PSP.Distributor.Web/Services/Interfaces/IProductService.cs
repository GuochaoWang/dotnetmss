using DBI.PSP.Distributor.Web.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Distributor.Web.Services.Interfaces
{
    public interface IProductService
    {
        Task<List<ProductDto>> List();
        Task<List<ChargeModeDto>> ChargeModeList();
    }
}
