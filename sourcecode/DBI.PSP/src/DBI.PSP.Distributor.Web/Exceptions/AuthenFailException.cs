/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using System;

namespace DBI.PSP.Distributor.Web.Exceptions
{
    /// <summary>
    /// 认证失败异常
    /// </summary>
    public class AuthenFailException : Exception
    {
        private const string MSG_FAIL = "身份认证失败";

        public AuthenFailException() : base(MSG_FAIL)
        {
        }

        public AuthenFailException(string msg) : base(msg)
        {
        }
    }
}
