/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using Newtonsoft.Json;
using Resilience.Http;
using System;
using System.Threading.Tasks;
using WebApiComm.Models;

namespace DBI.PSP.Distributor.Web.Infrastructure
{
    /// <summary>
    /// 微服务接口帮助类
    /// </summary>
    public static class API
    {
        public static class Executor
        {
            public static async Task<ApiResult<TRst>> Get<TRst>(IHttpClient httpClient, string url)
            {
                var dataString = await httpClient.GetStringAsync(url);
                if (string.IsNullOrEmpty(dataString)) throw new Exception();
                return JsonConvert.DeserializeObject<ApiResult<TRst>>(dataString);
            }

            public static async Task<ApiResult<TRst>> Post<TRst, TInp>(IHttpClient httpClient, string url, TInp data)
            {
                var dataString = await httpClient.PostAsync(url, data);
                return JsonConvert.DeserializeObject<ApiResult<TRst>>(dataString);
            }
        }

        public static class URL
        {
            public static class Product
            {
                public static string GetList(string baseUri)
                {
                    return $"{baseUri}/v1/product/list";
                }

                public static string GetChargeModeList(string baseUri)
                {
                    return $"{baseUri}/v1/product/chargemodelist";
                }
            }

            public static class Account
            {
                public static string Login(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/account/login";
                }

                public static string GetInfo(string baseUri, long id)
                {
                    return $"{baseUri}/v1/distributormng/distributor/getinfo/{id}";
                }
            }

            public static class CustomerManage
            {
                public static string GetProductList(string baseUri, long id)
                {
                    return $"{baseUri}/v1/distributormng/distributor/productlist/{id}";
                }

                public static string GetStockList(string baseUri, long id)
                {
                    return $"{baseUri}/v1/distributormng/distributor/stocklist/{id}";
                }
            }

            public static class ContactManage
            {
                public static string GetContactList(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/contact/list";
                }

                public static string GetContactInfo(string baseUri, long id)
                {
                    return $"{baseUri}/v1/distributormng/contact/getinfo/{id}";
                }

                public static string GetContactListByDistId(string baseUri, long distId)
                {
                    return $"{baseUri}/v1/distributormng/contact/list/{distId}";
                }

                public static string GetContactContentList(string baseUri)
                {
                    return $"{baseUri}/v1/distributormng/contact/contentlist";
                }

                public static string GetContactContents(string baseUri, long contId)
                {
                    return $"{baseUri}/v1/distributormng/contact/contents/{contId}";
                }

                public static string GetContactContents(string baseUri, long[] contIds)
                {
                    return $"{baseUri}/v1/distributormng/contact/contents";
                }
            }
        }
    }
}
