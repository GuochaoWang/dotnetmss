﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Product.Api.Models.Dto
{
    [Serializable]
    public class ProductDto
    {
        public long Id { get; set; }
        public string No { get; set; }
        public string Name { get; set; }
        public ChargeModeDto ChargeMode { get; set; }
        public string Image { get; set; }
    }
}
