﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Product.Api.Models.Dto
{
    [Serializable]
    public class ChargeModeDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
