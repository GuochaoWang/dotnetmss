﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using DBI.PSP.Product.Api.Internal;
using Swashbuckle.AspNetCore.Swagger;
using AopComm.Filters;
using Autofac;
using Autofac.Extensions.DependencyInjection;

namespace DBI.PSP.Product.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddAuthorization()
            //     .AddAuthentication("Bearer")
            //     .AddIdentityServerAuthentication(options => 
            //     {
            //         options.Authority = "http://localhost:50000";
            //         options.RequireHttpsMetadata = false;

            //         options.ApiName = "api1";
            //     });

            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(UnknownExceptionFilter));
            });

            // 注册Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "DBI.PSP.Product.Api", Version = "v1" });
            });

            // 配置业务服务类依赖关系
            services.AddService();

            // 配置EventBus
            services.AddEventBus();

            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseAuthentication();

            // 配置Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "DBI.PSP.Product.Api");
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
