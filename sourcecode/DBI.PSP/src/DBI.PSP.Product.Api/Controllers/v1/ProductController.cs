﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.Product.Api.Services.Interfaces;
using DBI.PSP.Product.Api.Models.Dto;
using WebApiComm.Models;
using Microsoft.AspNetCore.Authorization;

namespace DBI.PSP.Product.Api.Controllers.v1
{
    [Authorize]
    public class ProductController : BaseController
    {
        private IProductService _service;

        public ProductController(IProductService service)
        {
            _service = service;
        }

        /// <summary>
        /// 得到商品列表
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public ApiResult<List<ProductDto>> List()
        {
            var data = _service.List();
            return Success(data);
        }

        /// <summary>
        /// 得到计费方式列表
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public ApiResult<List<ChargeModeDto>> ChargeModeList()
        {
            var data = _service.ChargeModeList();
            return Success(data);
        }

        /// <summary>
        /// 得到计费方式列表
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public ApiResult<bool> Edit()
        {
            var rst = _service.Update(new ProductDto { Id = 5 });
            return Success(rst);
        }
    }
}