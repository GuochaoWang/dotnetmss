﻿using DBI.PSP.Product.Api.IntegrationEvents.Events;
using DBI.PSP.Product.Api.Models.Dto;
using DBI.PSP.Product.Api.Services.Interfaces;
using EventBus.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Product.Api.Services
{
    public class ProductService : IProductService
    {
        private readonly IEventBus _eventBus;
        private List<ProductDto> _list = new List<ProductDto>()
            {
                new ProductDto
                {
                    Id = 1,
                    No = "101",
                    Name = "专业版纸质发票",
                    ChargeMode = new ChargeModeDto{ Id=1, Name="年/开票点" },
                    Image = "",
                },
                new ProductDto
                {
                    Id = 2,
                    No = "102",
                    Name = "标准版纸质发票",
                    ChargeMode = new ChargeModeDto{ Id=1, Name="年/开票点" },
                    Image = "",
                },
                new ProductDto
                {
                    Id = 3,
                    No = "103",
                    Name = "基础版纸质发票",
                    ChargeMode = new ChargeModeDto{ Id=1, Name="年/开票点" },
                    Image = "",
                },
                new ProductDto
                {
                    Id = 4,
                    No = "201",
                    Name = "专业版电子发票",
                    ChargeMode = new ChargeModeDto{ Id=1, Name="年/开票点" },
                    Image = "",
                },
                new ProductDto
                {
                    Id = 5,
                    No = "202",
                    Name = "标准版电子发票",
                    ChargeMode = new ChargeModeDto{ Id=1, Name="年/开票点" },
                    Image = "",
                },
                new ProductDto
                {
                    Id = 6,
                    No = "203",
                    Name = "基础版电子发票",
                    ChargeMode = new ChargeModeDto{ Id=1, Name="年/开票点" },
                    Image = "",
                },
                new ProductDto
                {
                    Id = 7,
                    No = "401",
                    Name = "发票查验",
                    ChargeMode = new ChargeModeDto{ Id=2, Name="次" },
                    Image = "",
                }
            };

        public ProductService(IEventBus eventBus)
        {
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        public List<ProductDto> List()
        {
            return _list;
        }

        public List<ChargeModeDto> ChargeModeList()
        {
            return new List<ChargeModeDto>()
            {
                new ChargeModeDto{ Id=1, Name="年/开票点" },
                new ChargeModeDto{ Id=2, Name="次" },
            };
        }

        public bool Update(ProductDto data)
        {
            var modifyitem = _list.FirstOrDefault(t => t.Id == data.Id);
            if (modifyitem == null) return false;
            
            // Publish integration event to update product read data model
            // with the new locations updated
            PublishProductUpdInteEvent(data);

            return true;
        }

        private void PublishProductUpdInteEvent(ProductDto data)
        {
            var @event = new ProductUpdInteEvent(data);
            _eventBus.Publish(@event);
        }
    }
}
