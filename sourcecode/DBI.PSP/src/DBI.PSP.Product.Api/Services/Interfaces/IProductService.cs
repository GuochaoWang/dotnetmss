﻿using DBI.PSP.Product.Api.Models;
using DBI.PSP.Product.Api.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Product.Api.Services.Interfaces
{
    public interface IProductService
    {
        List<ProductDto> List();
        List<ChargeModeDto> ChargeModeList();
        bool Update(ProductDto data);
    }
}
