﻿using AopComm.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Product.Api.Internal
{
    public static class Config
    {
        public static string EventBusSubsClientName
        {
            get { return ConfigHelper.GetStrValue("EventBusSubsClientName"); }
        }

        public static int EventBusRetryCount
        {
            get { return ConfigHelper.GetIntValue("EventBusRetryCount"); }
        }

        public static string EventBusConnection
        {
            get { return ConfigHelper.GetStrValue("EventBusConnection"); }
        }

        public static string EventBusUserName
        {
            get { return ConfigHelper.GetStrValue("EventBusUserName"); }
        }

        public static string EventBusPassword
        {
            get { return ConfigHelper.GetStrValue("EventBusPassword"); }
        }
    }
}
