﻿using DBI.PSP.Product.Api.Services;
using DBI.PSP.Product.Api.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Product.Api.Internal
{
    public static class AddServiceExtension
    {
        public static void AddService(this IServiceCollection services)
        {
            services.AddScoped<IProductService, ProductService>();
        }
    }
}
