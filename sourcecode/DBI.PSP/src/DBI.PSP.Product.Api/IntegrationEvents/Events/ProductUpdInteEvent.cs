﻿using DBI.PSP.Product.Api.Models.Dto;
using EventBus.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Product.Api.IntegrationEvents.Events
{
    public class ProductUpdInteEvent : IntegrationEvent
    {
        public ProductDto Product { get; set; }
        
        public ProductUpdInteEvent(ProductDto product)
        {
            Product = product;
        }
    }
}
