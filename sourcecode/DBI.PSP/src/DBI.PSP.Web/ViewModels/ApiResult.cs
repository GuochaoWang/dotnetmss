﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Web.ViewModels
{
    [Serializable]
    public class ApiResult<T>
    {
        public string Message { get; set; }
        public int Code { get; set; }
        public T Data { get; set; }
        public bool Success { get; set; }
    }
}
