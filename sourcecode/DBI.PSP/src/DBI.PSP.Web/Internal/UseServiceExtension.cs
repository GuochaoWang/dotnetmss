﻿using DBI.PSP.Web.Services;
using DBI.PSP.Web.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Web.Internal
{
    public static class UseServiceExtension
    {
        public static void UseService(this IServiceCollection services)
        {
            services.AddScoped<IAccountService, AccountService>();
        }
    }
}
