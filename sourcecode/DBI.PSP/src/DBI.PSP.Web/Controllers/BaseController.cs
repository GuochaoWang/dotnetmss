﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.Web.Models;
using DBI.PSP.Web.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DBI.PSP.Web.Controllers
{
    internal class BaseController : Controller
    {
        private const string MSG_SUCCESS = "执行成功";
        private const string MSG_INTERNAL_ERROR = "服务器内部错误";
        private const string MSG_BAB_REQUEST = "错误的请求";
        private const string MSG_NOT_FOUND = "未找到";
        private const string MSG_EXECUTE_FAILED = "执行失败";

        private const int CODE_SUCCESS = 200;
        private const int CODE_BAB_REQUEST = 401;
        private const int CODE_NOT_FOUND = 404;
        private const int CODE_INTERNAL_ERROR = 500;
        private const int CODE_EXECUTE_FAILED = 600;
        
        internal IActionResult BabRequest()
        {
            return Json(FailJsonResult(CODE_BAB_REQUEST, MSG_BAB_REQUEST));
        }

        internal IActionResult BabRequest(string msg)
        {
            return Json(FailJsonResult(CODE_BAB_REQUEST, msg));
        }

        internal new IActionResult NotFound()
        {
            return Json(FailJsonResult(CODE_NOT_FOUND, MSG_NOT_FOUND));
        }

        internal IActionResult InteranlError()
        {
            return Json(FailJsonResult(CODE_INTERNAL_ERROR, MSG_INTERNAL_ERROR));
        }

        internal IActionResult Fail()
        {
            return Json(FailJsonResult(CODE_EXECUTE_FAILED, MSG_EXECUTE_FAILED));
        }

        internal IActionResult Fail(int code, string msg)
        {
            return Json(FailJsonResult(code, msg));
        }

        internal IActionResult Success<T>(T data)
        {
            return Json(SuccessJsonResult(data));
        }

        internal IActionResult Success<T>(string message, T data)
        {
            return Json(SuccessJsonResult(data, message));
        }

        internal IActionResult Success()
        {
            return Success("");
        }

        private ApiResult<string> FailJsonResult(int code, string msg)
        {
            return new ApiResult<string>
            {
                Code = code,
                Message = msg,
                Success = false,
            };
        }

        private ApiResult<T> SuccessJsonResult<T>(T data)
        {
            return new ApiResult<T>
            {
                Code = CODE_SUCCESS,
                Message = MSG_SUCCESS,
                Success = true,
                Data = data
            };
        }

        private ApiResult<T> SuccessJsonResult<T>(T data, string msg)
        {
            var rst = SuccessJsonResult<T>(data);
            rst.Message = msg;
            return rst;
        }
    }
}
