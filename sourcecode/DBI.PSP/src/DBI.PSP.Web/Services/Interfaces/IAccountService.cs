﻿using DBI.PSP.Web.Models;
using DBI.PSP.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Web.Services.Interfaces
{
    public interface IAccountService
    {
        Task<DistributorViewModel> GetDistributor(long id);
    }
}
