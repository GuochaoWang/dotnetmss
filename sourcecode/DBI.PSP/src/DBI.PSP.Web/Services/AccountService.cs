﻿using DBI.PSP.Web.Infrastructure;
using DBI.PSP.Web.Models;
using DBI.PSP.Web.Services.Interfaces;
using DBI.PSP.Web.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Resilience.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Web.Services
{
    public class AccountService : IAccountService
    {
        protected readonly IOptionsSnapshot<AppSettings> _settings;
        protected IHttpClient _apiClient;
        protected string _remoteServiceBaseUrl;
        protected IHttpContextAccessor _httpContextAccesor;

        public AccountService(IOptionsSnapshot<AppSettings> settings, IHttpContextAccessor httpContextAccesor, IHttpClient httpClient) //: base(settings, httpContextAccesor, httpClient)
        {
            _settings = settings;
            _remoteServiceBaseUrl = _settings.Value.AccountUrl;
            _httpContextAccesor = httpContextAccesor;
            _apiClient = httpClient;
        }

        public async Task<DistributorViewModel> GetDistributor(long id)
        {
            //var token = GetToken();

            var getDistributorUri = API.URL.Account.GetDistributor(_remoteServiceBaseUrl, id);

            var dataString = await _apiClient.GetStringAsync(getDistributorUri, "");

            var rst = JsonConvert.DeserializeObject<ApiResult<DistributorDto>>(dataString);

            if (rst.Success && rst.Data != null)
            {
                return new DistributorViewModel { Name = rst.Data.Name };
            }

            return null;
        }
    }
}
