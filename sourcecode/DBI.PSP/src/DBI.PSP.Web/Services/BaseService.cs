﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Resilience.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Web.Services
{
    public class BaseService
    {
        protected readonly IOptionsSnapshot<AppSettings> _settings;
        protected IHttpClient _apiClient;
        protected string _remoteServiceBaseUrl;
        protected IHttpContextAccessor _httpContextAccesor;

        public BaseService(IOptionsSnapshot<AppSettings> settings, IHttpContextAccessor httpContextAccesor, IHttpClient httpClient)
        {
            _settings = settings;
            _remoteServiceBaseUrl = $"{_settings.Value.AccountUrl}/api/v1/account";
            _httpContextAccesor = httpContextAccesor;
            _apiClient = httpClient;
        }

        protected string GetToken()
        {
            return "";
        }
    }
}
