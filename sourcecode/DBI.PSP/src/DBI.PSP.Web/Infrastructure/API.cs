﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Web.Infrastructure
{
    public static class API
    {
        public static class URL
        {
            public static class Account
            {
                public static string GetDistributor(string baseUri, long distributorId)
                {
                    return $"{baseUri}/distributor/{distributorId}";
                }
            }

            public static class Locations
            {
                public static string CreateOrUpdateUserLocation(string baseUri)
                {
                    return baseUri;
                }
            }
        }
    }
}
