﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.Web.Models
{
    public class DistributorDto
    {
        public string Name { get; set; }
    }
}
