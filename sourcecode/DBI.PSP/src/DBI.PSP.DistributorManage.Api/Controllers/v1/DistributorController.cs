﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.DistributorManage.Api.Services.Interfaces;
using DBI.PSP.DistributorManage.Api.Models;
using DBI.PSP.DistributorManage.Api.Models.Dto;
using WebApiComm.Models;
using DBI.PSP.DistributorManage.Api.Models.Enum;
using WebApiComm.Extensions;

namespace DBI.PSP.DistributorManage.Api.Controllers.v1
{
    public class DistributorController : BaseController
    {
        private IDistributorService _service;

        public DistributorController(IDistributorService service)
        {
            _service = service;
        }

        /// <summary>
        /// 渠道商列表
        /// </summary>
        /// <param name="listPage">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<ListPageDto<DistributorListDto>> List([FromBody]ListPageForm listPage)
        {
            if (listPage == null ||
                listPage.Pager == null || 
                listPage.Pager.PageSize == 0 ||
                listPage.Pager.PageNumber == 0)
            {
                return Fail<ListPageDto<DistributorListDto>>(601, "参数错误");
            }
            
            var rtn = _service.GetList(listPage);
            return Success(rtn);
        }

        /// <summary>
        /// 创建渠道商
        /// </summary>
        /// <param name="data">渠道商表单信息</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<long> Create([FromBody]DistributorCreateForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<long>(601, validateError);
            }

            var rtn = _service.Create(data);
            if (rtn == 0)
            {
                return Fail<long>(701, "名称或渠道码已存在");
            }

            return Success(rtn);
        }

        /// <summary>
        /// 修改渠道商
        /// </summary>
        /// <param name="data">渠道商表单信息</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<string> Update([FromBody]DistributorUpdateForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<string>(601, validateError);
            }

            var rst = _service.Update(data);

            if (rst != EnumUpdRst.Success) return Fail<string>(rst.GetVal(), rst.GetDes());
            
            return Success();
        }

        /// <summary>
        /// 渠道商重置密码
        /// </summary>
        /// <param name="data">重置密码表单</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<bool> ResetPsd([FromBody]ResetPasswordForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<bool>(601, validateError);
            }

            var rtn = _service.ResetPassword(data.Id, data.Password);
            if (!rtn) Fail<long>(701, "重置密码失败");

            return Success(rtn);
        }

        /// <summary>
        /// 得到渠道商信息
        /// </summary>
        /// <param name="id">渠道商记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiResult<DistributorDto> Get(long id)
        {
            if (id <= 0) return Fail<DistributorDto>(601, "参数错误");

            var rst = _service.Get(id);
            if (rst == null) return Fail<DistributorDto>(701, "没有找到数据");
            return Success(rst);
        }

        /// <summary>
        /// 得到渠道商详细信息
        /// </summary>
        /// <param name="id">渠道商记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiResult<DistributorInfoDto> GetInfo(long id)
        {
            if (id <= 0) return Fail<DistributorInfoDto>(601, "参数错误");

            var rst = _service.GetInfo(id);
            if (rst == null) return Fail<DistributorInfoDto>(701, "没有找到数据");
            return Success(rst);
        }

        /// <summary>
        /// 得到最新渠道名称（前1000条记录）
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public ApiResult<List<SelectItemDto<long>>> NameList()
        {
            var rst = _service.GetNameList();
            if (rst == null) return Fail<List<SelectItemDto<long>>>(701, "没有找到数据");
            return Success(rst);
        }

        /// <summary>
        /// 得到渠道商资产信息
        /// </summary>
        /// <param name="id">渠道商记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiResult<AssetDto> GetAsset(long id)
        {
            if (id <= 0) return Fail<AssetDto>(601, "参数错误");

            var rst = _service.GetAsset(id);
            if (rst == null) return Fail<AssetDto>(701, "没有找到数据");
            return Success(rst);
        }

        /// <summary>
        /// 渠道商充值
        /// </summary>
        /// <param name="data">充值表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<bool> Recharge([FromBody]RechargeForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<bool>(601, validateError);
            }

            if (data.Amount == 0) return Fail<bool>(602, "充值金额不能为0");

            var rst = _service.Recharge(data);

            if (!rst) return Fail<bool>(701, "充值失败");

            return Success(rst);
        }

        /// <summary>
        /// 渠道商充值列表
        /// </summary>
        /// <param name="listPage">列表信息</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<ListPageDto<RechargeListDto>> RechargeList([FromBody]ListPageForm listPage)
        {
            if (listPage == null ||
                listPage.Pager == null ||
                listPage.Pager.PageSize == 0 ||
                listPage.Pager.PageNumber == 0)
            {
                return Fail<ListPageDto<RechargeListDto>>(601, "参数错误");
            }

            var distId = 0L;
            if (string.IsNullOrEmpty(listPage.ParentKey) || !long.TryParse(listPage.ParentKey, out distId))
            {
                return Fail<ListPageDto<RechargeListDto>>(602, "渠道商Id不正确");
            }

            var rst = _service.GetRechargeList(listPage);
            return Success(rst);
        }

        /// <summary>
        /// 渠道商已购商品列表
        /// </summary>
        /// <param name="id">渠道商记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiResult<List<ProductDto>> ProductList(long id)
        {
            if (id <= 0) return Fail<List<ProductDto>>(601, "参数错误");

            var rst = _service.GetProductList(id);
            if (rst == null) return Fail<List<ProductDto>>(701, "没有找到数据");
            return Success(rst);
        }

        /// <summary>
        /// 创建渠道商商品
        /// </summary>
        /// <param name="data">商品表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<long> CreateProduct([FromBody]ProductCreateForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<long>(601, validateError);
            }

            if (data.ProductId <= 0) return Fail<long>(601, "产品无效");
            if (data.ChargeModeId <= 0) return Fail<long>(601, "参数错误");
            if (data.Price < 0) return Fail<long>(601, "价格无效");
            if (data.PurchaseCount <= 0) return Fail<long>(601, "购买数量无效");

            var rst = _service.CreateProduct(data);

            if (rst == -1) return Fail<long>(701, "产品已添加");
            if (rst == 0) return Fail<long>(702, "执行失败");

            return Success(rst);
        }

        /// <summary>
        /// 修改渠道商商品
        /// </summary>
        /// <param name="data">商品表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<string> UpdateProduct([FromBody]ProductUpdateForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<string>(601, validateError);
            }

            var rst = _service.UpdateProduct(data);

            if (rst != EnumUpdProdRst.Success) return Fail<string>(rst.GetVal(), rst.GetDes());

            return Success();
        }

        /// <summary>
        /// 删除渠道商商品
        /// </summary>
        /// <param name="data">商品表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<bool> DeleteProduct([FromBody]ProductDeleteForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<bool>(601, validateError);
            }

            var rst = _service.DeleteProduct(data.Id, data.DistributorId, data.ProductId);

            if (!rst) return Fail<bool>(701, "删除失败");

            return Success(rst);
        }

        /// <summary>
        /// 渠道商商品库存列表
        /// </summary>
        /// <param name="id">渠道商记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiResult<List<StockDto>> StockList(long id)
        {
            if (id <= 0) return Fail<List<StockDto>>(601, "参数错误");

            var rst = _service.GetStockList(id);
            if (rst == null) return Fail<List<StockDto>>(701, "没有找到数据");
            return Success(rst);
        }

        /// <summary>
        /// 渠道商商品库存信息
        /// </summary>
        /// <param name="id">渠道商记录ID</param>
        /// <param name="prodId">商品记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}/{prodid}")]
        public ApiResult<StockDto> GetStock(long id, long prodId)
        {
            if (id <= 0 || prodId <= 0) return Fail<StockDto>(601, "参数错误");

            var rst = _service.GetStock(id, prodId);
            if (rst == null) return Fail<StockDto>(701, "没有找到数据");
            return Success(rst);
        }

        /// <summary>
        /// 新增渠道商商品库存
        /// </summary>
        /// <param name="data">库存表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<bool> AddStock([FromBody]StockAddForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<bool>(601, validateError);
            }

            var rst = _service.AddStock(data.DistributorId, data.ProductId, data.Amount);

            if (!rst) return Fail<bool>(701, "修改失败");

            return Success(rst);
        }

        /// <summary>
        /// 渠道商库存更新历史记录
        /// </summary>
        /// <param name="id">渠道商记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiResult<List<StockChangeListDto>> StockChangeList(long id)
        {
            if (id <= 0) return Fail<List<StockChangeListDto>>(601, "参数错误");

            var rst = _service.GetStockChangeList(id);
            if (rst == null) return Fail<List<StockChangeListDto>>(701, "没有找到数据");
            return Success(rst);
        }
    }
}