﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.DistributorManage.Api.Models;
using DBI.PSP.DistributorManage.Api.Models.Dto;
using DBI.PSP.DistributorManage.Api.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApiComm.Models;

namespace DBI.PSP.DistributorManage.Api.Controllers.v1
{
    public class ContactController : BaseController
    {
        private IContactService _service;

        public ContactController(IContactService service)
        {
            _service = service;
        }

        /// <summary>
        /// 合同列表
        /// </summary>
        /// <param name="listPage">列表分页</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<ListPageDto<ContactListDto>> List([FromBody]ListPageForm listPage)
        {
            if (listPage == null ||
                listPage.Pager == null ||
                listPage.Pager.PageSize == 0 ||
                listPage.Pager.PageNumber == 0)
            {
                return Fail<ListPageDto<ContactListDto>>(601, "参数错误");
            }

            var rtn = _service.GetList(listPage);
            return Success(rtn);
        }

        /// <summary>
        /// 创建合同
        /// </summary>
        /// <param name="data">合同表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<long> Create([FromBody]ContactCreateForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<long>(601, validateError);
            }

            if (string.IsNullOrEmpty(data.Title)) return Fail<long>(602, "企业名称不能为空");
            if (data.BeginTime >= data.EndTime) return Fail<long>(603, "开始日期截至日期不正确");

            var rtn = _service.Create(data);
            if (rtn == -1) return Fail<long>(701, "企业名称不存在");
            else if (rtn == -2) return Fail<long>(702, "合同编号已存在");
            else if (rtn == 0) return Fail<long>(702, "执行失败");
            
            return Success(rtn);
        }

        /// <summary>
        /// 修改合同
        /// </summary>
        /// <param name="data">合同表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<int> Update([FromBody]ContactUpdateForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<int>(601, validateError);
            }

            if (data.BeginTime >= data.EndTime)
            {
                return Fail<int>(602, "开始日期截至日期不正确");
            }

            var rtn = _service.Update(data);
            if (rtn == -1) return Fail<int>(701, "没有找到数据");
            else if (rtn == -2) return Fail<int>(702, "企业名称不合法");
            else if (rtn == -3) return Fail<int>(703, "合同编号已存在");

            return Success(rtn);
        }

        /// <summary>
        /// 根据合同记录ID删除合同
        /// </summary>
        /// <param name="data">合同表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<bool> Delete([FromBody]ContactDeleteForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<bool>(601, validateError);
            }

            var rtn = _service.Delete(data.Id);
            if (!rtn) return Fail<bool>(701, "删除失败");

            return Success(rtn);
        }

        /// <summary>
        /// 根据渠道商ID删除合同
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<bool> DeleteWithDistId([FromBody]ContactDeleteForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<bool>(601, validateError);
            }

            var rtn = _service.Delete(data.Id, data.DistributorId);
            if (!rtn) return Fail<bool>(701, "删除失败");

            return Success(rtn);
        }

        /// <summary>
        /// 得到合同信息
        /// </summary>
        /// <param name="id">合同记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiResult<ContactInfoDto> GetInfo(long id)
        {
            if (id <= 0) return Fail<ContactInfoDto>(601, "参数错误");

            var rst = _service.GetInfo(id);
            if (rst == null) return Fail<ContactInfoDto>(701, "没有找到数据");
            return Success(rst);
        }

        /// <summary>
        /// 渠道商合同列表
        /// </summary>
        /// <param name="listPage">分页信息</param>
        /// <param name="distId">渠道商ID</param>
        /// <returns></returns>
        [HttpPost("{distId}")]
        public ApiResult<ListPageDto<ContactListDto>> List([FromBody]ListPageForm listPage, long distId)
        {
            if (listPage == null ||
                listPage.Pager == null ||
                listPage.Pager.PageSize == 0 ||
                listPage.Pager.PageNumber == 0)
            {
                return Fail<ListPageDto<ContactListDto>>(601, "参数错误");
            }

            var rtn = _service.GetListByDistId(listPage, distId);
            return Success(rtn);
        }

        /// <summary>
        /// 合同内容列表
        /// </summary>
        /// <param name="listPage">分页信息</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<ListPageDto<ContactContentListDto>> ContentList([FromBody]ListPageForm listPage)
        {
            if (listPage == null ||
                listPage.Pager == null ||
                listPage.Pager.PageSize == 0 ||
                listPage.Pager.PageNumber == 0)
            {
                return Fail<ListPageDto<ContactContentListDto>>(601, "参数错误");
            }

            var contactId = 0L;
            if (string.IsNullOrEmpty(listPage.ParentKey) || !long.TryParse(listPage.ParentKey, out contactId))
            {
                return Fail<ListPageDto<ContactContentListDto>>(602, "合同Id不正确");
            }

            var rtn = _service.GetContentList(listPage);
            return Success(rtn);
        }

        /// <summary>
        /// 合同内容列表
        /// </summary>
        /// <param name="id">合同记录ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiResult<List<ContactContentListDto>> Contents(long id)
        {
            if (id <= 0)
            {
                return Fail<List<ContactContentListDto>>(601, "参数错误");
            }

            var rtn = _service.GetContentList(id);
            return Success(rtn);
        }

        /// <summary>
        /// 合同内容列表
        /// </summary>
        /// <param name="ids">合同记录IDs</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<List<ContactContentListDto>> Contents([FromBody]long[] ids)
        {
            if (ids == null)
            {
                return Fail<List<ContactContentListDto>>(601, "参数错误");
            }

            var rtn = _service.GetContentList(ids);
            return Success(rtn);
        }

        /// <summary>
        /// 创建合同内容
        /// </summary>
        /// <param name="data">合同内容表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<long> CreateContent([FromBody]ContactContentCreateForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<long>(601, validateError);
            }

            if (data.ContactId <= 0)
            {
                return Fail<long>(602, "合同Id不正确");
            }

            var rtn = _service.CreateContent(data);
            return Success(rtn);
        }

        /// <summary>
        /// 更新合同内容
        /// </summary>
        /// <param name="data">合同内容表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<bool> UpdateContent([FromBody]ContactContentUpdateForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<bool>(601, validateError);
            }

            if (data.ContactId <= 0)
            {
                return Fail<bool>(602, "合同Id不正确");
            }

            var rtn = _service.UpdateContent(data);
            if (!rtn) return Fail<bool>(701, "更新失败");

            return Success(rtn);
        }

        /// <summary>
        /// 删除合同内容
        /// </summary>
        /// <param name="data">合同内容表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<bool> DeleteContent([FromBody]ContactContentDeleteForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<bool>(601, validateError);
            }

            if (data.ContactId <= 0)
            {
                return Fail<bool>(602, "合同Id不正确");
            }

            var rtn = _service.DeleteContent(data.Id, data.ContactId);
            if (!rtn) return Fail<bool>(701, "删除失败");

            return Success(rtn);
        }
    }
}
