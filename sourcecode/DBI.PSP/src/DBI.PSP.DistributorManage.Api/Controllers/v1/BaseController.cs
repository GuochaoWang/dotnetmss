﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using Microsoft.AspNetCore.Mvc;
using WebApiComm.Controllers;

namespace DBI.PSP.DistributorManage.Api.Controllers.v1
{
    [Route("api/v1/distributormng/[controller]/[action]")]
    public class BaseController : ApiBaseController
    {
    }
}