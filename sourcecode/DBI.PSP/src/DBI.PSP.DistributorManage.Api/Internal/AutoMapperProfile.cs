﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using AutoMapper;
using DBI.PSP.DistributorManage.Api.Models;
using DBI.PSP.DistributorManage.Api.Models.Dto;
using DBI.PSP.DistributorManage.Domain;
using Rafy;
using WebApiComm.Models;

namespace DBI.PSP.DistributorManage.Api.Internal
{
    /// <summary>
    /// AutoMapper映射关系配置
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ListPager, PagingInfo>();
            CreateMap<PagingInfo, ListPager>();

            CreateMap<DistributorCreateForm, Distributor>();
            CreateMap<DistributorList, DistributorListDto>();
            CreateMap<Distributor, DistributorDto>();
            CreateMap<Asset, AssetDto>();
            CreateMap<PurchasedProduct, ProductDto>();
            CreateMap<StockChangeHistory, StockChangeListDto>();
            CreateMap<Inventory, StockDto>();
            CreateMap<ProductCreateForm, PurchasedProduct>();
            CreateMap<TransactionHistoryList, RechargeListDto>();

            CreateMap<ContactCreateForm, Contact>();
            CreateMap<ContactContentCreateForm, ContactContent>();
            CreateMap<ContactList, ContactListDto>();
            CreateMap<ContactContentList, ContactContentListDto>();
            CreateMap<Contact, ContactInfoDto>();
        }
    }
}
