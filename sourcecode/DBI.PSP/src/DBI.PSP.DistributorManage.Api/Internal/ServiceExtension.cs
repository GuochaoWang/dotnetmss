﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.DistributorManage.Api.Services;
using DBI.PSP.DistributorManage.Api.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace DBI.PSP.DistributorManage.Api.Internal
{
    /// <summary>
    /// 业务服务类依赖配置
    /// </summary>
    public static class ServiceExtension
    {
        public static void AddService(this IServiceCollection services)
        {
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IDistributorService, DistributorService>();
            services.AddScoped<IContactService, ContactService>();
        }
    }
}
