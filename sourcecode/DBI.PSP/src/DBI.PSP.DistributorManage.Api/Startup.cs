﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 胡庆访 20180410 10:40
 * 
*******************************************************/

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using DBI.PSP.DistributorManage.Api.Internal;
using AutoMapper;
using AopComm.Filters;

namespace DBI.PSP.DistributorManage.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            new DistributorManageDomainApp().Startup();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(UnknownExceptionFilter));
            });

            services.AddRouting(options =>
            {
                options.LowercaseUrls = true;
            });

            // 注册Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "DBI.PSP.DistributorManage.Api", Version = "v1" });
            });
            

            // 配置业务服务类依赖关系
            services.AddService();

            services.AddAutoMapper();

            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // 配置Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "DBI.PSP.DistributorManage.Api");
            });

            // 配置异常处理
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
