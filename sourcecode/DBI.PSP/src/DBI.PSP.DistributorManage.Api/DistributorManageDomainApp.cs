﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 胡庆访 20180410 10:40
 * 
*******************************************************/

using DBI.PSP.DistributorManage.Domain;
using Rafy;
using Rafy.DbMigration;
using Rafy.Domain;
using Rafy.Domain.ORM.DbMigration;

namespace DBI.PSP.DistributorManage.Api
{
    public class DistributorManageDomainApp : DomainApp
    {
        protected override void InitEnvironment()
        {
            RafyEnvironment.DomainPlugins.Add(new DistributorManageDomainPlugin());
            base.InitEnvironment();
        }

        protected override void OnRuntimeStarting()
        {
            base.OnRuntimeStarting();

            if (ConfigurationHelper.GetAppSettingOrDefault("DbiPspDomainApp_AutoUpdateDb", true))
            {
                var svc = ServiceFactory.Create<MigrateService>();
                svc.Options = new MigratingOptions
                {
                    //ReserveHistory = true,//ReserveHistory 表示是否需要保存所有数据库升级的历史记录
                    RunDataLossOperation = DataLossOperation.All,//要禁止数据库表、字段的删除操作，请使用 DataLossOperation.None 值。
                    Databases = new string[] { DistributorManageDomainPlugin.DbSettingName }
                };
                svc.Invoke();
            }
        }
    }
}
