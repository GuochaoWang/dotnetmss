﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorManage.Api.Models
{
    public class DistributorUpdateForm
    {
        [DisplayName("渠道商Id")]
        [Required]
        public long Id { get; set; }
        [DisplayName("渠道商名称")]
        [Required]
        [StringLength(80)]
        public string Name { get; set; }
        [DisplayName("帐号")]
        [Required]
        [StringLength(50)]
        public string Username { get; set; }
        [DisplayName("渠道码")]
        [Required]
        [StringLength(20)]
        public string ChannelCode { get; set; }
    }
}
