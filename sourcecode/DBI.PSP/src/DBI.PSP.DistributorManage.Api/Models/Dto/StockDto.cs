﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorManage.Api.Models.Dto
{
    [Serializable]
    public class StockDto
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductNo { get; set; }
        public int StockOnHand { get; set; }
        public int StockAddUp { get; set; }
        public int InitialStock { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
