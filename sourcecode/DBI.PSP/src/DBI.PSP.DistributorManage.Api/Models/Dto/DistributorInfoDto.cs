﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorManage.Api.Models.Dto
{
    [Serializable]
    public class DistributorInfoDto
    {
        public DistributorDto Distributor { get; set; }
        public AssetDto Asset { get; set; }
    }
}
