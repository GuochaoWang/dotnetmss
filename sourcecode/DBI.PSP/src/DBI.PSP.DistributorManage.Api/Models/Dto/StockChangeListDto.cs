﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorManage.Api.Models.Dto
{
    [Serializable]
    public class StockChangeListDto
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductNo { get; set; }
        public int Amount { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
