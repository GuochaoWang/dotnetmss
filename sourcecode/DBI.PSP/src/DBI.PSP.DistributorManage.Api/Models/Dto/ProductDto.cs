﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorManage.Api.Models.Dto
{
    [Serializable]
    public class ProductDto
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public string No { get; set; }
        public string Name { get; set; }
        public long ChargeModeId { get; set; }
        public string ChargeModeName { get; set; }
        public decimal Price { get; set; }
    }
}
