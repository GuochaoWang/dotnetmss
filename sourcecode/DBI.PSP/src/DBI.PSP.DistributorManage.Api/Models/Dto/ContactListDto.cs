﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorManage.Api.Models.Dto
{
    [Serializable]
    public class ContactListDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 客户名称
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        public string No { get; set; }

        /// <summary>
        /// 合同金额
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime { get; set; }
    }
}
