﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorManage.Api.Models.Dto
{
    [Serializable]
    public class RechargeListDto
    {
        public long Id { get; set; }
        public decimal Amount { get; set; }
        public DateTime DealTime { get; set; }
    }
}
