﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorManage.Api.Models.Dto
{
    [Serializable]
    public class DistributorListDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string ChannelCode { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
