﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorManage.Api.Models
{
    public class ContactDeleteForm
    {
        [DisplayName("Id")]
        [Required]
        public long Id { get; set; }
        [DisplayName("渠道商Id")]
        [Required]
        public long DistributorId { get; set; }
    }
}
