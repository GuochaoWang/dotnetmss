using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorManage.Api.Models.Enum
{
    public enum EnumUpdProdRst
    {
        [Description("成功")]
        Success = 200,
        [Description("修改失败")]
        Fail = 701,
        [Description("没有找到要修改的记录")]
        NotFound = 702,
        [Description("商品已经存在")]
        Exists = 703,
        [Description("购买数量无效")]
        StockInvalid = 704,
    }
}
