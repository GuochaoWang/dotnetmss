using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorManage.Api.Models.Enum
{
    public enum EnumUpdRst
    {
        [Description("成功")]
        Success = 1,
        [Description("修改失败")]
        Fail = 701,
        [Description("没有找到要修改的记录")]
        NotFound = 702,
        [Description("帐号已经存在")]
        UsernameExists = 703,
        [Description("渠道码已经存在")]
        ChanCodeExists = 704,
        [Description("帐号或渠道码已经存在")]
        Exists = 705,
    }
}
