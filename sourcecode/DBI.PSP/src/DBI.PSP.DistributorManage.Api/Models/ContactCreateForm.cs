﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorManage.Api.Models
{
    public class ContactCreateForm
    {
        public long DistributorId { get; set; }
        public string Title { get; set; }
        [DisplayName("合同编号")]
        [Required]
        [StringLength(50)]
        public string No { get; set; }
        [DisplayName("合同金额")]
        [Required]
        public decimal Price { get; set; }
        [DisplayName("开始日期")]
        [Required]
        public DateTime BeginTime { get; set; }
        [DisplayName("截止日期")]
        [Required]
        public DateTime EndTime { get; set; }

        public List<ContactContentCreateForm> Contents { get; set; }
    }
}
