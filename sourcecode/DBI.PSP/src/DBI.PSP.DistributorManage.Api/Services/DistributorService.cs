﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.DistributorManage.Api.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using DBI.PSP.DistributorManage.Domain;
using Rafy.Domain;
using Rafy;
using DBI.PSP.DistributorManage.Api.Models.Dto;
using DBI.PSP.DistributorManage.Api.Models;
using WebApiComm.Models;
using AutoMapper;
using CommTools.Cryptology;
using DBI.PSP.DistributorManage.Api.Models.Enum;

namespace DBI.PSP.DistributorManage.Api.Services
{
    public class DistributorService : IDistributorService
    {
        public long Create(DistributorCreateForm data)
        {
            data.Name = data.Name.Trim();
            data.Username = data.Username.Trim();

            var resp = RF.ResolveInstance<DistributorRepository>();
            var item = resp.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatchGroup(BinaryOperator.Or)
                {
                    new PropertyMatch(Distributor.ChannelCodeProperty, PropertyOperator.Equal, data.ChannelCode),
                    new PropertyMatch(Distributor.NameProperty, PropertyOperator.Equal, data.Name)
                }
            });
            if (item != null) return 0;

            data.ChannelCode = data.ChannelCode.ToUpper();
            data.Password = MD5Helper.Get(data.Password);

            var entity = Mapper.Map<Distributor>(data);
            entity.CreateTime = DateTime.Now;
            resp.Save(entity);
            return entity.Id;
        }

        public ListPageDto<DistributorListDto> GetList(ListPageForm listPage)
        {
            var pageInfo = Mapper.Map<PagingInfo>(listPage.Pager);
            pageInfo.IsNeedCount = true;
            var resp = RF.ResolveInstance<DistributorRepository>();
            var list = resp.GetList(pageInfo, listPage.Keyword).Concrete().ToList()
                .Select(t => Mapper.Map<DistributorListDto>(t))
                .ToList();
            return new ListPageDto<DistributorListDto>
            {
                List = list,
                Pager = Mapper.Map<ListPager>(pageInfo),
            };
        }

        public bool ResetPassword(long distId, string newPsd)
        {
            var resp = RF.ResolveInstance<DistributorRepository>();
            var data = resp.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatch(Distributor.IdProperty, PropertyOperator.Equal, distId),
            });
            if (data == null) return false;
            data.Password = MD5Helper.Get(newPsd);
            resp.Save(data);
            return true;
        }

        public EnumUpdRst Update(DistributorUpdateForm data)
        {
            data.Name = data.Name.Trim();
            data.Username = data.Username.Trim();

            var resp = RF.ResolveInstance<DistributorRepository>();

            var entity = resp.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatch(Distributor.IdProperty, PropertyOperator.Equal, data.Id),
            });
            if (entity == null) return EnumUpdRst.NotFound;

            if (entity.ChannelCode == data.ChannelCode && 
                entity.Username == data.Username &&
                entity.Name == data.Name)
            {
                return EnumUpdRst.Success;
            }

            if (entity.ChannelCode == data.ChannelCode && entity.Username == data.Username)
            {
                entity.Name = data.Name;
                resp.Save(entity);
                return EnumUpdRst.Success;
            }
            else if (entity.ChannelCode == data.ChannelCode && entity.Username != data.Username)
            {
                var item = resp.GetFirstBy(new CommonQueryCriteria()
                {
                    new PropertyMatch(Distributor.UsernameProperty, PropertyOperator.Equal, data.Username)
                });
                if (item != null) return EnumUpdRst.UsernameExists;

                entity.Name = data.Name;
                entity.Username = data.Username;
                resp.Save(entity);
                return EnumUpdRst.Success;
            }
            else if (entity.ChannelCode != data.ChannelCode && entity.Username == data.Username)
            {
                var item = resp.GetFirstBy(new CommonQueryCriteria()
                {
                    new PropertyMatch(Distributor.ChannelCodeProperty, PropertyOperator.Equal, data.ChannelCode),
                });
                if (item != null) return EnumUpdRst.ChanCodeExists;

                entity.Name = data.Name;
                entity.ChannelCode = data.ChannelCode;
                resp.Save(entity);
                return EnumUpdRst.Success;
            }
            else
            {
                var item = resp.GetFirstBy(new CommonQueryCriteria()
                {
                    new PropertyMatchGroup(BinaryOperator.Or)
                    {
                        new PropertyMatch(Distributor.ChannelCodeProperty, PropertyOperator.Equal, data.ChannelCode),
                        new PropertyMatch(Distributor.UsernameProperty, PropertyOperator.Equal, data.Username)
                    }
                });
                if (item != null) return EnumUpdRst.Exists;

                entity.Name = data.Name;
                entity.Username = data.Username;
                entity.ChannelCode = data.ChannelCode;
                resp.Save(entity);
                return EnumUpdRst.Success;
            }
        }

        public bool AddStock(long distId, long prodId, int amount)
        {
            var respProduct = RF.ResolveInstance<PurchasedProductRepository>();

            var qProd = new CommonQueryCriteria
            {
                new PropertyMatchGroup(BinaryOperator.And)
                {
                    new PropertyMatch(PurchasedProduct.DistributorIdProperty, PropertyOperator.Equal, distId),
                    new PropertyMatch(PurchasedProduct.IdProperty, PropertyOperator.Equal, prodId)
                }
            };

            var itemProd = respProduct.GetFirstBy(qProd);

            if (itemProd == null) return false;

            var respInve = RF.ResolveInstance<InventoryRepository>();
            var respStoc = RF.ResolveInstance<StockChangeHistoryRepository>();

            using (var tran = RF.TransactionScope(DistributorManageDomainPlugin.DbSettingName))
            {
                var qInve = new CommonQueryCriteria
                {
                    new PropertyMatchGroup(BinaryOperator.And) {
                        new PropertyMatch(Inventory.DistributorIdProperty, PropertyOperator.Equal, distId),
                        new PropertyMatch(Inventory.ProductIdProperty, PropertyOperator.Equal, prodId),
                    }
                };

                var itemStock = respInve.GetFirstBy(qInve);

                if (itemStock == null)
                {
                    var entity = new Inventory()
                    {
                        DistributorId = distId,
                        ProductId = prodId,
                        ProductName = itemProd.Name,
                        StockOnHand = 0,
                        StockAddUp = amount,
                        InitialStock = amount,
                        CreateTime = DateTime.Now
                    };
                    respInve.Save(entity);
                }
                else
                {
                    itemStock.StockAddUp += amount;
                    respInve.Save(itemStock);
                }
                
                var entityInve = new StockChangeHistory()
                {
                    DistributorId = distId,
                    ProductId = prodId,
                    ProductName = itemProd.Name,
                    Amount = amount,
                    CreateTime = DateTime.Now
                };
                respStoc.Save(entityInve);

                tran.Complete();
            }

            return true;
        }

        public DistributorDto Get(long id)
        {
            var resp = RF.ResolveInstance<DistributorRepository>();
            var data = resp.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatch(Distributor.IdProperty, PropertyOperator.Equal, id),
            });
            if (data == null) return null;
            return Mapper.Map<DistributorDto>(data);
        }

        public AssetDto GetAsset(long distId)
        {
            var resp = RF.ResolveInstance<AssetRepository>();
            var data = resp.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatch(Asset.DistributorIdProperty, PropertyOperator.Equal, distId),
            });
            if (data == null) return null;
            return Mapper.Map<AssetDto>(data);
        }

        public DistributorInfoDto GetInfo(long id)
        {
            var dist = Get(id);

            if (dist == null) return null;

            var asset = GetAsset(id);

            return new DistributorInfoDto()
            {
                Distributor = dist,
                Asset = asset,
            };
        }

        public List<SelectItemDto<long>> GetNameList()
        {
            var resp = RF.ResolveInstance<DistributorRepository>();
            return resp.GetNameList().Concrete().ToList()
                .Select(t =>
                {
                    return new SelectItemDto<long> { Id = t.Id, Value = t.Name };
                })
                .ToList();
        }

        public List<ProductDto> GetProductList(long distId)
        {
            var resp = RF.ResolveInstance<PurchasedProductRepository>();
            return resp.GetList(distId).Concrete().ToList()
                .Select(t => Mapper.Map<ProductDto>(t))
                .ToList();
        }

        public List<StockChangeListDto> GetStockChangeList(long distId)
        {
            var resp = RF.ResolveInstance<StockChangeHistoryRepository>();
            return resp.GetList(distId).Concrete().ToList()
                .Select(t => Mapper.Map<StockChangeListDto>(t))
                .ToList();
        }

        public List<StockDto> GetStockList(long distId)
        {
            var resp = RF.ResolveInstance<InventoryRepository>();
            return resp.GetList(distId).Concrete().ToList()
                .Select(t => Mapper.Map<StockDto>(t))
                .ToList();
        }

        public StockDto GetStock(long distId, long prodId)
        {
            var resp = RF.ResolveInstance<InventoryRepository>();

            var q = new CommonQueryCriteria
            {
                new PropertyMatchGroup(BinaryOperator.And) {
                    new PropertyMatch(PurchasedProduct.DistributorIdProperty, PropertyOperator.Equal, distId),
                    new PropertyMatch(PurchasedProduct.ProductIdProperty, PropertyOperator.Equal, prodId),
                }
            };

            var item = resp.GetFirstBy(q);
            return Mapper.Map<StockDto>(item);
        }

        public long CreateProduct(ProductCreateForm data)
        {
            var resp = RF.ResolveInstance<PurchasedProductRepository>();
            
            var item = resp.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatch(PurchasedProduct.DistributorIdProperty, PropertyOperator.Equal, data.DistributorId),
                new PropertyMatch(PurchasedProduct.ProductIdProperty, PropertyOperator.Equal, data.ProductId),
            });

            if (item != null) return -1;

            var rtn = 0L;
            
            var respInve = RF.ResolveInstance<InventoryRepository>();
            var respChag = RF.ResolveInstance<StockChangeHistoryRepository>();

            if (data.PurchaseCount > 0)
            {
                using (var tran = RF.TransactionScope(DistributorManageDomainPlugin.DbSettingName))
                {
                    var entity = Mapper.Map<PurchasedProduct>(data);
                    entity.CreateTime = DateTime.Now;
                    resp.Save(entity);
                    rtn = entity.Id;

                    var entityInve = new Inventory()
                    {
                        DistributorId = data.DistributorId,
                        ProductId = data.ProductId,
                        ProductNo = data.No,
                        ProductName = data.Name,
                        StockOnHand = data.PurchaseCount,
                        StockAddUp = data.PurchaseCount,
                        InitialStock = data.PurchaseCount,
                        CreateTime = DateTime.Now
                    };
                    respInve.Save(entityInve);

                    var entityChag = new StockChangeHistory()
                    {
                        DistributorId = data.DistributorId,
                        ProductId = data.ProductId,
                        ProductNo = data.No,
                        ProductName = data.Name,
                        Amount = data.PurchaseCount,
                        CreateTime = DateTime.Now
                    };
                    respChag.Save(entityChag);

                    tran.Complete();
                }
            }
            else
            {
                var entity = Mapper.Map<PurchasedProduct>(data);
                entity.CreateTime = DateTime.Now;
                resp.Save(entity);
                rtn = entity.Id;
            }
            
            return rtn;
        }

        public bool DeleteProduct(long id, long distId, long prodId)
        {
            var resp = RF.ResolveInstance<PurchasedProductRepository>();

            var q = new CommonQueryCriteria
            {
                new PropertyMatchGroup(BinaryOperator.And) {
                    new PropertyMatch(PurchasedProduct.IdProperty, PropertyOperator.Equal, id),
                    new PropertyMatch(PurchasedProduct.DistributorIdProperty, PropertyOperator.Equal, distId),
                    new PropertyMatch(PurchasedProduct.ProductIdProperty, PropertyOperator.Equal, prodId),
                }
            };

            var item = resp.GetFirstBy(q);

            if (item != null)
            {
                var respInve = RF.ResolveInstance<InventoryRepository>();

                using (var tran = RF.TransactionScope(DistributorManageDomainPlugin.DbSettingName))
                {
                    item.PersistenceStatus = PersistenceStatus.Deleted;
                    resp.Save(item);

                    var itemInve = respInve.GetFirstBy(new CommonQueryCriteria {
                        new PropertyMatchGroup(BinaryOperator.And) {
                            new PropertyMatch(Inventory.DistributorIdProperty, PropertyOperator.Equal, distId),
                            new PropertyMatch(Inventory.ProductIdProperty, PropertyOperator.Equal, prodId),
                    }});
                    itemInve.PersistenceStatus = PersistenceStatus.Deleted;
                    respInve.Save(itemInve);

                    tran.Complete();

                    return true;
                }
            }
            
            return false;
        }

        public EnumUpdProdRst UpdateProduct(ProductUpdateForm data)
        {
            var resp = RF.ResolveInstance<PurchasedProductRepository>();

            var q = new CommonQueryCriteria
            {
                new PropertyMatchGroup(BinaryOperator.And) {
                    new PropertyMatch(PurchasedProduct.IdProperty, PropertyOperator.Equal, data.Id),
                    new PropertyMatch(PurchasedProduct.DistributorIdProperty, PropertyOperator.Equal, data.DistributorId)
                }
            };

            var item = resp.GetFirstBy(q);

            if (item == null) return EnumUpdProdRst.NotFound;

            if (item.ProductId != data.ProductId)
            {
                q = new CommonQueryCriteria
                {
                    new PropertyMatchGroup(BinaryOperator.And) {
                        new PropertyMatch(PurchasedProduct.IdProperty, PropertyOperator.Equal, data.Id),
                        new PropertyMatch(PurchasedProduct.DistributorIdProperty, PropertyOperator.Equal, data.DistributorId),
                        new PropertyMatch(PurchasedProduct.ProductIdProperty, PropertyOperator.Equal, data.ProductId),
                    }
                };

                var itemExist = resp.GetFirstBy(q);
                if (itemExist != null) return EnumUpdProdRst.Exists;
            }

            if (data.PurchaseCount > 0)
            {
                var respInve = RF.ResolveInstance<InventoryRepository>();

                var qInve = new CommonQueryCriteria
                {
                    new PropertyMatchGroup(BinaryOperator.And) {
                        new PropertyMatch(Inventory.DistributorIdProperty, PropertyOperator.Equal, data.DistributorId),
                        new PropertyMatch(Inventory.ProductIdProperty, PropertyOperator.Equal, data.ProductId),
                    }
                };

                var entityInventory = respInve.GetFirstBy(qInve);

                if (entityInventory == null) return EnumUpdProdRst.StockInvalid;

                var respStoc = RF.ResolveInstance<StockChangeHistoryRepository>();

                using (var tran = RF.TransactionScope(DistributorManageDomainPlugin.DbSettingName))
                {
                    item.ProductId = data.ProductId;
                    item.Name = data.Name;
                    item.No = data.No;
                    item.ChargeModeId = data.ChargeModeId;
                    item.ChargeModeName = data.ChargeModeName;
                    item.Price = data.Price;
                    resp.Save(item);

                    entityInventory.StockAddUp = data.PurchaseCount;
                    respInve.Save(entityInventory);

                    var entityInve = new StockChangeHistory()
                    {
                        DistributorId = data.DistributorId,
                        ProductId = data.ProductId,
                        ProductName = data.Name,
                        Amount = data.PurchaseCount,
                        CreateTime = DateTime.Now
                    };
                    respStoc.Save(entityInve);

                    tran.Complete();
                }
            }

            item.Price = data.Price;
            resp.Save(item);
            return EnumUpdProdRst.Success;
        }

        public bool Recharge(RechargeForm data)
        {
            var respAsse = RF.ResolveInstance<AssetRepository>();
            var itemAsse = respAsse.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatch(Asset.DistributorIdProperty, PropertyOperator.Equal, data.DistributorId),
            });

            using (var tran = RF.TransactionScope(DistributorManageDomainPlugin.DbSettingName))
            {
                if (itemAsse == null)
                {
                    var entity = new Asset
                    {
                        DistributorId = data.DistributorId,
                        Balance = data.Amount,
                        CreateTime = DateTime.Now,
                    };
                    respAsse.Save(entity);
                }
                else
                {
                    itemAsse.Balance += data.Amount;
                    respAsse.Save(itemAsse);
                }

                var respTran = RF.ResolveInstance<TransactionHistoryRepository>();
                var entityTran = new TransactionHistory()
                {
                    DistributorId = data.DistributorId,
                    Amount = data.Amount,
                    DealTime = DateTime.Now
                };
                respTran.Save(entityTran);

                tran.Complete();
            }

            return true;
        }

        public ListPageDto<RechargeListDto> GetRechargeList(ListPageForm listPage)
        {
            var pageInfo = Mapper.Map<PagingInfo>(listPage.Pager);
            pageInfo.IsNeedCount = true;
            var resp = RF.ResolveInstance<TransactionHistoryRepository>();
            pageInfo.IsNeedCount = true;
            var list = resp.GetList(pageInfo, long.Parse(listPage.ParentKey)).Concrete().ToList()
                .Select(t => Mapper.Map<RechargeListDto>(t))
                .ToList();

            return new ListPageDto<RechargeListDto>
            {
                List = list,
                Pager = Mapper.Map<ListPager>(pageInfo),
            };
        }
    }
}
