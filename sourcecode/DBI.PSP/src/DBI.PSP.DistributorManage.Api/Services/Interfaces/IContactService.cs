﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.DistributorManage.Api.Models;
using DBI.PSP.DistributorManage.Api.Models.Dto;
using System.Collections.Generic;
using WebApiComm.Models;

namespace DBI.PSP.DistributorManage.Api.Services.Interfaces
{
    /// <summary>
    /// 合同管理服务接口
    /// </summary>
    public interface IContactService
    {
        /// <summary>
        /// 查询合同列表
        /// </summary>
        /// <param name="pageInfo">分页信息</param>
        /// <returns>ListPageDto<ContactListDto></returns>
        ListPageDto<ContactListDto> GetList(ListPageForm pageInfo);
        /// <summary>
        /// 创建合同
        /// </summary>
        /// <param name="data">ContactCreateForm</param>
        /// <returns></returns>
        long Create(ContactCreateForm data);
        /// <summary>
        /// 修改合同
        /// </summary>
        /// <param name="data">ContactUpdateForm</param>
        /// <returns></returns>
        int Update(ContactUpdateForm data);
        /// <summary>
        /// 删除合同
        /// </summary>
        /// <param name="id">合同记录ID</param>
        /// <param name="distId">渠道商ID</param>
        /// <returns></returns>
        bool Delete(long id, long? distId = null);
        /// <summary>
        /// 得到合同信息
        /// </summary>
        /// <param name="id">合同记录ID</param>
        /// <returns></returns>
        ContactInfoDto GetInfo(long id);
        /// <summary>
        /// 查询渠道商合同列表
        /// </summary>
        /// <param name="pageInfo">分页信息</param>
        /// <param name="distId">渠道商ID</param>
        /// <returns></returns>
        ListPageDto<ContactListDto> GetListByDistId(ListPageForm pageInfo, long distId);

        /// <summary>
        /// 查询合所有同内容列表
        /// </summary>
        /// <param name="pageInfo">分页信息</param>
        /// <returns></returns>
        ListPageDto<ContactContentListDto> GetContentList(ListPageForm pageInfo);
        /// <summary>
        /// 查询合同内容列表
        /// </summary>
        /// <param name="contId">合同记录ID</param>
        /// <returns></returns>
        List<ContactContentListDto> GetContentList(long contId);
        /// <summary>
        /// 查询合同内容列表
        /// </summary>
        /// <param name="contIds">合同ID数组</param>
        /// <returns></returns>
        List<ContactContentListDto> GetContentList(long[] contIds);
        /// <summary>
        /// 创建合同内同
        /// </summary>
        /// <param name="data">ContactContentCreateForm</param>
        /// <returns></returns>
        long CreateContent(ContactContentCreateForm data);
        /// <summary>
        /// 修改合同内容
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool UpdateContent(ContactContentUpdateForm data);
        /// <summary>
        /// 删除合同内容
        /// </summary>
        /// <param name="id">合同内容ID</param>
        /// <param name="contactId">合同记录ID</param>
        /// <returns></returns>
        bool DeleteContent(long id, long contactId);
    }
}
