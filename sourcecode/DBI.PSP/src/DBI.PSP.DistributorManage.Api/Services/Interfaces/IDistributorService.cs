﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.DistributorManage.Api.Models;
using DBI.PSP.DistributorManage.Api.Models.Dto;
using DBI.PSP.DistributorManage.Api.Models.Enum;
using System.Collections.Generic;
using WebApiComm.Models;

namespace DBI.PSP.DistributorManage.Api.Services.Interfaces
{
    /// <summary>
    /// 渠道商服务接口
    /// </summary>
    public interface IDistributorService
    {
        /// <summary>
        /// 创建渠道商
        /// </summary>
        /// <param name="data">DistributorCreateForm</param>
        /// <returns></returns>
        long Create(DistributorCreateForm data);
        /// <summary>
        /// 修改渠道商
        /// </summary>
        /// <param name="data">DistributorUpdateForm</param>
        /// <returns></returns>
        EnumUpdRst Update(DistributorUpdateForm data);
        /// <summary>
        /// 重置渠道商密码
        /// </summary>
        /// <param name="distId">渠道商ID</param>
        /// <param name="newPsd">新密码</param>
        /// <returns></returns>
        bool ResetPassword(long distId, string newPsd);
        /// <summary>
        /// 查询渠道商列表
        /// </summary>
        /// <param name="pageInfo">分页信息</param>
        /// <returns></returns>
        ListPageDto<DistributorListDto> GetList(ListPageForm pageInfo);
        /// <summary>
        /// 得到渠道商信息
        /// </summary>
        /// <param name="id">渠道商ID</param>
        /// <returns></returns>
        DistributorDto Get(long id);
        /// <summary>
        /// 得到渠道商信息
        /// </summary>
        /// <param name="id">渠道商ID</param>
        /// <returns></returns>
        DistributorInfoDto GetInfo(long id);
        /// <summary>
        /// 得到最新渠道商名称列表（前1000位）
        /// </summary>
        /// <returns></returns>
        List<SelectItemDto<long>> GetNameList();

        /// <summary>
        /// 得到渠道商账户信息
        /// </summary>
        /// <param name="distId"></param>
        /// <returns></returns>
        AssetDto GetAsset(long distId);
        /// <summary>
        /// 渠道商账户充值
        /// </summary>
        /// <param name="data">RechargeForm</param>
        /// <returns></returns>
        bool Recharge(RechargeForm data);
        /// <summary>
        /// 渠道商账户充值历史记录
        /// </summary>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        ListPageDto<RechargeListDto> GetRechargeList(ListPageForm pageInfo);

        /// <summary>
        /// 查询渠道商已购商品列表
        /// </summary>
        /// <param name="distId"></param>
        /// <returns></returns>
        List<ProductDto> GetProductList(long distId);
        /// <summary>
        /// 创建渠道商已购商品
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        long CreateProduct(ProductCreateForm data);
        /// <summary>
        /// 修改渠道商已购商品
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        EnumUpdProdRst UpdateProduct(ProductUpdateForm data);
        /// <summary>
        /// 删除渠道商已购商品
        /// </summary>
        /// <param name="id">商品记录ID</param>
        /// <param name="distId">渠道商ID</param>
        /// <param name="prodId">商品ID</param>
        /// <returns></returns>
        bool DeleteProduct(long id, long distId, long prodId);

        /// <summary>
        /// 查询渠道商库存列表
        /// </summary>
        /// <param name="distId">渠道商ID</param>
        /// <returns></returns>
        List<StockDto> GetStockList(long distId);
        /// <summary>
        /// 得到渠道商商品库存信息
        /// </summary>
        /// <param name="distId">渠道商ID</param>
        /// <param name="prodId">商品ID</param>
        /// <returns></returns>
        StockDto GetStock(long distId, long prodId);
        /// <summary>
        /// 增加渠道商商品库存
        /// </summary>
        /// <param name="distId">渠道商ID</param>
        /// <param name="prodId">商品ID</param>
        /// <param name="amount">库存数量</param>
        /// <returns></returns>
        bool AddStock(long distId, long prodId, int amount);
        /// <summary>
        /// 查询渠道商库存变化历史记录
        /// </summary>
        /// <param name="distId">渠道商ID</param>
        /// <returns></returns>
        List<StockChangeListDto> GetStockChangeList(long distId);
    }
}
