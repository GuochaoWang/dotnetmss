﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using CommTools.Cryptology;
using DBI.PSP.DistributorManage.Api.Models.Dto;
using DBI.PSP.DistributorManage.Api.Services.Interfaces;
using DBI.PSP.DistributorManage.Domain;
using Rafy.Domain;

namespace DBI.PSP.DistributorManage.Api.Services
{
    public class AccountService : IAccountService
    {
        public AccountDto Login(string username, string passsword)
        {
            var resp = RF.ResolveInstance<DistributorRepository>();

            var item = resp.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatch(Distributor.UsernameProperty, PropertyOperator.Equal, username),
            });

            if (item == null) return null;

            if (!MD5Helper.Verify(passsword, item.Password)) return null;

            return new AccountDto
            {
                Id = item.Id,
                Name = item.Name,
                Username = item.Username,
                ChannelCode = item.ChannelCode,
            };
        }
    }
}
