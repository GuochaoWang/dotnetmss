﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using AutoMapper;
using DBI.PSP.DistributorManage.Api.Models;
using DBI.PSP.DistributorManage.Api.Models.Dto;
using DBI.PSP.DistributorManage.Api.Services.Interfaces;
using DBI.PSP.DistributorManage.Domain;
using Rafy;
using Rafy.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApiComm.Models;

namespace DBI.PSP.DistributorManage.Api.Services
{
    public class ContactService : IContactService
    {
        public ListPageDto<ContactListDto> GetList(ListPageForm listPage)
        {
            var pageInfo = Mapper.Map<PagingInfo>(listPage.Pager);
            pageInfo.IsNeedCount = true;
            var resp = RF.ResolveInstance<ContactRepository>();
            var list = resp.GetList(pageInfo, listPage.Keyword).Concrete().ToList()
                .Select(t => Mapper.Map<ContactListDto>(t))
                .ToList();
            return new ListPageDto<ContactListDto>
            {
                List = list,
                Pager = Mapper.Map<ListPager>(pageInfo),
            };
        }

        public long Create(ContactCreateForm data)
        {
            data.No = data.No.Trim();

            if (data.DistributorId <= 0)
            {
                var respDist = RF.ResolveInstance<DistributorRepository>();
                var dataDist = respDist.GetFirstBy(new CommonQueryCriteria()
                {
                    new PropertyMatch(Distributor.NameProperty, PropertyOperator.Equal, data.Title),
                });
                if (dataDist == null) return -1;
                data.DistributorId = dataDist.Id;
            }
            
            var resp = RF.ResolveInstance<ContactRepository>();
            var item = resp.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatch(Contact.NoProperty, PropertyOperator.Equal, data.No),
            });
            if (item != null) return -2;

            var entity = Mapper.Map<Contact>(data);
            entity.BeginTime = data.BeginTime.ToLocalTime();
            entity.EndTime = data.EndTime.ToLocalTime();
            entity.CreateTime = DateTime.Now;
            var id = 0L;

            if (data.Contents != null && data.Contents.Count > 0)
            {
                var respCont = RF.ResolveInstance<ContactContentRepository>();

                using (var tran = RF.TransactionScope(DistributorManageDomainPlugin.DbSettingName))
                {
                    resp.Save(entity);
                    id = entity.Id;

                    data.Contents.ForEach(t => 
                    {
                        var entityCont = Mapper.Map<ContactContent>(t);
                        entityCont.CreateTime = DateTime.Now;
                        entityCont.ContactId = id;
                        entityCont.Contact = entity;
                        respCont.Save(entityCont);
                    });

                    tran.Complete();
                }
            }
            else
            {
                resp.Save(entity);
                id = entity.Id;
            }

            return id;
        }

        public int Update(ContactUpdateForm data)
        {
            data.No = data.No.Trim();

            var resp = RF.ResolveInstance<ContactRepository>();
            var entity = resp.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatch(Contact.IdProperty, PropertyOperator.Equal, data.Id),
            });
            if (entity == null) return -1;

            if (data.DistributorId <= 0)
            {
                var respDist = RF.ResolveInstance<DistributorRepository>();
                var dataDist = respDist.GetFirstBy(new CommonQueryCriteria()
                {
                    new PropertyMatch(Distributor.NameProperty, PropertyOperator.Equal, data.Title),
                });
                if (dataDist == null) return -2;
                entity.DistributorId = dataDist.Id;
            }
            else
            {
                entity.DistributorId = data.DistributorId;
            }

            var item = resp.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatch(Contact.NoProperty, PropertyOperator.Equal, data.No),
            });
            if (item != null && string.Compare(entity.No, data.No, true) != 0) return -3;
            
            entity.Title = data.Title;
            entity.No = data.No;
            entity.Price = data.Price;
            entity.BeginTime = data.BeginTime.ToLocalTime();
            entity.EndTime = data.EndTime.ToLocalTime();

            resp.Save(entity);
            return 0;
        }

        public bool Delete(long id, long? distId = null)
        {
            var resp = RF.ResolveInstance<ContactRepository>();

            var q = new CommonQueryCriteria();

            var g = new PropertyMatchGroup(BinaryOperator.And) {
                    new PropertyMatch(PurchasedProduct.IdProperty, PropertyOperator.Equal, id),
                };

            if (distId.HasValue)
            {
                g.Add(new PropertyMatch(PurchasedProduct.DistributorIdProperty, PropertyOperator.Equal, distId));
            }

            q.Add(g);

            var item = resp.GetFirstBy(q);

            if (item != null)
            {
                if (item.ContactContentList != null && item.ContactContentList.Count > 0)
                {
                    using (var tran = RF.TransactionScope(DistributorManageDomainPlugin.DbSettingName))
                    {
                        var respCont = RF.ResolveInstance<ContactContentRepository>();
                        item.ContactContentList.Clear();
                        respCont.Save(item.ContactContentList);

                        item.PersistenceStatus = PersistenceStatus.Deleted;
                        resp.Save(item);

                        tran.Complete();

                        return true;
                    }
                }
                else
                {
                    item.PersistenceStatus = PersistenceStatus.Deleted;
                    resp.Save(item);
                    return true;
                }
            }

            return false;
        }

        public ContactInfoDto GetInfo(long id)
        {
            var resp = RF.ResolveInstance<ContactRepository>();
            var item = resp.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatch(Contact.IdProperty, PropertyOperator.Equal, id),
            });
            if (item == null) return null;

            var rtn = Mapper.Map<ContactInfoDto>(item);
            rtn.Contents = Mapper.Map<List<ContactContentListDto>>(item.ContactContentList);
            return rtn;
        }

        public ListPageDto<ContactListDto> GetListByDistId(ListPageForm listPage, long distId)
        {
            var pageInfo = Mapper.Map<PagingInfo>(listPage.Pager);
            pageInfo.IsNeedCount = true;
            var resp = RF.ResolveInstance<ContactRepository>();
            var list = resp.GetList(pageInfo, distId, listPage.Keyword).Concrete().ToList()
                .Select(t => Mapper.Map<ContactListDto>(t))
                .ToList();
            return new ListPageDto<ContactListDto>
            {
                List = list,
                Pager = Mapper.Map<ListPager>(pageInfo),
            };
        }

        public ListPageDto<ContactContentListDto> GetContentList(ListPageForm listPage)
        {
            var pageInfo = Mapper.Map<PagingInfo>(listPage.Pager);
            pageInfo.IsNeedCount = true;
            var resp = RF.ResolveInstance<ContactContentRepository>();
            var list = resp.GetByKey(pageInfo, long.Parse(listPage.ParentKey), listPage.Keyword).Concrete().ToList()
                .Select(t => Mapper.Map<ContactContentListDto>(t))
                .ToList();
            return new ListPageDto<ContactContentListDto>
            {
                List = list,
                Pager = Mapper.Map<ListPager>(pageInfo),
            };
        }

        public List<ContactContentListDto> GetContentList(long contId)
        {
            var resp = RF.ResolveInstance<ContactContentRepository>();
            return resp.GetByKey(contId).Concrete().ToList()
                .Select(t => Mapper.Map<ContactContentListDto>(t))
                .ToList();
        }

        public List<ContactContentListDto> GetContentList(long[] contIds)
        {
            var resp = RF.ResolveInstance<ContactContentRepository>();
            return resp.GetByKeys(contIds).Concrete().ToList()
                .Select(t => Mapper.Map<ContactContentListDto>(t))
                .ToList();
        }

        public long CreateContent(ContactContentCreateForm data)
        {
            var resp = RF.ResolveInstance<ContactContentRepository>();
            var entity = Mapper.Map<ContactContent>(data);
            entity.CreateTime = DateTime.Now;
            resp.Save(entity);
            return entity.Id;
        }

        public bool UpdateContent(ContactContentUpdateForm data)
        {
            var resp = RF.ResolveInstance<ContactContentRepository>();
            var entity = resp.GetFirstBy(new CommonQueryCriteria()
            {
                new PropertyMatch(ContactContent.IdProperty, PropertyOperator.Equal, data.Id),
                new PropertyMatch(ContactContent.ContactIdProperty, PropertyOperator.Equal, data.ContactId),
            });
            if (data == null) return false;

            entity.ProductId = data.ProductId;
            entity.ProductNo = data.ProductNo;
            entity.ProductName = data.ProductName;
            entity.ChargeModeId = data.ChargeModeId;
            entity.ChargeModeName = data.ChargeModeName;
            entity.Count = data.Count;

            resp.Save(entity);
            return true;
        }

        public bool DeleteContent(long id, long contactId)
        {
            var resp = RF.ResolveInstance<ContactContentRepository>();

            var q = new CommonQueryCriteria
            {
                new PropertyMatchGroup(BinaryOperator.And) {
                    new PropertyMatch(ContactContent.IdProperty, PropertyOperator.Equal, id),
                    new PropertyMatch(ContactContent.ContactIdProperty, PropertyOperator.Equal, contactId),
                }
            };

            var item = resp.GetFirstBy(q);

            if (item != null)
            {
                item.PersistenceStatus = PersistenceStatus.Deleted;
                resp.Save(item);
                return true;
            }

            return false;
        }
    }
}
