﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

using System;
using System.Security.Cryptography;
using System.Text;

namespace CommTools.Cryptology
{
    /// <summary>
    /// MD5加密帮助类
    /// </summary>
    public class MD5Helper
    {
        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="s">原文</param>
        /// <returns></returns>
        public static string Get(string s)
        {
            var md5Hasher = MD5.Create();

            byte[] data = md5Hasher.ComputeHash(Encoding.UTF8.GetBytes(s));

            var sb = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="s">原文</param>
        /// <param name="hs">加密文</param>
        /// <returns></returns>
        public static bool Verify(string s, string hs)
        {
            var hsn = Get(s);

            var comparer = StringComparer.OrdinalIgnoreCase;

            return comparer.Compare(hsn, hs) == 0;
        }
    }
}
