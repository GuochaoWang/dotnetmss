﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

using System;
using System.Collections;

namespace CommTools.Cache
{
    /// <summary>
    /// 本地缓存帮助类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LocalCacheHelper<T>
    {
        private static volatile LocalCacheHelper<T> _instance;
        private static object _syncRoot = new Object();
        private readonly Hashtable _dataHt = Hashtable.Synchronized(new Hashtable());
        private int _expiredMins = 60;

        private LocalCacheHelper()
        {
        }

        public static LocalCacheHelper<T> Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new LocalCacheHelper<T>();
                    }
                }
                return _instance;
            }
        }

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="t">数据</param>
        /// <param name="key">键值</param>
        /// <param name="expiredMinutes">过期时间（分钟）</param>
        public void Set(T t, string key, int expiredMinutes = -1)
        {
            if (expiredMinutes == -1)
            {
                expiredMinutes = this._expiredMins;
            }

            lock (_syncRoot)
            {
                if (_dataHt.ContainsKey(key))
                {
                    _dataHt[key] = new TContainer<T>(t, expiredMinutes);
                }
                else
                {
                    _dataHt.Add(key, new TContainer<T>(t, expiredMinutes));
                }
            }
        }

        private T CacheData { get; set; }

        /// <summary>
        /// 得到缓存值
        /// </summary>
        /// <param name="key">键值</param>
        /// <returns></returns>
        public T Get(string key)
        {
            if (!_dataHt.ContainsKey(key))
                return CacheData;
            var cacheContainer = (TContainer<T>)_dataHt[key];
            if (cacheContainer.IsExpired)
            {
                cacheContainer.Dispose();
                _dataHt.Remove(key);
            }
            else
            {
                return cacheContainer.CacheData;
            }
            return CacheData;
        }

        /// <summary>
        /// 移除缓存
        /// </summary>
        /// <param name="key">键值</param>
        public void Remove(string key)
        {
            if (_dataHt.ContainsKey(key))
            {
                _dataHt.Remove(key);
            }
        }
    }

    /// <summary>
    /// 本地缓存容器类
    /// </summary>
    /// <typeparam name="T">数据类型</typeparam>
    internal class TContainer<T> : IDisposable
    {
        public T Obj;

        private readonly DateTime expireTime = DateTime.Now;

        public TContainer(T obj, int refreshIntervalMinutes)
        {
            expireTime = DateTime.Now.AddMinutes(refreshIntervalMinutes);
            Obj = obj;
        }

        public DateTime ExpireTime
        {
            get { return expireTime; }
        }

        public bool IsExpired
        {
            get { return DateTime.Now > ExpireTime; }
        }

        public T CacheData
        {
            get { return this.Obj; }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
