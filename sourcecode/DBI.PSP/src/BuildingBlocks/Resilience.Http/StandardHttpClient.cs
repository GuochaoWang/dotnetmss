﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Resilience.Http
{
    /// <summary>
    /// 标准HTTP请求客户端
    /// </summary>
    public class StandardHttpClient : IHttpClient
    {
        private HttpClient _client;
        private ILogger<StandardHttpClient> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public StandardHttpClient(ILogger<StandardHttpClient> logger, IHttpContextAccessor httpContextAccessor)
        {
            _client = new HttpClient();
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<string> GetStringAsync(string uri, string token = null)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, uri);

            SetAuthorizationHeader(requestMessage);

            if (!string.IsNullOrEmpty(token))
            {
                requestMessage.Headers.Authorization = new AuthenticationHeaderValue(token);
            }

            var response = await _client.SendAsync(requestMessage);

            CheckResponse(uri, response);

            var dataString =  await response.Content.ReadAsStringAsync();

            _logger.LogDebug(dataString);

            return dataString;
        }

        private async Task<string> DoPostPutAsync<T>(HttpMethod method, string uri, T item, string token = null)
        {
            if (method != HttpMethod.Post && method != HttpMethod.Put)
            {
                throw new ArgumentException("Http Method必须为POST或PUT", nameof(method));
            }

            var requestMessage = new HttpRequestMessage(method, uri);

            SetAuthorizationHeader(requestMessage);

            requestMessage.Content = new StringContent(JsonConvert.SerializeObject(item), System.Text.Encoding.UTF8, "application/json");

            if (!string.IsNullOrEmpty(token))
            {
                requestMessage.Headers.Authorization = new AuthenticationHeaderValue(token);
            }
            
            var response = await _client.SendAsync(requestMessage);

            CheckResponse(uri, response);

            var dataString = await response.Content.ReadAsStringAsync();

            _logger.LogDebug(dataString);

            return dataString;
        }
        
        public async Task<string> PostAsync<T>(string uri, T item, string token = null)
        {
            return await DoPostPutAsync(HttpMethod.Post, uri, item, token);
        }

        public async Task<string> PutAsync<T>(string uri, T item, string token = null)
        {
            return await DoPostPutAsync(HttpMethod.Put, uri, item, token);
        }

        public async Task<string> DeleteAsync(string uri, string token)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Delete, uri);

            SetAuthorizationHeader(requestMessage);

            if (!string.IsNullOrEmpty(token))
            {
                requestMessage.Headers.Authorization = new AuthenticationHeaderValue(token);
            }

            var response = await _client.SendAsync(requestMessage);

            CheckResponse(uri, response);

            var dataString = await response.Content.ReadAsStringAsync();

            _logger.LogDebug(dataString);

            return dataString;
        }

        private void SetAuthorizationHeader(HttpRequestMessage requestMessage)
        {
            var authorizationHeader = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (!string.IsNullOrEmpty(authorizationHeader))
            {
                requestMessage.Headers.Add("Authorization", new List<string>() { authorizationHeader });
            }
        }

        private void CheckResponse(string uri, HttpResponseMessage resp)
        {
            if (resp.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new HttpRequestException($"微服务，发送Http请求, url:{uri}，返回服务器内部错误。HttpStatusCode:500");
            }

            if (resp.StatusCode == HttpStatusCode.NotFound)
            {
                throw new HttpRequestException($"微服务，发送Http请求, url:{uri}，未找到请求的资源。HttpStatusCode:404");
            }
        }
    }
}
