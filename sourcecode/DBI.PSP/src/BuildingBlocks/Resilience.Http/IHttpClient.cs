﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

using System.Threading.Tasks;

namespace Resilience.Http
{
    /// <summary>
    /// HTTP请求接口
    /// </summary>
    public interface IHttpClient
    {
        /// <summary>
        /// GET请求并获取返回字符串
        /// </summary>
        /// <param name="uri">请求地址</param>
        /// <param name="token">令牌</param>
        /// <returns></returns>
        Task<string> GetStringAsync(string uri, string token = null);

        /// <summary>
        /// POST请求并获取返回字符串
        /// </summary>
        /// <typeparam name="T">请求数据类型</typeparam>
        /// <param name="uri">请求地址</param>
        /// <param name="item">请求数据</param>
        /// <param name="token">令牌</param>
        /// <returns></returns>
        Task<string> PostAsync<T>(string uri, T item, string token = null);

        /// <summary>
        /// DELETE请求并获取返回字符串
        /// </summary>
        /// <param name="uri">请求地址</param>
        /// <param name="token">令牌</param>
        /// <returns></returns>
        Task<string> DeleteAsync(string uri, string token = null);

        /// <summary>
        /// PUT请求并获取返回字符串
        /// </summary>
        /// <typeparam name="T">请求数据类型</typeparam>
        /// <param name="uri">请求地址</param>
        /// <param name="item">请求数据</param>
        /// <param name="token">令牌</param>
        /// <returns></returns>
        Task<string> PutAsync<T>(string uri, T item, string token = null);
    }
}
