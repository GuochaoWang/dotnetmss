﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

using AopComm.Config;
using Serilog;
using Serilog.Core;
using System;

namespace AopComm.Log
{
    /// <summary>
    /// 日志帮助类
    /// </summary>
    public class LogHelper
    {
        private static Logger _logger;
        private static object _syncRoot = new Object();

        private static Logger Logger
        {
            get
            {
                if (_logger == null)
                {
                    lock (_syncRoot)
                    {
                        if (_logger == null)
                        {
                            _logger = new LoggerConfiguration()
                                .ReadFrom.Configuration(ConfigHelper.Configuration)
                                .CreateLogger();
                        }
                    }
                }
                return _logger;
            }
        }

        /// <summary>
        /// 记录业务信息日志
        /// </summary>
        /// <param name="log">日志文本</param>
        public static void Info(string log)
        {
            Logger.Information(log);
        }

        /// <summary>
        /// 记录调试信息日志
        /// </summary>
        /// <param name="log">日志文本</param>
        public static void Debug(string log)
        {
            Logger.Debug(log);
        }

        /// <summary>
        /// 记录异常信息日志
        /// </summary>
        /// <param name="ex">异常类</param>
        /// <param name="log">日志文本</param>
        public static void Exception(Exception ex, string log)
        {
            Logger.Error(ex, log);
        }
    }
}
