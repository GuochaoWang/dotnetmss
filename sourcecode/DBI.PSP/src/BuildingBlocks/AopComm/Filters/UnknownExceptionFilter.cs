﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

using AopComm.Log;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Net;

namespace AopComm.Filters
{
    /// <summary>
    /// 未知异常过滤器
    /// </summary>
    public class UnknownExceptionFilter : IExceptionFilter
    {
        private readonly IHostingEnvironment env;
        private readonly ILogger<UnknownExceptionFilter> debugLogger;

        public UnknownExceptionFilter(IHostingEnvironment env, ILogger<UnknownExceptionFilter> logger)
        {
            this.env = env;
            this.debugLogger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            debugLogger.LogError(new EventId(context.Exception.HResult),
                context.Exception,
                context.Exception.Message);

            if (env.IsProduction() || env.IsStaging())
            {
                LogHelper.Exception(context.Exception, context.Exception.Message);
            }

            var json = new JsonErrorResponse
            {
                Messages = new[] { "发生错误，请重试" }
            };

            if (env.IsDevelopment())
            {
                json.DeveloperMeesage = context.Exception;
            }

            context.Result = new InternalServerErrorObjectResult(json);
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            context.ExceptionHandled = true;
        }

        private class JsonErrorResponse
        {
            public string[] Messages { get; set; }

            public object DeveloperMeesage { get; set; }
        }

        public class InternalServerErrorObjectResult : ObjectResult
        {
            public InternalServerErrorObjectResult(object error)
                : base(error)
            {
                StatusCode = StatusCodes.Status500InternalServerError;
            }
        }
    }
}
