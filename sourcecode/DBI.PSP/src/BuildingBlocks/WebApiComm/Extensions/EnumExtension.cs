﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

using System;
using System.ComponentModel;

namespace WebApiComm.Extensions
{
    /// <summary>
    /// 枚举扩展方法类
    /// </summary>
    public static class EnumExtension
    {
        /// <summary>
        /// 得到枚举的属性
        /// </summary>
        /// <typeparam name="T">枚举属性类型</typeparam>
        /// <param name="enumVal">枚举值</param>
        /// <returns>枚举存在的属性</returns>
        /// <example>string desc = myEnumVariable.GetAttributeOfType<DescriptionAttribute>().Description;</example>
        public static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return (attributes.Length > 0) ? (T)attributes[0] : null;
        }

        /// <summary>
        /// 得到枚举描述
        /// </summary>
        /// <param name="enumVal"></param>
        /// <returns></returns>
        public static string GetDes(this Enum enumVal)
        {
            return enumVal.GetAttributeOfType<DescriptionAttribute>().Description;
        }

        /// <summary>
        /// 得到枚举值
        /// </summary>
        /// <param name="enumVal"></param>
        /// <returns></returns>
        public static int GetVal(this Enum enumVal)
        {
            return Convert.ToInt32(enumVal);
        }
    }
}
