﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

namespace WebApiComm.Models
{
    /// <summary>
    /// 列表请求表单数据模型
    /// </summary>
    public class ListPageForm
    {
        /// <summary>
        /// 搜索关键字
        /// </summary>
        public string Keyword { get; set; }
        /// <summary>
        /// 主表主键
        /// </summary>
        public string ParentKey { get; set; }
        /// <summary>
        /// 分页器
        /// </summary>
        public ListPager Pager { get; set; }
    }
}
