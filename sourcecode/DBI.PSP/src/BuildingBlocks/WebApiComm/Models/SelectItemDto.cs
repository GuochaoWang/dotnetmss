﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

namespace WebApiComm.Models
{
    /// <summary>
    /// 下拉框数据模型
    /// </summary>
    /// <typeparam name="TId"></typeparam>
    public class SelectItemDto<TId>
    {
        public TId Id { get; set; }
        public string Value { get; set; }
    }
}
