﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

using System;

namespace WebApiComm.Models
{
    /// <summary>
    /// Web Api返回值数据模型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class ApiResult<T>
    {
        public string Message { get; set; }
        public int Code { get; set; }
        public T Data { get; set; }
        public bool Success { get; set; }
    }
}
