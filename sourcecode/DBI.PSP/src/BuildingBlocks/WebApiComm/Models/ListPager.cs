﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

namespace WebApiComm.Models
{
    /// <summary>
    /// 列表分页器数据模型
    /// </summary>
    public class ListPager
    {
        /// <summary>
        /// 总记录数
        /// </summary>
        public long? TotalCount { get; set; }
        /// <summary>
        /// 每页记录数
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 当前页
        /// </summary>
        public long PageNumber { get; set; }
        /// <summary>
        /// 总页数
        /// </summary>
        public long? PageCount { get; set; }
        /// <summary>
        /// 是否有上一页
        /// </summary>
        public bool? HasPreviousPage { get; set; }
        /// <summary>
        /// 是否有下一页
        /// </summary>
        public bool? HasNextPage { get; set; }
    }
}
