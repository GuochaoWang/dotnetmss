﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

using Newtonsoft.Json.Converters;

namespace WebApiComm.Models
{
    /// <summary>
    /// JSON日期数据类型格式转换器
    /// </summary>
    public class JsonDateTimeConverter : IsoDateTimeConverter
    {
        public JsonDateTimeConverter(string format)
        {
            base.DateTimeFormat = format;
        }
    }
}
