﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

using System.Collections.Generic;

namespace WebApiComm.Models
{
    /// <summary>
    /// 列表返回数据模型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ListPageDto<T>
    {
        /// <summary>
        /// 返回列表
        /// </summary>
        public List<T> List { get; set; }
        /// <summary>
        /// 列表分页器
        /// </summary>
        public ListPager Pager { get; set; }
    }
}
