﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

using Microsoft.AspNetCore.Mvc;
using WebApiComm.Models;
using System.Text;

namespace WebApiComm.Controllers
{
    /// <summary>
    /// Web Api基类
    /// </summary>
    [Produces("application/json")]
    public class ApiBaseController : Controller
    {
        private const string MSG_SUCCESS = "执行成功";
        private const string MSG_INTERNAL_ERROR = "服务器内部错误";
        private const string MSG_BAB_REQUEST = "错误的请求";
        private const string MSG_NOT_FOUND = "未找到";
        private const string MSG_EXECUTE_FAILED = "执行失败";
        private const string MSG_PARAM_NULL = "请求参数为空";

        private const int CODE_SUCCESS = 200;
        private const int CODE_BAB_REQUEST = 401;
        private const int CODE_NOT_FOUND = 404;
        private const int CODE_INTERNAL_ERROR = 500;
        private const int CODE_EXECUTE_FAILED = 600;

        /// <summary>
        /// 无效请求
        /// </summary>
        /// <typeparam name="T">返回值类型</typeparam>
        /// <returns></returns>
        [NonAction]
        public ApiResult<T> BabRequest<T>()
        {
            return FailJsonResult<T>(CODE_BAB_REQUEST, MSG_BAB_REQUEST);
        }

        /// <summary>
        /// 无效请求
        /// </summary>
        /// <typeparam name="T">返回值类型</typeparam>
        /// <param name="msg">消息</param>
        /// <returns></returns>
        [NonAction]
        public ApiResult<T> BabRequest<T>(string msg)
        {
            return FailJsonResult<T>(CODE_BAB_REQUEST, msg);
        }

        /// <summary>
        /// 未找到资源
        /// </summary>
        /// <typeparam name="T">返回值类型</typeparam>
        /// <returns></returns>
        [NonAction]
        public ApiResult<T> NotFound<T>()
        {
            return FailJsonResult<T>(CODE_NOT_FOUND, MSG_NOT_FOUND);
        }

        /// <summary>
        /// 服务器内部错误
        /// </summary>
        /// <typeparam name="T">返回值类型</typeparam>
        /// <returns></returns>
        [NonAction]
        public ApiResult<T> InteranlError<T>()
        {
            return FailJsonResult<T>(CODE_INTERNAL_ERROR, MSG_INTERNAL_ERROR);
        }

        /// <summary>
        /// 请求验证失败-参数无效
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        [NonAction]
        public ApiResult<T> ParameterNull<T>()
        {
            return FailJsonResult<T>(CODE_EXECUTE_FAILED, MSG_PARAM_NULL);
        }

        /// <summary>
        /// 请求执行失败
        /// </summary>
        /// <typeparam name="T">返回值类型</typeparam>
        /// <returns></returns>
        [NonAction]
        public ApiResult<T> Fail<T>()
        {
            return FailJsonResult<T>(CODE_EXECUTE_FAILED, MSG_EXECUTE_FAILED);
        }

        /// <summary>
        /// 请求执行失败
        /// </summary>
        /// <typeparam name="T">返回值类型</typeparam>
        /// <param name="code">代码</param>
        /// <param name="msg">消息</param>
        /// <returns></returns>
        [NonAction]
        public ApiResult<T> Fail<T>(int code, string msg)
        {
            return FailJsonResult<T>(code, msg);
        }

        /// <summary>
        /// 请求执行成功
        /// </summary>
        /// <typeparam name="T">返回值类型</typeparam>
        /// <param name="data">返回数据</param>
        /// <returns></returns>
        [NonAction]
        public ApiResult<T> Success<T>(T data)
        {
            return SuccessJsonResult(data);
        }

        /// <summary>
        /// 请求执行成功
        /// </summary>
        /// <typeparam name="T">返回值类型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">返回数据</param>
        /// <returns></returns>
        [NonAction]
        public ApiResult<T> Success<T>(string message, T data)
        {
            return SuccessJsonResult(data, message);
        }

        /// <summary>
        /// 请求执行成功
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public ApiResult<string> Success()
        {
            return Success("");
        }

        private ApiResult<T> FailJsonResult<T>(int code, string msg)
        {
            return new ApiResult<T>
            {
                Code = code,
                Message = msg,
                Success = false,
                Data = default(T),
            };
        }

        private ApiResult<T> SuccessJsonResult<T>()
        {
            return new ApiResult<T>
            {
                Code = CODE_SUCCESS,
                Message = MSG_SUCCESS,
                Success = true,
                Data = default(T)
            };
        }

        private ApiResult<T> SuccessJsonResult<T>(T data)
        {
            return new ApiResult<T>
            {
                Code = CODE_SUCCESS,
                Message = MSG_SUCCESS,
                Success = true,
                Data = data
            };
        }

        private ApiResult<T> SuccessJsonResult<T>(T data, string msg)
        {
            var rst = SuccessJsonResult<T>(data);
            rst.Message = msg;
            return rst;
        }

        [NonAction]
        public bool ValidateFormData(object data, out string error)
        {
            error = string.Empty;
            var rst = TryValidateModel(data);
            if (!rst) error = GetErrorMessage();
            return rst;
        }

        private string GetErrorMessage()
        {
            var errors = new StringBuilder();
            foreach (var modelState in ModelState.Values)
            {
                foreach (var error in modelState.Errors)
                {
                    errors.Append(error.ErrorMessage);
                }
            }
            return errors.ToString();
        }
    }
}