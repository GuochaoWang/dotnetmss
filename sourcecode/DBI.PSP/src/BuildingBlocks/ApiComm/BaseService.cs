﻿using System;

namespace ApiComm
{
    public class BaseService
    {
        private const string MSG_SUCCESS = "执行成功";
        private const string MSG_INTERNAL_ERROR = "服务器内部错误";
        private const string MSG_BAB_REQUEST = "错误的请求";
        private const string MSG_NOT_FOUND = "未找到";
        private const string MSG_EXECUTE_FAILED = "执行失败";
        private const string MSG_PARAM_NULL = "请求参数为空";

        private const int CODE_SUCCESS = 200;
        private const int CODE_BAB_REQUEST = 401;
        private const int CODE_NOT_FOUND = 404;
        private const int CODE_INTERNAL_ERROR = 500;
        private const int CODE_EXECUTE_FAILED = 600;

        [NonAction]
        public ApiResult<T> BabRequest<T>()
        {
            return FailJsonResult<T>(CODE_BAB_REQUEST, MSG_BAB_REQUEST);
        }

        [NonAction]
        public ApiResult<T> BabRequest<T>(string msg)
        {
            return FailJsonResult<T>(CODE_BAB_REQUEST, msg);
        }

        [NonAction]
        public ApiResult<T> NotFound<T>()
        {
            return FailJsonResult<T>(CODE_NOT_FOUND, MSG_NOT_FOUND);
        }

        [NonAction]
        public ApiResult<T> InteranlError<T>()
        {
            return FailJsonResult<T>(CODE_INTERNAL_ERROR, MSG_INTERNAL_ERROR);
        }

        [NonAction]
        public ApiResult<T> ParameterNull<T>()
        {
            return FailJsonResult<T>(CODE_EXECUTE_FAILED, MSG_PARAM_NULL);
        }

        [NonAction]
        public ApiResult<T> Fail<T>()
        {
            return FailJsonResult<T>(CODE_EXECUTE_FAILED, MSG_EXECUTE_FAILED);
        }

        [NonAction]
        public ApiResult<T> Fail<T>(int code, string msg)
        {
            return FailJsonResult<T>(code, msg);
        }

        [NonAction]
        public ApiResult<T> Success<T>(T data)
        {
            return SuccessJsonResult(data);
        }

        [NonAction]
        public ApiResult<T> Success<T>(string message, T data)
        {
            return SuccessJsonResult(data, message);
        }

        [NonAction]
        public ApiResult<string> Success()
        {
            return Success("");
        }

        private ApiResult<T> FailJsonResult<T>(int code, string msg)
        {
            return new ApiResult<T>
            {
                Code = code,
                Message = msg,
                Success = false,
                Data = default(T),
            };
        }

        private ApiResult<T> SuccessJsonResult<T>(T data)
        {
            return new ApiResult<T>
            {
                Code = CODE_SUCCESS,
                Message = MSG_SUCCESS,
                Success = true,
                Data = data
            };
        }

        private ApiResult<T> SuccessJsonResult<T>(T data, string msg)
        {
            var rst = SuccessJsonResult<T>(data);
            rst.Message = msg;
            return rst;
        }

        [NonAction]
        public bool ValidateFormData(object data, out string error)
        {
            error = string.Empty;
            var rst = TryValidateModel(data);
            if (!rst) error = GetErrorMessage();
            return rst;
        }

        private string GetErrorMessage()
        {
            var errors = new StringBuilder();
            foreach (var modelState in ModelState.Values)
            {
                foreach (var error in modelState.Errors)
                {
                    errors.Append(error.ErrorMessage);
                }
            }
            return errors.ToString();
        }
    }
}
