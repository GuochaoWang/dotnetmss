﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.BusinessManage.Api.Models.Dto;

namespace DBI.PSP.BusinessManage.Api.Services.Interfaces
{
    /// <summary>
    /// 帐号管理
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="passsword">密码</param>
        /// <returns></returns>
        UserDto Login(string username, string passsword);
    }
}
