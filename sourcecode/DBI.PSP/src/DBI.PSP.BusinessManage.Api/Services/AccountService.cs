﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using DBI.PSP.BusinessManage.Api.Models.Dto;
using DBI.PSP.BusinessManage.Api.Services.Interfaces;
using Microsoft.Extensions.Options;
using System.Linq;

namespace DBI.PSP.BusinessManage.Api.Services
{
    public class AccountService : IAccountService
    {
        IOptionsSnapshot<AppSettings> _settings;

        public AccountService(IOptionsSnapshot<AppSettings> settings)
        {
            _settings = settings;
        }

        public UserDto Login(string username, string password)
        {
            var user = _settings.Value.BusinessUserConfig.Users.FirstOrDefault(t => t.Username == username && t.Password == password);
            if (user == null) return null;
            return new UserDto
            {
                Id = user.Id,
                TrueName = user.TrueName,
                Username = user.Username
            };
        }
    }
}
