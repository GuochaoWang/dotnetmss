﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using Microsoft.AspNetCore.Mvc;
using DBI.PSP.BusinessManage.Api.Services.Interfaces;
using DBI.PSP.BusinessManage.Api.Models.Dto;
using WebApiComm.Models;

namespace DBI.PSP.BusinessManage.Api.Controllers.v1
{
    public class AccountController : BaseController
    {
        private IAccountService _service;

        public AccountController(IAccountService service)
        {
            _service = service;
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="data">登录表单数据</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<UserDto> Login([FromBody]LoginForm data)
        {
            var validateError = string.Empty;
            if (!ValidateFormData(data, out validateError))
            {
                return Fail<UserDto>(601, validateError);
            }

            var rst = _service.Login(data.Username, data.Password);

            if (rst == null) return Fail<UserDto>(701, "账号或密码错误");

            return Success(rst);
        }
    }
}