﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.BusinessManage.Api.Models;
using WebApiComm.Controllers;

namespace DBI.PSP.BusinessManage.Api.Controllers.v1
{
    [Produces("application/json")]
    [Route("api/v1/businessmng/[controller]/[action]")]
    public class BaseController : ApiBaseController
    {
    }
}