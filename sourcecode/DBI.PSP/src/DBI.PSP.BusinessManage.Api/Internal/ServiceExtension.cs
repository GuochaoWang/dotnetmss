﻿using DBI.PSP.BusinessManage.Api.Services;
using DBI.PSP.BusinessManage.Api.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.BusinessManage.Api.Internal
{
    public static class ServiceExtension
    {
        public static void AddService(this IServiceCollection services)
        {
            services.AddScoped<IAccountService, AccountService>();
        }
    }
}
