﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180301
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180301 10:03
 * 
*******************************************************/

using System.Collections.Generic;

namespace DBI.PSP.BusinessManage.Api
{
    public class AppSettings
    {
        public string ProductUrl { get; set; }
        public BusinessUserConfig BusinessUserConfig { get; set; }
        public Logging Logging { get; set; }
    }

    public class Logging
    {
        public bool IncludeScopes { get; set; }
        public Loglevel LogLevel { get; set; }
    }

    public class Loglevel
    {
        public string Default { get; set; }
        public string System { get; set; }
        public string Microsoft { get; set; }
    }

    public class BusinessUserConfig
    {
        public List<BusinessUser> Users { get; set; }
    }

    public class BusinessUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string TrueName { get; set; }
        public long Id { get; set; }
    }
}
