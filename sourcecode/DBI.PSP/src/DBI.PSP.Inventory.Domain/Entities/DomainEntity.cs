﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using Rafy;
using Rafy.ComponentModel;
using Rafy.Domain;
using Rafy.Domain.ORM;
using Rafy.Domain.Validation;
using Rafy.MetaModel;
using Rafy.MetaModel.Attributes;
using Rafy.MetaModel.View;
using Rafy.ManagedProperty;

namespace DBI.PSP.Inventory.Domain
{
    [Serializable]
    public abstract class DomainEntity : LongEntity
    {
        #region 构造函数

        protected DomainEntity() { }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected DomainEntity(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion
    }

    [Serializable]
    public abstract class DomainEntityList : EntityList { }

    public abstract class DomainEntityRepository : EntityRepository
    {
        protected DomainEntityRepository() { }
    }

    [DataProviderFor(typeof(DomainEntityRepository))]
    public class DomainEntityRepositoryDataProvider : RdbDataProvider
    {
        protected override string ConnectionStringSettingName
        {
            get { return InventoryDomainPlugin.DbSettingName; }
        }
    }

    public abstract class DomainEntityConfig<TEntity> : EntityConfig<TEntity> { }
}