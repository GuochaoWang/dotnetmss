﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorCustomer.Api.Models.Dto
{
    public class DistributorDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
