﻿using DBI.PSP.DistributorCustomer.Domain;
using Rafy;
using Rafy.DbMigration;
using Rafy.Domain;
using Rafy.Domain.ORM.DbMigration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorCustomer.Api
{
    public class DistributorCustomerDomainApp : DomainApp
    {
        protected override void InitEnvironment()
        {
            RafyEnvironment.DomainPlugins.Add(new DistributorCustomerDomainPlugin());

            base.InitEnvironment();
        }

        protected override void OnRuntimeStarting()
        {
            base.OnRuntimeStarting();

            if (ConfigurationHelper.GetAppSettingOrDefault("DbiPspDomainApp_AutoUpdateDb", true))
            {
                var svc = ServiceFactory.Create<MigrateService>();
                svc.Options = new MigratingOptions
                {
                    //ReserveHistory = true,//ReserveHistory 表示是否需要保存所有数据库升级的历史记录
                    RunDataLossOperation = DataLossOperation.All,//要禁止数据库表、字段的删除操作，请使用 DataLossOperation.None 值。
                    Databases = new string[] { DistributorCustomerDomainPlugin.DbSettingName }
                };
                svc.Invoke();
            }
        }
    }
}
