﻿using DBI.PSP.DistributorCustomer.Api.Models.Dto;
using DBI.PSP.DistributorCustomer.Api.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorCustomer.Api.Services
{
    public class AccountService : IAccountService
    {
        public bool ChangeBalance(long id)
        {
            throw new NotImplementedException();
        }

        public DistributorDto GetDistributor(long id)
        {
            return new DistributorDto { Id = id, Name = id.ToString() };
        }

        public void SetDistributor(long id)
        {
            
        }
    }
}
