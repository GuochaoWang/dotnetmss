﻿using DBI.PSP.DistributorCustomer.Api.Models;
using DBI.PSP.DistributorCustomer.Api.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.PSP.DistributorCustomer.Api.Services.Interfaces
{
    public interface IAccountService
    {
        DistributorDto GetDistributor(long id);
        void SetDistributor(long id);
        bool ChangeBalance(long id);
    }
}
