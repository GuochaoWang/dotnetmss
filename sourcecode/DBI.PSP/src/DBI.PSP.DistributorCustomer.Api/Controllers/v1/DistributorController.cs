﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.DistributorCustomer.Api.Services.Interfaces;
using DBI.PSP.DistributorCustomer.Api.Models;
using DBI.PSP.DistributorCustomer.Api.Models.Dto;
using WebApiComm.Models;

namespace DBI.PSP.DistributorCustomer.Api.Controllers.v1
{
    public class DistributorController : BaseController
    {
        private IAccountService _service;

        public DistributorController(IAccountService service)
        {
            _service = service;
        }

        [HttpGet("{id}")]
        public ApiResult<DistributorDto> Get(long id)
        {
            var data = _service.GetDistributor(id);
            return Success(data);
        }
    }
}