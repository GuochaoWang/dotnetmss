﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DBI.PSP.DistributorCustomer.Api.Models;
using WebApiComm.Controllers;

namespace DBI.PSP.DistributorCustomer.Api.Controllers.v1
{
    [Route("api/v1/account/[controller]")]
    public class BaseController : ApiBaseController
    {
    }
}