﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rafy;
using Rafy.ComponentModel;
using Rafy.DbMigration;
using Rafy.Domain;
using Rafy.Domain.ORM.DbMigration;

namespace DBI.PSP.Inventory.Domain
{
    public class DomainPlugin : DomainPlugin
    {
        public static string DbSettingName = "Domain";

        public override void Initialize(IApp app)
        {
        }
    }
}
