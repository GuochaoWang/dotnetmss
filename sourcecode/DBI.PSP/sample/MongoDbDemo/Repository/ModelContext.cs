using MongoDB.Driver;
using MongoDbDemo.Repository;
using MongoDbDomo.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MongoDbDomo.Repository
{
    public class ModelContext
    {
        private IMongoClient Client { get; set; }
        private IMongoDatabase Database { get; set; }
        private IConfigurationRepository ConfigurationRepository { get; set; }
        private static ModelContext _modelContext;
 
        private ModelContext() { }
 
        public static ModelContext Create(IConfigurationRepository configurationRepository, 
            IConnectionStringRepository connectionStringRepository)
        {
            if (configurationRepository == null) throw new ArgumentNullException("ConfigurationRepository");
            if (connectionStringRepository == null) throw new ArgumentNullException("ConnectionStringRepository");

            if (_modelContext == null)
            {
                _modelContext = new ModelContext();
                string connectionString = connectionStringRepository.ReadConnectionString("MongoDbConnectionString");
                _modelContext.Client = new MongoClient(connectionString);
                _modelContext.Database = _modelContext.Client.GetDatabase(configurationRepository.GetConfigurationValue("DemoDatabaseName", "model"));
                _modelContext.ConfigurationRepository = configurationRepository;
            }
            return _modelContext;
        }
 
        public void TestConnection()
        {
            var dbsCursor = _modelContext.Client.ListDatabases();
            var dbsList = dbsCursor.ToList();
            foreach (var db in dbsList)
            {
                Console.WriteLine(db);
            }
        }

        public IMongoCollection<RestaurantDb> Restaurants
        {
            get { return Database.GetCollection<RestaurantDb>(ConfigurationRepository.GetConfigurationValue("RestaurantsCollectionName", "restaurants")); }
        }

        //public IQueryable<T> QueryTest<T>(string collection, IEnumerable<Guid> shouldBeIn, string fieldName)
        //{
        //    var dataModelCollection = Database.GetCollection<T>(collection);
        //    var findResult = dataModelCollection
        //                            .FindAll()
        //                            .ToList();  //Doing this on purpose, so the query is executed immediatly. Just for this test!
        //                                        //This action takes about 110~140ms.
        //    return findResult.AsQueryable();
        //}
    }
}