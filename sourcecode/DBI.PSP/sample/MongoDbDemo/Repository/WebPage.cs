﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MongoDbDemo.Repository
{
    public class WebPage
    {
        public bool IsSsl { get; set; }
        public string Domain { get; set; }
    }
}
