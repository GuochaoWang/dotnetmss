﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MongoDbDemo.Repository
{
    public class Customer
    {
        public string Address { get; set; }
        IEnumerable<string> Telephones { get; set; }
        public WebPage PublicPage { get; set; }
    }
}
