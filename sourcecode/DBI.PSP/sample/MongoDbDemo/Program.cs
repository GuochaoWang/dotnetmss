﻿using System;
using MongoDB.Driver;
using MongoDbDemo.Repository;
using MongoDbDomo.Infrastructure;
using MongoDbDomo.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MongoDbDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ModelContext modelContext = ModelContext.Create(new ConfigFileConfigurationRepository(), new AppConfigConnectionStringRepository());

            var filter = Builders<RestaurantDb>.Filter.Eq(r => r.Borough, "Somewhere in Pakistan");
            var restaurants = modelContext.Restaurants.FindSync<RestaurantDb>(filter).ToList();
            var restaurant = modelContext.Restaurants.FindSync<RestaurantDb>(filter).FirstOrDefault();

            if (restaurant == null)
            {
                RestaurantDb newPakistaniRestaurant = new RestaurantDb();
                newPakistaniRestaurant.Address = new RestaurantAddressDb()
                {
                    BuildingNr = "12A",
                    Coordinates = new double[] { 31.135, 71.24 },
                    Street = "New Street",
                    ZipCode = 9877654
                };
                newPakistaniRestaurant.Borough = "Somewhere in Pakistan";
                newPakistaniRestaurant.Cuisine = "Pakistani";
                newPakistaniRestaurant.Grades = new List<RestaurantGradeDb>()
                {
                    new RestaurantGradeDb() {Grade = "A", InsertedUtc = DateTime.UtcNow, Score = "9" },
                    new RestaurantGradeDb() {Grade = "C", InsertedUtc = DateTime.UtcNow, Score = "3" }
                };
                newPakistaniRestaurant.Id = 457656745;
                newPakistaniRestaurant.Name = "PakistaniKing";

                RestaurantDb newMexicanRestaurant = new RestaurantDb();
                newMexicanRestaurant.Address = new RestaurantAddressDb()
                {
                    BuildingNr = "2/C",
                    Coordinates = new double[] { 24.68, -100.9 },
                    Street = "Mexico Street",
                    ZipCode = 768324523
                };
                newMexicanRestaurant.Borough = "Somewhere in Mexico";
                newMexicanRestaurant.Cuisine = "Mexican";
                newMexicanRestaurant.Grades = new List<RestaurantGradeDb>()
                {
                    new RestaurantGradeDb() {Grade = "B", InsertedUtc = DateTime.UtcNow, Score = "10" }
                };
                newMexicanRestaurant.Id = 457656745;
                newMexicanRestaurant.Name = "MexicanKing";

                List<RestaurantDb> newRestaurants = new List<RestaurantDb>()
                {
                    newPakistaniRestaurant,
                    newMexicanRestaurant
                };

                modelContext.Restaurants.InsertMany(newRestaurants);
            }

            restaurant = modelContext.Restaurants.FindSync<RestaurantDb>(filter).FirstOrDefault();



            Console.WriteLine(restaurant);

            Console.ReadKey();
        }

        private void CreateIndex()
        {
            ModelContext modelContext = ModelContext.Create(new ConfigFileConfigurationRepository(), new AppConfigConnectionStringRepository());
            //Build an index if it is not already built
            IndexKeysDefinition<ClubMember> keys =
            Builders<ClubMember>.IndexKeys.Ascending("Lastname").Ascending("Forename").Descending("Age");
            //Add an optional name- useful for admin
            var options = new CreateIndexOptions { Name = "MyIndex" };
            modelContext.Restaurants.Indexes.CreateOneAsync(keys, options);
        }

        //private void SearchText()
        //{
        //    var credential = MongoCredential.CreateCredential("test", "readonlyUser", "password");
        //    var settings = new MongoClientSettings
        //    {
        //        Credentials = new[] { credential },
        //        Server = new MongoServerAddress("localhost")
        //    };

        //    var mongoClient = new MongoClient(settings);

        //    var database = mongoClient.GetDatabase("test");

        //    var collection = database.GetCollection<BsonDocument>("oneMillionDocumentsIndexed");

        //    var searchWord = "raven";

        //    var filter = Builders<BsonDocument>.Filter.Text(searchWord);


        //    var documentCount = 0;


        //    var stopwatch = new Stopwatch();

        //    stopwatch.Start();
        //    using (var cursor = collection.FindAsync(filter).Result)
        //    {
        //        while (cursor.MoveNext())  // <-- We never get past this point
        //        {
        //            var batch = cursor.Current;

        //            foreach (var document in batch)
        //            {
        //                Console.WriteLine(document["_id"].AsObjectId.ToString());
        //                Assert.That(document, Is.Not.Null);
        //                documentCount++;
        //            }

        //        }
        //    }
        //    stopwatch.Stop();

        //    Console.WriteLine($"Found {documentCount} documents.  Total time {stopwatch.ElapsedMilliseconds}ms.  Avg. {stopwatch.ElapsedMilliseconds / documentCount}");
        //}

        // Linq
        //public async Task EnumerateClubMembersAsync(IMongoCollection<ClubMember> collection)
        //{
        //    Console.WriteLine("Starting EnumerateClubMembersAsync");
        //    List<ClubMember> membershipList =
        //      await collection.AsQueryable()
        //            .OrderBy(p => p.Lastname)
        //            .ThenBy(p => p.Forename)
        //            .ToListAsync();
        //    Console.WriteLine("Finished EnumerateClubMembersAsync");
        //    Console.WriteLine("List of ClubMembers in collection ...");
        //    foreach (ClubMember clubMember in membershipList)
        //    {
        //        ConsoleHelper.PrintClubMemberToConsole(clubMember);
        //    }
        //}
        //public async Task OrderedFindSelectingAnonymousTypeAsync(IMongoCollection<ClubMember> collection)
        //{
        //    Console.WriteLine("Starting  OrderedFindSelectingAnonymousTypeAsync");
        //    var names =
        //        await
        //            collection.AsQueryable()
        //               .Where(p => p.Lastname.StartsWith("R") && p.Forename.EndsWith("an"))
        //               .OrderBy(p => p.Lastname)
        //               .ThenBy(p => p.Forename)
        //               .Select(p => new { p.Forename, p.Lastname })
        //               .ToListAsync();
        //Console.WriteLine("Finished  OrderedFindSelectingAnonymousTypeAsync");
        //    Console.WriteLine("Members with Lastname starting with 'R' and Forename ending with 'an'");
        //    foreach (var name in names)
        //    {
        //        Console.WriteLine(name.Lastname + " " + name.Forename);
        //    }
        //}

        // 使用forEachAsync 传入aciton 方法避免把所有数据放到内存
        //public async Task FindUsingForEachAsync(IMongoCollection<ClubMember> collection)
        //{
        //    Console.WriteLine("Starting FindUsingForEachAsync");
        //    var builder = Builders<ClubMember>.Filter;
        //    var filter =
        //        builder.Or(
        //            Builders<ClubMember>.Filter.Eq("Lastname", "Rees"),
        //            Builders<ClubMember>.Filter.Eq("Lastname", "Jones"));
        //    await
        //        collection.Find(filter)
        //         //the async read of each item is awaited in sequence
        //         //the 'action' delegate runs on a threadpool thread
        //         .ForEachAsync(c => DoSomeAction(c));
        //    Console.WriteLine(" Finished FindUsingForEachAsync");

        //}

        //private void DoSomeAction(ClubMember c)
        //{
        //    //It's best only to use thread safe methods here
        //    //as we are not running on the main thread

        //}

        // 使用FilterDefinationBuilder执行查询
        //public async Task FindUsingFilterDefinitionBuilder1Async(IMongoCollection<ClubMember> collection)
        //{
        //    Console.WriteLine("Starting FindUsingFilterDefinitionBuilder1Async");
        //    DateTime cutOffDate = DateTime.Now.AddYears(-5);
        //    var builder = Builders<ClubMember>.Filter;
        //    //A greater than filter. Selects where the MembershipDate is greater than the cutOffDate
        //    var filterDefinition = builder.Gt("MembershipDate", cutOffDate.ToUniversalTime());
        //    //DateTime is stored in BsonElement as a UTC value so need to convert
        //    List<ClubMember> membersList =
        //        await
        //            collection.Find(filterDefinition)
        //                .SortBy(c => c.Lastname)
        //                .ThenBy(c => c.Forename)
        //                .ToListAsync();
        //    Console.WriteLine("Finished FindUsingFilterDefinitionBuilder1Async");
        //    Console.WriteLine("\r\nMembers who have joined in the last 5 years ...");
        //    foreach (ClubMember clubMember in membersList)
        //    {
        //        ConsoleHelper.PrintClubMemberToConsole(clubMember);
        //    }
        //}

        //public async Task FindUsingFilterDefinitionBuilder2Async(IMongoCollection<ClubMember> collection)
        //{
        //    Console.WriteLine("Starting FindUsingFilterDefinitionBuilder2Async");
        //    var builder = Builders<ClubMember>.Filter;
        //    //An 'Or' filter. Selects where Lastname ==Rees Or Lastname==Jones
        //    var filter =
        //        builder.Or(
        //            Builders<ClubMember>.Filter.Eq("Lastname", "Rees"),
        //            Builders<ClubMember>.Filter.Eq("Lastname", "Jones"));
        //    IEnumerable<ClubMember> jonesReesList =
        //    await
        //        collection.Find(filter)
        //            .SortBy(c => c.Lastname)
        //            .ThenBy(c => c.Forename)
        //            .ThenByDescending(c => c.Age)
        //            .ToListAsync();
        //    Console.WriteLine("Finished FindUsingFilterDefinitionBuilder2Async");
        //    Console.WriteLine("Members named Jones or Rees ...");
        //    foreach (ClubMember clubMember in jonesReesList)
        //    {
        //        ConsoleHelper.PrintClubMemberToConsole(clubMember);
        //    }
        //    Console.WriteLine("...........");
        //}
    }

    //public static class MainClass
    //{
    //    static IMongoClient _client;
    //    static IMongoDatabase _database;

    //    public static IAggregateFluent<BsonDocument> Sample(this IAggregateFluent<BsonDocument> agg, int count)
    //    {
    //        var new_agg = agg.Skip(10);
    //        var stage = new_agg.Stages[new_agg.Stages.Count - 1];
    //        var newDoc = new BsonDocument {
    //            { "$sample", new BsonDocument {
    //                    {"size", count}
    //                } }
    //        };
    //        stage.GetType().GetField("_document"
    //         , BindingFlags.Instance | BindingFlags.NonPublic)
    //             .SetValue(stage, newDoc);
    //        return new_agg;
    //    }

    //    public static void Main(string[] args)
    //    {
    //        Console.WriteLine("Hello World!");
    //        _client = new MongoClient();
    //        _database = _client.GetDatabase("jobs");

    //        var col = _database.GetCollection<BsonDocument>("results");

    //        var agg = col.Aggregate().Sample(1);

    //        var data = agg.FirstOrDefault();
    //        data = null;
    //    }
    //}
}
