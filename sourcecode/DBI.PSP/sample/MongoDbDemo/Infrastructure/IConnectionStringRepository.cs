namespace MongoDbDomo.Infrastructure
{
    public interface IConnectionStringRepository
    {
        string ReadConnectionString(string connectionStringName);
    }
}