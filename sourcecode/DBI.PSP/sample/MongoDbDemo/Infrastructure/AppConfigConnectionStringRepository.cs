using AopComm.Config;
using System.Configuration;
namespace MongoDbDomo.Infrastructure
{
    public class AppConfigConnectionStringRepository : IConnectionStringRepository
    {
        public string ReadConnectionString(string connectionStringName)
        {
            return ConfigHelper.GetStrValue(connectionStringName);
        }
    }
}