﻿/*******************************************************
 * 
 * 作者：王国超
 * 创建日期：20180410
 * 说明：此文件只包含一个类，具体内容见类型注释。
 * 运行环境：.NET Core 2.0
 * 版本号：1.0.0
 * 
 * 历史记录：
 * 创建文件 王国超 20180410 10:03
 * 
*******************************************************/

using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace AopComm.Config
{
    /// <summary>
    /// 配置文件帮助类
    /// </summary>
    public static class ConfigHelper
    {
        private static IConfigurationRoot _configuration;
        private static object _syncRoot = new Object();

        public static IConfigurationRoot Configuration
        {
            get
            {
                if (_configuration == null)
                {
                    lock (_syncRoot)
                    {
                        if (_configuration == null)
                        {
                            var builder = new ConfigurationBuilder()
                                .SetBasePath(Directory.GetCurrentDirectory())
                                .AddJsonFile("appsettings.json");
                            _configuration = builder.Build();
                        }
                    }
                }
                return _configuration;
            }
        }

        /// <summary>
        /// 得到字符串类型键值
        /// </summary>
        /// <param name="key">键名</param>
        /// <returns></returns>
        public static string GetStrValue(string key)
        {
            return Configuration[key];
        }

        /// <summary>
        /// 得到整形类型键值
        /// </summary>
        /// <param name="key">键名</param>
        /// <returns></returns>
        public static int GetIntValue(string key)
        {
            return Configuration.GetSection(key).Get<int>();
        }
        
        /// <summary>
        /// 得到字符串数组类型键值
        /// </summary>
        /// <param name="key">键名</param>
        /// <returns></returns>
        public static string[] GetArrayValue(string key)
        {
            return Configuration.GetSection(key).Get<string[]>();
        }
    }
}
