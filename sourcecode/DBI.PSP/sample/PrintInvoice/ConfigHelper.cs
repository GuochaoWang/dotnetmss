﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace WebApplication
{
    /// <summary>
    /// 配置文件帮助类
    /// </summary>
    public static class ConfigHelper
    {
        private static IConfigurationRoot _configuration;
        private static object _syncRoot = new Object();

        public static IConfigurationRoot Configuration
        {
            get
            {
                if (_configuration == null)
                {
                    lock (_syncRoot)
                    {
                        if (_configuration == null)
                        {
                            var builder = new ConfigurationBuilder()
                                .SetBasePath(Directory.GetCurrentDirectory())
                                .AddJsonFile("appsettings.json");
                            _configuration = builder.Build();
                        }
                    }
                }
                return _configuration;
            }
        }

        /// <summary>
        /// 得到字符串类型键值
        /// </summary>
        /// <param name="key">键名</param>
        /// <returns></returns>
        public static string GetStrValue(string key)
        {
            return Configuration[key];
        }

        /// <summary>
        /// 得到整形类型键值
        /// </summary>
        /// <param name="key">键名</param>
        /// <returns></returns>
        public static int GetIntValue(string key)
        {
            return Configuration.GetSection(key).Get<int>();
        }

        /// <summary>
        /// 得到字符串数组类型键值
        /// </summary>
        /// <param name="key">键名</param>
        /// <returns></returns>
        public static string[] GetArrayValue(string key)
        {
            return Configuration.GetSection(key).Get<string[]>();
        }
    }
}
