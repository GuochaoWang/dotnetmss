﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace WebApplication.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class PrinterController : Controller
    {
        [HttpPost()]
        public Result Post([FromBody]PrintForm fromData)
        {
            var rst = new Result();

            if (fromData == null || fromData.PdfFileName == null)
            {
                rst.Msg = "打印文件为空";
                return rst;
            }

            rst.Data = new List<PrintResult>();

            foreach (var item in fromData.PdfFileName)
            {
                var rtn = PrintPdf(item);
                rst.Data.Add(new PrintResult { FileName = item, Success = rtn });
                Console.WriteLine("===================================");
            }

            return rst;
        }

        private bool PrintPdf(string filename)
        {
            if (string.IsNullOrWhiteSpace(filename))
            {
                Console.WriteLine("文件名为空");
                return false;
            }

            var proc = new Process();
            try
            {
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proc.StartInfo.Verb = "print";

                proc.StartInfo.FileName = ConfigHelper.GetStrValue("PdfReaderPath");
                var fulfilePath = $"{ConfigHelper.GetStrValue("BaseFileDir")}{filename}";
                proc.StartInfo.Arguments = String.Format(@"/p /h {0}", fulfilePath);

                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.CreateNoWindow = false;

                Console.Write($"文件：{fulfilePath}");
                Console.Write("开始打印...............");

                proc.Start();
                
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                if (proc.HasExited == false)
                {
                    proc.WaitForExit(10000);
                }

                proc.EnableRaisingEvents = true;

                Console.WriteLine("打印成功");

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("打印失败");
                Console.WriteLine($"异常信息：{ex.StackTrace}");

                return false;
            }
            finally
            {
                proc.Close();
                KillAdobe("AcroRd32");
            }
        }

        private bool KillAdobe(string name)
        {
            foreach (Process clsProcess in Process.GetProcesses().Where(
                         clsProcess => clsProcess.ProcessName.StartsWith(name)))
            {
                clsProcess.Kill();
                return true;
            }
            return false;
        }
    }

    public class PrintForm
    {
        public string[] PdfFileName { get; set; }
    }

    public class Result
    {
        public string Msg { get; set; }
        public List<PrintResult> Data { get; set; }
        public Result(string msg = "打印完成")
        {
            Msg = msg;
        }
    }

    public class PrintResult
    {
        public string FileName { get; set; }
        public bool Success { get; set; }
    }
}
